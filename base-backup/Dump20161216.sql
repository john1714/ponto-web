CREATE DATABASE  IF NOT EXISTS `ponto` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ponto`;
-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: ponto
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tab_admin_acesso`
--

DROP TABLE IF EXISTS `tab_admin_acesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_admin_acesso` (
  `idadmin` int(11) NOT NULL AUTO_INCREMENT,
  `login_admin` varchar(45) NOT NULL,
  `senha_admin` varchar(45) NOT NULL,
  PRIMARY KEY (`idadmin`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_admin_acesso`
--

LOCK TABLES `tab_admin_acesso` WRITE;
/*!40000 ALTER TABLE `tab_admin_acesso` DISABLE KEYS */;
INSERT INTO `tab_admin_acesso` VALUES (4,'admin','123ibyte!');
/*!40000 ALTER TABLE `tab_admin_acesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_data_interval`
--

DROP TABLE IF EXISTS `tab_data_interval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_data_interval` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `qnt_inicio` int(11) DEFAULT NULL,
  `qnt_fim` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_data_interval`
--

LOCK TABLES `tab_data_interval` WRITE;
/*!40000 ALTER TABLE `tab_data_interval` DISABLE KEYS */;
INSERT INTO `tab_data_interval` VALUES (1,-1,1),(2,-3,3),(3,-6,3),(4,-6,3),(5,-6,6),(6,-2,2),(7,-3,2),(8,-1,2),(9,-1,3);
/*!40000 ALTER TABLE `tab_data_interval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_dp_acesso`
--

DROP TABLE IF EXISTS `tab_dp_acesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_dp_acesso` (
  `idusuario_dp` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL,
  `nome_colaborador` varchar(100) DEFAULT NULL,
  `status` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idusuario_dp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_dp_acesso`
--

LOCK TABLES `tab_dp_acesso` WRITE;
/*!40000 ALTER TABLE `tab_dp_acesso` DISABLE KEYS */;
/*!40000 ALTER TABLE `tab_dp_acesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_filial_mirror`
--

DROP TABLE IF EXISTS `tab_filial_mirror`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_filial_mirror` (
  `idfilial` int(11) NOT NULL AUTO_INCREMENT,
  `filial` varchar(45) NOT NULL,
  `status` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idfilial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_filial_mirror`
--

LOCK TABLES `tab_filial_mirror` WRITE;
/*!40000 ALTER TABLE `tab_filial_mirror` DISABLE KEYS */;
/*!40000 ALTER TABLE `tab_filial_mirror` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_gestor_acesso`
--

DROP TABLE IF EXISTS `tab_gestor_acesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_gestor_acesso` (
  `idgestor` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL,
  `nome_colaborador` varchar(5) DEFAULT NULL,
  `status` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idgestor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_gestor_acesso`
--

LOCK TABLES `tab_gestor_acesso` WRITE;
/*!40000 ALTER TABLE `tab_gestor_acesso` DISABLE KEYS */;
INSERT INTO `tab_gestor_acesso` VALUES (1,'em003287','John ','cf');
/*!40000 ALTER TABLE `tab_gestor_acesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_ip_fixo`
--

DROP TABLE IF EXISTS `tab_ip_fixo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_ip_fixo` (
  `codip` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(20) NOT NULL,
  `filial_usuario` varchar(20) NOT NULL,
  `status` varchar(5) NOT NULL,
  PRIMARY KEY (`codip`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_ip_fixo`
--

LOCK TABLES `tab_ip_fixo` WRITE;
/*!40000 ALTER TABLE `tab_ip_fixo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tab_ip_fixo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_ip_padrao`
--

DROP TABLE IF EXISTS `tab_ip_padrao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_ip_padrao` (
  `codip` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(20) NOT NULL,
  `status` varchar(5) NOT NULL,
  PRIMARY KEY (`codip`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_ip_padrao`
--

LOCK TABLES `tab_ip_padrao` WRITE;
/*!40000 ALTER TABLE `tab_ip_padrao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tab_ip_padrao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_log`
--

DROP TABLE IF EXISTS `tab_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_log` (
  `idlog` int(12) NOT NULL AUTO_INCREMENT,
  `nome` varchar(70) DEFAULT NULL,
  `em` varchar(30) DEFAULT NULL,
  `usuario` varchar(30) DEFAULT NULL,
  `moment_log` varchar(30) DEFAULT NULL,
  `message_log` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idlog`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_log`
--

LOCK TABLES `tab_log` WRITE;
/*!40000 ALTER TABLE `tab_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tab_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_motivos`
--

DROP TABLE IF EXISTS `tab_motivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_motivos` (
  `idmotivo` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  PRIMARY KEY (`idmotivo`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_motivos`
--

LOCK TABLES `tab_motivos` WRITE;
/*!40000 ALTER TABLE `tab_motivos` DISABLE KEYS */;
INSERT INTO `tab_motivos` VALUES (31,'001','ESQUECIMENTO'),(32,'003','PONTO SEM OPERAR'),(33,'004','TRABALHO EXTERNO'),(34,'005','REUNIÃƒO'),(35,'008','OUTROS TRABALHOS'),(36,'0012','LOJA FECHADA'),(37,'0013','EM OUTRA FILIAL'),(38,'0015','PROBLEMA NA ABERTURA DA LOJA'),(39,'0016','REALIZAR EXAME PERIODICO'),(40,'0019','DIGITAL NAO RECONHECIDA'),(41,'0020','FISOTERAPIA'),(42,'0021','SEM CADASTRO NO PONTO'),(43,'0023','REALIZAÃ‡ÃƒO DE CURSO'),(44,'0024','BATIDA TESTE'),(45,'0025','JUSTIFICADO AO GESTOR'),(46,'0027','VIAGEM A TRABALHO'),(47,'0029','TREINAMENTO'),(48,'0030','SEMANA DA INTEGRAÃ‡ÃƒO'),(49,'0031','SEM BOBINA NO PONTO'),(50,'0032','SEM ENERGIA NA LOJA'),(51,'0033','PAPEL NAO SAIU'),(52,'0034','NAO JUSTIFICADO'),(53,'0036','PONTO TRAVADO'),(54,'0037','PONTO MANUAL'),(55,'0038','PROBLEMAS COM A DIGITAL'),(56,'0039','SAIU DA LOJA'),(57,'0040','VISITA A CLIENTE EXTERNO'),(58,'0041','ERRO NO RECONHECIMENTO DA DIGITAL'),(59,'0042','EVENTO DA EMPRESA'),(60,'0043','RETORNOU A LOJA');
/*!40000 ALTER TABLE `tab_motivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_ponto_batidas`
--

DROP TABLE IF EXISTS `tab_ponto_batidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_ponto_batidas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `em` varchar(30) NOT NULL,
  `filial_origem` varchar(10) NOT NULL,
  `filial_autenticado` varchar(10) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `data_` varchar(10) NOT NULL,
  `hora` varchar(10) NOT NULL,
  `pis` bigint(20) NOT NULL,
  `status` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16174 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_ponto_batidas`
--

LOCK TABLES `tab_ponto_batidas` WRITE;
/*!40000 ALTER TABLE `tab_ponto_batidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tab_ponto_batidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_ponto_justificativa`
--

DROP TABLE IF EXISTS `tab_ponto_justificativa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_ponto_justificativa` (
  `idponto_jus` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(90) NOT NULL,
  `user` varchar(45) NOT NULL,
  `em` varchar(45) NOT NULL,
  `filial` varchar(45) NOT NULL,
  `pis` varchar(45) NOT NULL,
  `data_` varchar(45) NOT NULL,
  `data_add` varchar(45) NOT NULL,
  `hora` varchar(45) NOT NULL,
  `motivo` varchar(250) NOT NULL,
  `obs` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `gestor` varchar(50) DEFAULT NULL,
  `feedback` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`idponto_jus`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_ponto_justificativa`
--

LOCK TABLES `tab_ponto_justificativa` WRITE;
/*!40000 ALTER TABLE `tab_ponto_justificativa` DISABLE KEYS */;
INSERT INTO `tab_ponto_justificativa` VALUES (22,'Silvano Celso Miranda Mota','scelso','001333','F13','12589702487         ','2016-12-13','2016-12-13','08:00','55','Teste',0,'em003287',NULL);
/*!40000 ALTER TABLE `tab_ponto_justificativa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_ponto_old`
--

DROP TABLE IF EXISTS `tab_ponto_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_ponto_old` (
  `idponto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `login` varchar(45) NOT NULL,
  `filial_origem` varchar(45) NOT NULL,
  `filial_autenticacao` varchar(45) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `data` varchar(45) NOT NULL,
  `hora` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `pis` varchar(45) NOT NULL,
  `em` varchar(45) NOT NULL,
  PRIMARY KEY (`idponto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_ponto_old`
--

LOCK TABLES `tab_ponto_old` WRITE;
/*!40000 ALTER TABLE `tab_ponto_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `tab_ponto_old` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-16 12:05:57
