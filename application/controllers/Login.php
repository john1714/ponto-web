<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    private $data = array();
    //private $form_names = null;

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Csrf');
        $this->load->model('Functions');
        $this->load->model('Alert','alert');

        $this->data['body']        = "";
        $this->data['title']       = "Login | Bem-Vindo";

        $this->load->model("Alert");
    }

    public function index()
    {

        if($this->session->flashdata('aviso')) {

            $message       = $this->session->flashdata('aviso');

            if(isset($message['type']) && $message['type'] == "modal"){

                $this->load->model('Modal');
                $this->data['aviso'] = $this->Modal->modal(array( 'body' => $message['message']));

            }


        }

        $this->data['token_id']     = $this->Csrf->get_token_id();
        $this->data['token_value']  = $this->Csrf->get_token( $this->Csrf->get_token_id() );
        $this->data['form_names']   = $this->Csrf->form_names(array('user', 'password','opcao'), false);
        $form  = $this->Csrf->form_names(array('user', 'password','opcao'), false);

        $this->load->view('login/index.php',$this->data);
    }



    public function logar(){


        try {
            $this->load->model('Functions');

            echo $this->Functions->load_view();

            $form_name = $this->Csrf->form_names(array('user', 'password', 'opcao'), false);

            if (isset($_POST[$form_name['user']], $_POST[$form_name['password']], $_POST[$form_name['opcao']])) {

                if ($this->Csrf->check_valid('post')) {

                    $opcao = trim($this->input->post($form_name['opcao']));

                    $usuario = trim($this->input->post($form_name['user']));
                    $senha = trim($this->input->post($form_name['password']));

                    switch ($opcao) {

                        case 'l1':
                            $this->load->model('Service');
                            // AUTENTICA USUARIO
                            $auth = $this->Service->ldapAutenticar($usuario, $senha);
                            if ($auth['sucesso'] == "true") {

                                $dados = array('usuario' => $auth['sAMAccountName'],
                                    'em' => $auth['matricula'],
                                    'nome' => $auth['displayName'],
                                    'filial' => $auth['physicalDeliveryOfficeName']
                                );

                                $novosdados = array(
                                    'usuario' => $auth['sAMAccountName'],
                                    'em' => $this->Functions->removeEm($auth['matricula']),
                                    'nome' => $auth['displayName'],
                                    'filial' => $auth['physicalDeliveryOfficeName'],
                                    'permissao' => 'l1',
                                    'token' => $this->Csrf->get_token()
                                );

                                // CRIA SESSÃO COM OS DADOS DO COLABORADOR
                                $this->session->set_userdata(array('session' => $novosdados));
                                redirect('/PontoWeb', 'refresh');
                            } else {
                                $this->session->set_flashdata('aviso', array('type' => 'modal', 'message' => 'Usuário inválido!'));
                                redirect('/login', 'refresh');
                            }

                            break;

                        case 'l2':
                            $this->load->model('Service');
                            $auth = $this->Service->ldapAutenticar($usuario, $senha);

                            if ($auth['sucesso'] == "true") {

                                $this->load->model("DAO/UsuarioDAO", "usuario");
                                $this->load->model("DAO/UsuarioLocalDAO", "usuarioL");

                                $authSql = $this->usuario->isDp($this->Functions->removeEm($auth['matricula']));
                                $authLocal = $this->usuarioL->isLocalDp($auth['sAMAccountName']);

                                if ($authSql === FALSE && $authLocal === FALSE) {
                                    $this->session->set_flashdata('aviso', array('type' => 'modal', 'message' => 'Você não possui permissão!'));
                                    redirect('/login', 'refresh');
                                    exit();
                                }

                                $novosdados = array(
                                    'usuario' => $auth['sAMAccountName'],
                                    'em' => $this->Functions->removeEm($auth['matricula']),
                                    'nome' => $auth['displayName'],
                                    'filial' => $auth['physicalDeliveryOfficeName'],
                                    'permissao' => 'l2',
                                    'token' => $this->Csrf->get_token()
                                );

                                $this->session->set_userdata(array('session' => $novosdados));
                                redirect('/dp', 'refresh');
                            } else {
                                $this->session->set_flashdata('aviso', array('type' => 'modal', 'message' => 'Usuário inválido!'));
                                redirect('/login', 'refresh');
                            }

                            break;

                        case 'l3':

                            $this->load->model('Service');
                            $this->load->model('DAO/UsuarioDAO', "usuario");
                            $this->load->model('DAO/UsuarioLocalDAO', "usuarioL");
                            $this->load->model('DAO/GestorDAO', "gestor");

                            $auth = $this->Service->ldapAutenticar($usuario, $senha);

                            if ($auth['sucesso'] == "true") {

                                $dados = array('usuario' => $auth['sAMAccountName'],
                                    'em' => trim($auth['matricula']),
                                    'filial' => $auth['physicalDeliveryOfficeName'],
                                    'nome' => $auth['displayName']
                                );

                                $em = $this->Functions->removeEm($dados['em']);

                                if ($this->usuario->isDp($em)) {
                                    $catg = "dp";
                                } elseif ($this->usuario->isGestor($em)) {
                                    $catg = "gs";
                                } elseif ($this->usuarioL->isLocalDp($usuario)) {
                                    $catg = "dp";
                                } elseif ($this->usuarioL->isLocalGestor($usuario)) {
                                    $catg = "gs";
                                } else {
                                    //$this->session->set_flashdata('aviso', array('type' => 'modal', 'message' => 'Você não possui permissão!'));
                                    //redirect('/login', 'refresh');
                                    $catg = "cl"; //colaborador
                                }
                                // brena 001114
                                $novosdados = array(
                                    'usuario' => $auth['sAMAccountName'],
                                    'em' => $this->Functions->removeEm($auth['matricula']),
                                    'nome' => $auth['displayName'],
                                    'filial' => $auth['physicalDeliveryOfficeName'],
                                    'permissao' => 'l3',
                                    'catg' => $catg,
                                    'token' => $this->Csrf->get_token()
                                );

                                $this->session->set_userdata(array('session' => $novosdados));

                                $this->data['aviso'][] = array("message" => "<strong>Bem vindo</strong> " . $novosdados['nome'],
                                    "class" => "alert alert-success");

                                $this->Alert->alert_message($this->data['aviso'], '/Gestor');
                            } else {
                                $this->session->set_flashdata('aviso', array('type' => 'modal', 'message' => 'Usuário inválido!'));
                                redirect('/login', 'refresh');
                            }

                            break;

                        case 'l4':

                            $this->load->model("DAO/AdminDAO", "admin");

                            if ($this->admin->autentica($usuario, $senha)) {

                                $novosdados = array(
                                    'usuario' => $usuario,
                                    'senha' => $senha,
                                    'permissao' => 'l4',
                                    'token' => $this->Csrf->get_token()
                                );

                                $this->session->set_userdata(array('session' => $novosdados));
                                $this->session->set_flashdata('aviso', $this->alert->_alert("Bem Vindo!", "alert alert-success"));
                                redirect("Administrador/acesso", "refresh");
                            } else {
                                $this->session->set_flashdata('aviso', array('type' => 'modal', 'message' => 'Usuário inválido!'));
                                redirect('/login', 'refresh');
                            }
                            echo "1";

                            break;

                        default:
                            $this->session->set_flashdata('aviso', array('type' => 'modal', 'message' => 'Opção Inválida!'));
                            redirect('/login', 'refresh');
                            break;
                    }

                } else {
                    echo "Post inválido!";
                    $form_names = $this->Csrf->form_names(array('usuario', 'senha', 'opcao'), true);
                }

            } else {
                redirect('/login', 'refresh');
                $form_names = $this->Csrf->form_names(array('usuario', 'senha', 'opcao'), true);
            }
        } catch (Exception $exception){
            $this->session->set_flashdata('aviso', array('type' => 'modal', 'message' => $exception->getMessage()));
            redirect('/login', 'refresh');
            exit();
        }
    }

    public function sair(){
        session_destroy();
        $this->data['aviso'][] = array("message" => "Sessão encerrada!",
            "class"   => "alert alert-success");
        $this->Alert->alert_message($this->data['aviso'],'/login');
    }

}
