<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gestor extends CI_Controller
{

    private $alert = array();

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Csrf');

        if (isset($_SESSION['session'])) {
            $sess = $_SESSION['session'];

            $this->sess = $_SESSION['session'];

            if ($sess['token'] == $this->Csrf->get_token() && $sess['permissao'] == 'l3') {

                $this->load->model("Alert");
                $this->load->model("InputNames", "names");

                $names = $this->names->namesFormGestor();

                $this->data['em'] = $sess['em'];
                $this->data['nome'] = $sess['nome'];
                $this->data['usuario'] = $sess['usuario'];
                $this->data['filial'] = $sess['filial'];
                $this->data['catg'] = $sess['catg'];

                $this->data['token_id'] = $this->Csrf->get_token_id();
                $this->data['token_value'] = $this->Csrf->get_token($this->Csrf->get_token_id());
                $this->data['form_names'] = $this->Csrf->form_names($names, false);
                $this->data['body'] = "";
                $this->data['title'] = "Bem-Vindo";
		
            } else {

                $this->session->unset_userdata('session');
                echo 'No direct script access allowed';
                redirect('/login', 'refresh');


            }
        } else {
            $this->session->unset_userdata('session');
            echo 'No direct script access allowed';
            redirect('/login', 'refresh');

        }

        $this->data['body'] = "";

    }

    public function index()
    {

        $this->load->model('DAO/GestorDAO', 'gestor');
        $this->load->model('DAO/UsuarioDAO', 'usuario');
        $this->load->model('Service');
        $message = $this->session->flashdata('aviso');

        if ($message['type'] == "text") {
            $this->data['aviso'] = $message['message'];
        }
        $this->load->model('Functions', "function");

        if ($this->session->flashdata('aviso')) {
            $message = $this->session->flashdata('aviso');

            if ($message['type'] == "text") {
                $this->data['aviso'] = $message['message'];
            }
        }

        /*if($this->sess['catg'] == "dp"){

            $this->data['colaboradores'] = json_decode($this->Service->ldapGetAllUser(),true);

            if($this->data['colaboradores'] == "NULL" || $this->data['colaboradores'] == false){

                $this->data['aviso'][] = array('message' => "Não foi possivel recuperar seus colaboradores, erro de conexão ou de usuário, reinicie sua sessão e caso o erro persista ligue para o Service Desk", "class" => 'alert alert-danger');
            }

        }elseif ($this->sess['catg'] == "gs") {*/

        if ($this->sess['catg'] == "gs" || $this->sess['catg'] == "dp") {
		$ln = $this->usuario->listaColaboradorGestor($this->data['em']);


		$colab = array();
		            if(empty($ln)){
		                    $colab[] = array(
		                      'displayName' => $this->data['nome'],
  		                      'filial' => $this->data['filial'],
  	                              'matricula' => $this->data['em']
				    );        
	                    }else{
			        foreach ($ln as $val) {
			             $colab[] = array(
			               'displayName' => $val['nnomel'],
			               'filial' => $val['filial'],
			               'matricula' => $val['em']
			             );
			        }
		            }        
	            $this->data['colaboradores'] = $colab;


            /*$colab = array();
            foreach ($ln as $val) {
                $colab[] = array(
                    'displayName' => $val['nnomel'],
                    'filial' => $val['filial'],
                    'matricula' => $val['em']
                );
            }
	    $this->data['colaboradores'] = $colab;*/
        } else {
            $colab[] = array(
                'displayName' => $this->data['nome'],
                'filial' => $this->data['filial'],
                'matricula' => $this->data['em']
            );

            $this->data['colaboradores'] = $colab;
        }

        //}

        $this->data['motivos'] = $this->gestor->listaMotivos();

        $this->data['dataintervalo'] = $this->getInterval($_SESSION['session']['filial']);

        $this->load->view('gestor/index.php', $this->data);
    }

    public function view_batidascolaborador()
    {
        $this->load->model('DAO/GestorDAO', 'gestor');
        $this->load->model('DAO/UsuarioDAO', 'usuario');
        $this->load->model('DAO/PontoDAO');

        $ln = $this->usuario->listaColaboradorGestor($this->data['em']);

        $colab = array();

        foreach ($ln as $val) {
            $colab[] = array(
                'displayName' => $val['nnomel'],
                'filial' => $val['filial'],
                'matricula' => $val['em']
            );
        }

        $this->data['colaboradores'] = $colab;


        // #########################################################################

        if ($this->input->post('em') !== NULL) {

            $this->load->model('Service', 'service');

            $em = $this->input->post('em');

            $user = $this->service->ldapGetUserEm($em);

            $this->PontoDAO->setUsuario($user['sAMAccountName']);

            $this->data['batidas'] = $this->PontoDAO->listaBatidasMes();

            #$this->data['tabela'] = $this->PontoDAO->view_table_ponto_user($dados);
        }

        $this->load->view('gestor/colaborador.php', $this->data);

    }

    public function justificativas()
    {

        /*if($this->data['usuario'] == 'em003789'){
            $this->sess['catg'] = 'dp';
		}*/

        $this->load->model("DAO/GestorDAO");
        $this->load->model("DAO/UsuarioDAO", "usuario");
        $this->load->model("DAO/GlobalDAO", "global");

        $this->data['status_'] = $status = array(
            array('status' => 'td', 'nome' => 'Todos'),
            array('status' => 'pd', 'nome' => 'Pendentes'),
            array('status' => 'ap', 'nome' => 'Aprovados'),
            array('status' => 'rc', 'nome' => 'Recusados')
        );


        $filialInput = ($this->input->post("filial")) ? $this->input->post("filial") : "";
        $nomeInput = ($this->input->post("nome")) ? $this->input->post("nome") : "";
        $data1Input = ($this->input->post("data1")) ? $this->input->post("data1") : "";
        $data2Input = ($this->input->post("data2")) ? $this->input->post("data2") : "";
        $status = ($this->input->post("status")) ? $this->input->post("status") : "";

            if($status == "td"){
                $status = "";
            }
            if($status == "pd"){
                $status = " status = 'PD' ";
            }
            if($status == "ap"){
                $status = " status = 'CF' ";
            }
            if($status == "rc"){
                $status = " status = 'CA' ";
            }

        $ln = $this->usuario->listaColaboradorGestor($this->data['em']);

        $colab = array();
        foreach ($ln as $val) {
            $colab[] = array(
                'displayName' => $val['nnomel'],
                'filial' => $val['filial'],
                'matricula' => $val['em']
            );
        }

        if ($this->sess['catg'] == "cl") {
            $array = array(
                'filial' => $filialInput,
                'nome' => $nomeInput,
                'data1' => $data1Input,
                'data2' => $data2Input,
                'status' => $status,
                'gestor' => $this->data['usuario']
            );
            $sql = $this->GestorDAO->prepare_filter($array);
            $lista = $this->GestorDAO->listaFiltroGestor($sql);

            $this->data['justificativas'] = $this->GestorDAO->view_table_colaborador_jus($lista);
        }

        if ($this->sess['catg'] == "dp") {

            $array = array(
                'filial' => $filialInput,
                'nome' => $nomeInput,
                'data1' => $data1Input,
                'data2' => $data2Input,
                'status' => $status,
                'filial' => $filialInput
            );
            $sql = $this->GestorDAO->prepare_filter($array);
            $lista = $this->GestorDAO->listaFiltroGestor($sql);
            $this->data['justificativas'] = $this->GestorDAO->view_table_dp_jus($lista);
        }

        if ($this->sess['catg'] == "gs") {

            $array = array(
                'filial' => $filialInput,
                'em' => $nomeInput,
                'data1' => $data1Input,
                'data2' => $data2Input,
                'status' => $status,
                'gestor' => $this->data['usuario']
            );

            $sql = $this->GestorDAO->prepare_filter($array);

            $lista = $this->GestorDAO->listaFiltroGestor($sql);

            $this->data['justificativas'] = $this->GestorDAO->view_table_gestor_jus($lista);
        }

        $this->data['catg'] = $this->sess['catg'];
        $this->data['filiais'] = $this->global->getFiliais();
        $this->data['form'] = $this->input->post();

        $this->data['colaboradores'] = $colab;

        $this->load->view('gestor/list-justificativa.php', $this->data);
    }

    public function justificativaColaborador()
    {
        $this->data['justificativas'] = $this->listaJustificativaCl();

        $this->load->view('gestor/justificativa-colaborador.php', $this->data);
    }

    public function listaJustificativaCl()
    {

        $this->load->model("DAO/GestorDAO");
        $this->load->model("DAO/UsuarioDAO", "usuario");
        $this->load->model("DAO/GlobalDAO", "global");

        if ($this->sess['catg'] == 'gs') {
            $ln = $this->usuario->listaColaboradorGestor($this->data['em']);

            $col = array();
            $c = 0;
            foreach ($ln as $value) {
                $listaColaborador = $this->GestorDAO->getJustificativaColaborador(trim($value['em']));
                foreach ($listaColaborador as $itemListaClab) {
                    if (!empty($itemListaClab) && $itemListaClab['em'] != $this->data['em']) {
                        $col[] = $itemListaClab;
                    }
                    $c++;
                }
            }

        } elseif ($this->sess['catg'] == 'cl') {
            $col = $this->GestorDAO->getJustificativaColaborador(trim($this->data['em']));
        }


        return $col;
    }

    public function vieweditafeed()
    {
        $this->load->model("DAO/GestorDAO", "gestor");

        $data["id"] = $_POST["id"];

        $dados = $this->gestor->getJusId($data["id"]);

        $data["feedbd"] = $dados[0]["feedback"];

        $this->load->view('gestor/modal-feedback', $data);
    }

    public function justifica()
    {

        $this->load->model("Functions");
        $this->load->model("InputNames", "names");

        $names = $this->names->namesFormGestor();
        $form_name = $this->Csrf->form_names($names, false);

        $motivo = $this->input->post($form_name['motivo']);
        $em = $this->Functions->removeEm($this->input->post($form_name['em']));
        $check = $this->input->post($form_name['check']);

        $data = $this->input->post($form_name['data']);

        $hora = $this->input->post($form_name['hora']);
        $hora .= ":" . date("s");
        $hora1 = $this->input->post($form_name['hora1']);
        $hora2 = $this->input->post($form_name['hora2']);
        $hora3 = $this->input->post($form_name['hora3']);
        $hora4 = $this->input->post($form_name['hora4']);
        $obs = $this->input->post($form_name['obs']);

        //Valida se é o mesmo usuário que esta fazendo a justificativa caso seja colaborador
        if ($this->sess['catg'] == 'cl') {
            if ($em != $this->data['em']) {
                $this->data['aviso'][] = array("message" => "Ação não permitida!", "class" => "alert alert-info");
                $this->Alert->alert_message($this->data['aviso'], 'Gestor');
                redirect('Gestor', 'refresh');
            }
        }

        // VALIDA ENTRADA DA JUSTIFICATIVA
        if (empty($em) || $em === NULL) {
            $this->data['aviso'][] = array("message" => "Nenhum usuário selecionado!", "class" => "alert alert-info");
        }

        if (empty($data) || $data === NULL) {
            $this->data['aviso'][] = array("message" => "Data inválida!", "class" => "alert alert-info");
        } else {

            if ($this->Functions->checkdata($data)) {
                if (!$this->Functions->validaDataJustificativa($data)) {
                    $this->data['aviso'][] = array("message" => "Data inválida!", "class" => "alert alert-info");
                }
            }

        }

        if ($check === 'on') {
            if (empty($hora1) || $hora1 === NULL || $hora1 == "00:00") {
                $this->data['aviso'][] = array("message" => "Hora primeira batida inválida!", "class" => "alert alert-info");
            }

            if (empty($hora2) || $hora2 === NULL || $hora2 == "00:00") {
                $this->data['aviso'][] = array("message" => "Hora segunda batida inválida!", "class" => "alert alert-info");
            }

            if (empty($hora3) || $hora3 === NULL || $hora3 == "00:00") {
                $this->data['aviso'][] = array("message" => "Hora terceira batida inválida!", "class" => "alert alert-info");
            }

            if (empty($hora4) || $hora4 === NULL || $hora4 == "00:00") {
                $this->data['aviso'][] = array("message" => "Hora quarta batida inválida!", "class" => "alert alert-info");
            }
        } else {

            if (empty($hora) || $hora === NULL || $hora == "00:00") {
                $this->data['aviso'][] = array("message" => "Hora inválida", "class" => "alert alert-info");
            }

        }

        if (empty($motivo)) {
            $this->data['aviso'][] = array("message" => "Motivo inválido!", "class" => "alert alert-info");
        }

        if (empty($obs) || $obs === NULL) {
            $this->data['aviso'][] = array("message" => "Preencha campo observação", "class" => "alert alert-info");
        }


        if (!empty($this->data['aviso'])) {
            $this->Alert->alert_message($this->data['aviso'], 'Gestor');
            redirect('Gestor', 'refresh');
        }

        $data_do_formulario = $data;
        $parts = explode('-', $data_do_formulario);
        $dia_ = $parts[0];
        $mes_ = $parts[1];
        $ano_ = $parts[2];

        $datanew = $ano_ . "-" . $mes_ . "-" . $dia_;

        // FIM DA VALIDAÇÃO DOS DADOS DA JUSTIFICATIVA


        // COLETA DADOS DO COLABORADOR
        $this->load->model("Service");
        $this->load->model("DAO/UsuarioDAO", "usuario");
        $dados = $this->Service->ldapGetUserEm($em);


        $nome = $dados['displayName'];
        $usuario = $dados["sAMAccountName"];
        $em = $this->Functions->removeEm($dados["matricula"]);
        $filial = $dados["filial"];

        $status = ($this->sess['catg'] == 'cl') ? 'PDCL' : 'PD';

        if($this->sess['catg'] == 'gs'){
            $gestor = $this->data['usuario'];
        } else if ($this->sess['catg'] == 'cl'){
            $gestor = $em;
        } else if ($this->sess['catg'] == 'dp'){
            $gestor = $this->data['usuario'];
        }

        if ($check === NULL) {
            //CRIA OBJETO DA JUSTIFICATIVA
            try {
                echo $this->Functions->load_view();

                $this->load->model("Obj/Justificativa", "jus");

                $this->jus->setNome($nome);
                $this->jus->setUsuario($usuario);
                $this->jus->setEm($em);
                $this->jus->setFilial($filial);
                $this->jus->setPis($this->usuario->getPis($em));
                $this->jus->setData($datanew);
                $this->jus->setDataRegistro(date("Y-m-d"));
                $this->jus->setHora($hora);
                $this->jus->setMotivo($motivo);
                $this->jus->setObservacao($obs);
                $this->jus->setStatus($status);
                $this->jus->setGestor($gestor);

                $obj = $this->jus->getObject();

            } catch (Exception $e) {

                $this->data['aviso'][] = array("message" => "Exeption: Não conseguimos completar todas as infomações necessárias, pois o valor <strong>" . $e->getMessage() . "</strong> está faltando.", "class" => "alert alert-danger");

                $this->Alert->alert_message($this->data['aviso'], 'Gestor');

            }

            $this->load->model("DAO/GestorDAO", "gestor");

            try {

                $this->gestor->insereJustificativa($obj);

                $this->data['aviso'][] = array("message" => "<strong>Justificativa salva!</strong>",
                    "class" => "alert alert-success");
                unset($obj);

                $this->Alert->alert_message($this->data['aviso'], 'Gestor');


            } catch (Exception $e) {

                $this->data['aviso'][] = array("message" => "<strong>" . $e->getMessage() . "</strong>",
                    "class" => "alert alert-danger");
                unset($obj);
                $this->Alert->alert_message($this->data['aviso'], 'Gestor');


            }
        }

        if ($check == "on") {
            $hora = array();

            $horas[] = $hora1 . date(":s");
            $horas[] = $hora2 . date(":s");
            $horas[] = $hora3 . date(":s");
            $horas[] = $hora4 . date(":s");

            $this->load->model("DAO/GestorDAO", "gestor");

            for ($a = 0; $a < count($horas); $a++) {

                $this->load->model("Obj/Justificativa", "jus_$a");

                try {
                    $class = "jus_" . $a;

                    $this->$class->setNome($nome);
                    $this->$class->setUsuario($usuario);
                    $this->$class->setEm($em);
                    $this->$class->setFilial($filial);
                    $this->$class->setPis($this->usuario->getPis($em));
                    $this->$class->setData($datanew);
                    $this->$class->setDataRegistro(date("Y-m-d"));
                    $this->$class->setMotivo($motivo);
                    $this->$class->setObservacao($obs);
                    $this->$class->setStatus($status);
                    $this->$class->setGestor($gestor);
                    $this->$class->setHora($horas[$a]);

                    $objs = $this->$class->getObject();

                    $this->gestor->insereJustificativa($objs);

                    $ins = true;

                } catch (Exception $e) {

                    $this->data['aviso'][] = array("message" => "<strong>" . $e->getMessage() . "</strong>",
                        "class" => "alert alert-danger");

                }

            }

            if ($ins) {
                $this->data['aviso'][] = array("message" => "<strong>Concluido!</strong>",
                    "class" => "alert alert-success");
            }

            if (isset($this->data['aviso'])) {
                $this->Alert->alert_message($this->data['aviso'], 'Gestor');
                exit();
            }

        }


    }

    public function recusar()
    {

        $this->load->model("DAO/GestorDAO", "gestor");

        $id = $this->input->post("id");

        $status = "";
        if ($this->sess['catg'] == 'gs') {
            $status = "CAGS"; //Cancelado pelo gestor
        } elseif ($this->sess['catg'] == 'dp') {
            $status = "CA"; //Cancelado pelo dp
        }

        if ($this->gestor->atualizaStatus($id, $status)) {

            $lista = $this->gestor->getJusId($id);

            $table = $this->gestor->view_table_gestor_jus_obj($lista);

            echo json_encode(array('success' => "true", 'input' => $table));
        } else {
            echo json_encode(array('success' => "false", "message" => "Não foi possível aprovar!"));
        }

    }

    public function aprovar()
    {

        $this->load->model("DAO/GestorDAO", "gestor");

        $id = $this->input->post("id");

        $this->load->model("DAO/PontoDAO", "ponto");

        $lista = $this->gestor->getJusId($id);
        $lista = $lista[0];

        $status = "";
        if ($this->sess['catg'] == 'gs') {

            if ($this->gestor->atualizaStatus($id, 'PD', true)) {
                $lista = $this->gestor->getJusId($id);

                $table = $this->gestor->view_table_gestor_jus_obj($lista);

                echo json_encode(array('success' => "true", 'input' => $table));
                exit();
            }

        } elseif ($this->sess['catg'] == 'dp') {

            $rowPonto = (object)array(
                'nome' => $lista['nome'],
                'usuario' => $lista['user'],
                'em' => $lista['em'],
                'filial_origem' => $lista['filial'],
                'filial_autenticado' => $lista['filial'],
                'ip' => '0.0.0.0',
                'data_' => $lista['data_'],
                'hora' => $lista['hora'],
                'pis' => $lista['pis'],
                'status' => 'PD'
            );

            if ($this->ponto->inserePonto($rowPonto)) {

                if ($this->gestor->atualizaStatus($id, 'CF')) {

                    $lista = $this->gestor->getJusId($id);

                    $table = $this->gestor->view_table_gestor_jus_obj($lista);

                    echo json_encode(array('success' => "true", 'input' => $table));
                } else {
                    echo json_encode(array('success' => "false", "message" => "Não foi possível aprovar!"));
                }

            }
        }

    }

    public function editarFeedback()
    {


        $this->load->model("DAO/GestorDAO", "gestor");

        $id = $this->input->post("id");
        $feed = $this->input->post("feedback");

        if ($this->gestor->atualizaFeed($id, $feed)) {

            $lista = $this->gestor->getJusId($id);

            $table = $this->gestor->view_table_gestor_jus_obj($lista);

            echo json_encode(array('success' => "true", 'input' => $table));
        } else {
            echo json_encode(array('success' => "false", "message" => "Não foi possível editar, talvez esta justificativa não exista, caso o erro persista contate o suporte."));
        }

    }

    public function getInterval($filial = null)
    {
        $this->load->model("Functions", "function");

        $ln = $this->function->getDataSemanal($filial);

        $json = (string)json_encode($ln);

        $texto = str_replace("\/", "-", $json);

        return $texto;

    }

    public function excluir()
    {

        $this->load->model("DAO/GestorDAO", "gestor");

        $id = $this->input->post("id");

        if ($this->gestor->atualizaStatus($id, 'EX')) {

            echo json_encode(array('success' => "true", 'identf' => $id));

        } else {

            echo json_encode(array('success' => "false", "message" => "Não foi possível excluir, talvez já tenha sido excluída, caso o erro persista contate o suporte."));

        }

    }

}

?>
