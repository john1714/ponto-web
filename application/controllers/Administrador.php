<?php

class Administrador extends CI_Controller
{

    private $data = array();

    public function __construct()
    {

        parent::__construct();
        $this->load->model('Csrf');

        if (!isset($_SESSION['session'])):
            $this->session->unset_userdata('session');
            echo 'No direct script access allowed';
            redirect('/login', 'refresh');
        endif;

        $sess = $_SESSION['session'];

        if ($sess['token'] != $this->Csrf->get_token() && $sess['permissao'] != 'l4') {
            $this->session->unset_userdata('session');
            echo 'No direct script access allowed';
            redirect('/login', 'refresh');
        }

        // INSTANCIA CLASSES PRE DEFINIDAS

        $this->load->model('DAO/AdminDAO', 'admin');
        $this->load->model('DAO/GlobalDAO', 'global');
        $this->load->model("DAO/GlobalLocalDAO", "globall");
        $this->load->model('Functions');
        $this->load->model('InputNames', 'names');
        $this->load->model("Alert", "alert");

        $this->data['body'] = "";
        $this->data['title'] = "Administrador";

        if ($this->session->flashdata('aviso')) {
            $this->data['aviso'] = $this->session->flashdata('aviso');
        }

        if ($this->session->flashdata('erro')) {
            $this->data['erro'] = $this->session->flashdata('erro');
        }

    }

    public function index()
    {
        redirect("Administrador/acesso", "refresh");
    }

    public function view_permissao_filial()
    {

        $this->data['filial'] = $this->global->getFiliais();
        /*$this->data['filial'] = array(
                                        array("iuorg" => "F01"),
                                        array("iuorg" => "F02"),
                                        array("iuorg" => "F03"),
                                        array("iuorg" => "F04")

        );*/
        $this->data['token_id'] = $this->Csrf->get_token_id();
        $this->data['token_value'] = $this->Csrf->get_token($this->Csrf->get_token_id());
        $this->data['form_names'] = $this->Csrf->form_names(array("filial", "acao"), false);

        $this->load->view("admin/view-header");
        $this->load->view("admin/view-acesso-filial", $this->data);
        $this->load->view("admin/view-footer");
    }

    public function view_permissao_ip()
    {

        $this->data['token_id'] = $this->Csrf->get_token_id();
        $this->data['token_value'] = $this->Csrf->get_token($this->Csrf->get_token_id());
        $this->data['form_names'] = $this->Csrf->form_names(array("ipf", "ipp", "filial", "acao"), false);

        $this->data['filial'] = $this->global->getFiliais();

        $this->load->view("admin/view-header");
        $this->load->view("admin/view-acesso-ip", $this->data);
        $this->load->view("admin/view-footer", $this->data);
    }


    public function acesso()
    {
        $this->load->model("DAO/AdminDAO", "admin");

        $ln = $this->admin->getDataInterval();

        $intervalos = $this->admin->getFilialIntervalAll();

        $this->data['intervalos'] = $intervalos;

        $names = $this->names->getNameAcesso();

        $this->data['token_id'] = $this->Csrf->get_token_id();
        $this->data['token_value'] = $this->Csrf->get_token($this->Csrf->get_token_id());
        $this->data['form_names'] = $this->Csrf->form_names($names, false);

        if ($this->session->flashdata('aviso')) {
            $this->data['aviso'] = $this->session->flashdata('aviso');
        }


        $this->load->view("admin/view-header");
        $this->load->view("admin/view-acesso", $this->data);
        $this->load->view("admin/view-footer");
    }


    public function listaAcessoDp()
    {

        $ln = $this->admin->getAcessoDp();

        $li = "";

        foreach ($ln as $val) {

            if ($val['status'] == "cf") {
                $li .= "<li class=\"list-group-item text-success\">
							<strong>" . $val['usuario'] . " - " . $val['nome_colaborador'] . "</strong> <br />
									Ativo
						</li>";
            } elseif ($val['status'] != "cf" && $val['status'] != "ca") {
                $li .= "<li class=\"list-group-item text-warning\">
							<strong>" . $val['usuario'] . " - " . $val['nome_colaborador'] . "</strong> <br />
									Status inválido!
						</li>";
            }
            /*if($val['status'] == "ca"){
                $li .= "<li class=\"list-group-item text-danger\">
                            <strong>".$val['usuario']." - ".$val['nome_colaborador']."</strong> <br />
                                    Desativado
                        </li>";

            }elseif($val['status'] == "cf"){
                $li .= "<li class=\"list-group-item text-success\">
                            <strong>".$val['usuario']." - ".$val['nome_colaborador']."</strong> <br />
                                    Ativo
                        </li>";
            }else{
                $li .= "<li class=\"list-group-item text-warning\">
                            <strong>".$val['usuario']." - ".$val['nome_colaborador']."</strong> <br />
                                    Status inválido!
                        </li>";
            }*/


        }

        echo $li;

    }


    public function listaAcessoGestor()
    {

        $ln = $this->admin->getAcessoGestor();

        $li = "";

        foreach ($ln as $val) {

            if ($val['status'] == "cf") {
                $li .= "<li class=\"list-group-item text-success\">
							<strong>" . $val['usuario'] . " - " . $val['nome_colaborador'] . "</strong> <br />
									Ativo
						</li>";
            } elseif ($val['status'] != "cf" && $val['status'] != "ca") {
                $li .= "<li class=\"list-group-item text-warning\">
							<strong>" . $val['usuario'] . " - " . $val['nome_colaborador'] . "</strong> <br />
									Status inválido!
						</li>";
            }

            /*if($val['status'] == "ca"){
                $li .= "<li class=\"list-group-item text-danger\">
                            <strong>".$val['usuario']." - ".$val['nome_colaborador']."</strong> <br />
                                    Desativado
                        </li>";

            }elseif($val['status'] == "cf"){
                $li .= "<li class=\"list-group-item text-success\">
                            <strong>".$val['usuario']." - ".$val['nome_colaborador']."</strong> <br />
                                    Ativo
                        </li>";
            }else{
                $li .= "<li class=\"list-group-item text-warning\">
                            <strong>".$val['usuario']." - ".$val['nome_colaborador']."</strong> <br />
                                    Status inválido!
                        </li>";
            }*/


        }

        echo $li;

    }


    public function listaPadraoIp()
    {

        $ln = $this->globall->getIpPadrao();

        $li = "";

        foreach ($ln as $val) {

            if ($val["status"] == "cf") {
                $li .= "<li class=\"list-group-item text-success\">
							<strong>" . $val["ip"] . "</strong> <br />
									Ativo
						</li>";
            } elseif ($val['status'] != "cf" && $val['status'] != "ca") {
                $li .= "<li class=\"list-group-item text-success\">Status inválido</li>";
            }


        }

        echo $li;

    }

    public function listaIpFixo()
    {

        $ln = $this->globall->getIpFixo();

        $li = "";

        foreach ($ln as $val) {

            if ($val["status"] == "cf") {
                $li .= "<li class='list-group-item text-success'>
							<strong>" . $val['filial_usuario'] . " - " . $val['ip'] . "</strong> <br />
									Ativo
						</li>";

            } elseif ($val['status'] != "cf" && $val['status'] != "ca") {
                $li .= "<li class=\"list-group-item text-warning\">
							<strong>" . $val["filial_usuario"] . " - " . $val["ip"] . "</strong> <br />
									Status inválido! -> $val[status]
						</li>";
            }


        }

        echo $li;

    }


    public function listaFilial()
    {

        $ln = $this->globall->getFilial();

        $li = "";

        foreach ($ln as $val) {

            /*if($val["status"] == "ca"){
                $li .= "<li class=\"list-group-item text-danger\">
                            <strong>".$val["filial"]."</strong> <br />
                                    Desativado
                        </li>";

            }else*/
            if ($val["status"] == "cf") {
                $li .= "<li class=\"list-group-item text-success\">
							<strong>" . $val["filial"] . "</strong> <br />
									Ativo
						</li>";
            } elseif ($val["status"] != "cf" && $val["status"] != "ca") {
                $li .= "<li class=\"list-group-item text-warning\">
							<strong>" . $val["filial"] . "</strong> <br />
								Status inválido! -> " . $val['status'] . "
								</li>";
            }


        }

        echo $li;

    }

    public function salva_ip()
    {

        $form_name = $this->Csrf->form_names(array("filial", "acao", "ipp", "ipf"), false);
        //var_dump($_POST);
        if (isset($_POST[$form_name['filial']], $_POST[$form_name['acao']], $_POST[$form_name['ipp']], $_POST[$form_name['ipf']])) {

            if ($this->Csrf->check_valid('post')) {

                $ipf = trim($this->input->post($form_name['ipf']));
                $ipp = trim($this->input->post($form_name['ipp']));
                $filial = trim($this->input->post($form_name['filial']));
                $acao = $this->input->post($form_name['acao']);

                $acao = trim($acao);

                if ($acao == "adipf") {
                    if (empty($ipf) || empty($filial)) {
                        $this->session->set_flashdata("erro", "<script>swal('Preencha todos os paramentros!');</script>");
                        redirect('/Administrador/permissao-ip', 'refresh');
                        exit();
                    }

                }

                if ($acao == "rmipf") {
                    if (empty($ipf) || empty($filial)) {
                        $this->session->set_flashdata("erro", "<script>swal('Preencha todos os paramentros!');</script>");
                        redirect('/Administrador/permissao-ip', 'refresh');
                        exit();
                    }

                }


                switch ($acao) {
                    case "adipf":

                        $ln = $this->globall->getIpFixo(array("ip" => $ipf, "filial" => $filial));

                        isset($ln[0]) ? $ln = $ln[0] : null;

                        if (count($ln) > 0) {
                            if ($this->globall->updateIpfixo(array("id" => $ln["codip"], "status" => "cf"))) {

                                $this->session->set_flashdata("erro", "<script>swal('Concluido!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alterações!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            }
                        } else {
                            if ($this->globall->insertIpfixo(array("ip" => $ipf, "filial" => $filial, "status" => "cf"))) {

                                $this->session->set_flashdata("erro", "<script>swal('Concluido!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alterações!!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            }
                        }


                        break;

                    case "rmipf":

                        $this->load->model("DAO/GlobalLocalDAO", "globall");

                        $ln = $this->globall->getIpFixo(array("ip" => $ipf, "filial" => $filial));

                        isset($ln[0]) ? $ln = $ln[0] : null;

                        if (count($ln) > 0) {
                            if ($this->globall->updateIpfixo(array("id" => $ln["codip"], "status" => "ca"))) {

                                $this->session->set_flashdata("erro", "<script>swal('Concluido!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alterações!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            }
                        } else {
                            if ($this->globall->insertIpfixo(array("ip" => $ipf, "filial" => $filial, "status" => "ca"))) {

                                $this->session->set_flashdata("erro", "<script>swal('Concluido!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alterações!!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            }
                        }


                        break;


                    case "adipp":

                        $iptest = $ipp . ".0.0";

                        if (!$this->input->valid_ip($iptest)) {

                            $this->session->set_flashdata("erro", "<script>swal('Formato inválido!');</script>");
                            redirect('/Administrador/permissao-ip', 'refresh');

                        }

                        $ln = $this->globall->getIpPadrao(array("ip" => $ipp));

                        isset($ln[0]) ? $ln = $ln[0] : null;

                        if (count($ln) > 0) {
                            if ($this->globall->updateIpPadrao(array("id" => $ln["codip"], "status" => "cf"))) {

                                $this->session->set_flashdata("erro", "<script>swal('Concluido!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alterações!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            }
                        } else {
                            if ($this->globall->insertIpPadrao(array("ip" => $ipp, "status" => "cf"))) {

                                $this->session->set_flashdata("erro", "<script>swal('Concluido!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alterações!!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            }
                        }


                        break;

                    case "rmipp":

                        $iptest = $ipp . ".0.0";

                        if (!$this->input->valid_ip($iptest)) {

                            $this->session->set_flashdata("erro", "<script>swal('Formato inválido!');</script>");
                            redirect('/Administrador/permissao-ip', 'refresh');

                        }

                        $ln = $this->globall->getIpPadrao(array("ip" => $ipp));

                        isset($ln[0]) ? $ln = $ln[0] : null;

                        if (count($ln) > 0) {
                            if ($this->globall->updateIpPadrao(array("id" => $ln["codip"], "status" => "ca"))) {

                                $this->session->set_flashdata("erro", "<script>swal('Concluido!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alterações!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            }
                        } else {
                            if ($this->globall->insertIpPadrao(array("ip" => $ipp, "status" => "ca"))) {

                                $this->session->set_flashdata("erro", "<script>swal('Concluido!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alterações!!');</script>");
                                redirect('/Administrador/permissao-ip', 'refresh');

                            }
                        }


                        break;
                }


            }
        }
    }


    public function salva_acesso_filial()
    {

        $form_name = $this->Csrf->form_names(array("filial", "acao"), false);

        if (isset($_POST[$form_name['filial']], $_POST[$form_name['acao']])) {

            if ($this->Csrf->check_valid('post')) {

                $filial = $this->input->post($form_name['filial']);
                $acao = $this->input->post($form_name['acao']);

                if (empty($filial) || empty($acao)) {
                    $this->session->set_flashdata("erro", "<script>swal('Parametros inválidos!');</script>");
                    redirect('/Administrador/permissao-filial', 'refresh');
                }

                if (!$this->global->isFilial($filial)) {
                    $this->session->set_flashdata("erro", "<script>swal('Filial inválida!');</script>");
                    redirect('/Administrador/permissao-filial', 'refresh');
                }

                switch ($acao) {
                    case "add":

                        if ($this->admin->liberaFilial(array("filial" => $filial, "status" => "cf"))) {

                            $this->session->set_flashdata("erro", "<script>swal('Filial Liberada!');</script>");
                            redirect('/Administrador/permissao-filial', 'refresh');

                        } else {
                            $this->session->set_flashdata("erro", "<script>swal('Erro ao fazer a alteração');</script>");
                            redirect('/Administrador/permissao-filial', 'refresh');
                        }

                        break;

                    case "rmv":

                        if ($this->admin->cancelaFilial(array("filial" => $filial, "status" => "cf"))) {

                            $this->session->set_flashdata("erro", "<script>swal('Permissão retirada!!');</script>");
                            redirect('/Administrador/permissao-filial', 'refresh');

                        } else {
                            $this->session->set_flashdata("erro", "<script>swal('Erro ao fazer a alteração');</script>");
                            redirect('/Administrador/permissao-filial', 'refresh');
                        }

                        break;
                }

            }
        }
    }


    public function salva_acesso()
    {

        $names = $this->names->getNameAcesso();

        $form_name = $this->Csrf->form_names($names, false);

        if (isset($_POST[$form_name['usuario']], $_POST[$form_name['acao']])) {

            if ($this->Csrf->check_valid('post')) {

                $usuario = trim($this->input->post($form_name['usuario']));
                $acao = trim($this->input->post($form_name['acao']));

                $this->load->model("Service", "service");
                $ldapUser = $this->service->ldapGetUser($usuario);

                if (!$ldapUser) {
                    $this->session->set_flashdata("erro", "<script>swal('Usuário inválido!');</script>");
                    redirect('/Administrador/acesso/', 'refresh');
                    exit();
                }

                if (empty($usuario) || empty($acao)) {
                    $this->session->set_flashdata("erro", "<script>swal('Parametros inválidos!');</script>");
                    redirect('/Administrador/acesso/', 'refresh');
                    exit();
                }

                switch ($acao) {
                    case "ad":

                        $dados = array('usuario' => $usuario);

                        $ln = $this->admin->getAcessoDp($dados);


                        if (count($ln) > 0) {
                            $ln = $ln[0];
                            if ($this->admin->updateDp(array('id' => $ln['idusuario_dp'], 'status' => 'cf'))) {

                                $this->session->set_flashdata("erro", "<script>swal('Salvo!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alteração!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            }


                        } else {

                            if ($this->admin->insereDp((object)array('usuario' => $usuario, 'nome_colaborador' => $ldapUser['displayName'], 'status' => 'cf'))) {

                                $this->session->set_flashdata("erro", "<script>swal('Salvo!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alteração!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            }

                        }


                        break;

                    case "ag":

                        $dados = array('usuario' => $usuario);

                        $ln = $this->admin->getAcessoGestor($dados);

                        if (count($ln) > 0) {
                            if ($this->admin->updateGestor(array('id' => $ln[0]['idgestor'], 'status' => 'cf'))) {

                                $this->session->set_flashdata("erro", "<script>swal('Salvo!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alteração!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            }


                        } else {

                            if ($this->admin->insereGestor((object)array('usuario' => $usuario, 'nome_colaborador' => $ldapUser['displayName'], 'status' => 'cf'))) {

                                $this->session->set_flashdata("erro", "<script>swal('Salvo!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alteração!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            }

                        }


                        break;

                    case "nag":

                        $dados = array('usuario' => $usuario);

                        $ln = $this->admin->getAcessoGestor($dados);

                        $ln = $ln[0];

                        if (count($ln) > 0) {

                            if ($this->admin->updateGestor(array('id' => $ln['idgestor'], 'status' => 'ca'))) {

                                $this->session->set_flashdata("erro", "<script>swal('Salvo!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alteração!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            }


                        } else {

                            if ($this->admin->insereGestor((object)array('usuario' => $usuario, 'nome_colaborador' => $ldapUser['displayName'], 'status' => 'ca'))) {

                                $this->session->set_flashdata("erro", "<script>swal('Salvo!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alteração!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            }

                        }

                        break;

                    case "nad":

                        $dados = array('usuario' => $usuario);

                        $ln = $this->admin->getAcessoDp($dados);

                        $ln = $ln[0];

                        if (count($ln) > 0) {

                            if ($this->admin->updateDp(array('id' => $ln['idusuario_dp'], 'status' => 'ca'))) {

                                $this->session->set_flashdata("erro", "<script>swal('Salvo!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alteração!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            }


                        } else {

                            if ($this->admin->insereDp((object)array('usuario' => $usuario, 'nome_colaborador' => $ldapUser['displayName'], 'status' => 'ca'))) {

                                $this->session->set_flashdata("erro", "<script>swal('Salvo!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            } else {

                                $this->session->set_flashdata("erro", "<script>swal('Erro ao salvar alteração!');</script>");
                                redirect('/Administrador/acesso', 'refresh');
                                exit();

                            }

                        }

                        break;

                }

            } else {
                $form_name = $this->Csrf->form_names($names, true);
                redirect('/Administrador/acesso', 'refresh');
            }
        } else {
            $form_name = $this->Csrf->form_names($names, true);
            redirect('/Administrador/acesso', 'refresh');
        }


    }


    public function verifica_existe_filial()
    {

    }

    public function alteraInterlavoData()
    {

        $this->load->model("DAO/AdminDAO", "admin");

        $names = $this->names->getNameAcesso();

        $form_name = $this->Csrf->form_names($names, false);

        if (isset($_POST[$form_name['diaanterior']], $_POST[$form_name['diaposterior']], $_POST[$form_name['filial']])) {

            if ($this->Csrf->check_valid('post')) {

                $filial = $this->input->post($form_name["filial"]);
                $dt_ant = $this->input->post($form_name["diaanterior"]);
                $dt_pos = $this->input->post($form_name["diaposterior"]);


                if (!$this->global->isFilial($filial)) {
                    $this->session->set_flashdata("aviso", "<script>swal('Filial inválida!');</script>");
                    redirect('/Administrador/acesso', 'refresh');
                }

                if (!is_numeric($dt_ant) || !is_numeric($dt_pos)) {
                    $this->session->set_flashdata("aviso", "<script>swal('Apenas numeros!');</script>");
                    redirect('/Administrador/acesso', 'refresh');
                }

                $dia = (object)array(
                    'filial' => $filial,
                    'qnt_inicio' => $dt_ant,
                    'qnt_fim' => $dt_pos
                );

                if (count($this->admin->getExisteFilialInterval(trim($dia->filial)))) {

                    $this->admin->updateDataInterva($dia);
                    $this->session->set_flashdata("aviso", "<script>swal('Concluido!');</script>");
                    redirect('/Administrador/acesso', 'refresh');
                    var_dump('Alterado!');

                } else {
                    if ($this->admin->insereDataInterval($dia)) {
                        $this->session->set_flashdata("aviso", "<script>swal('Concluido!');</script>");

                        redirect('/Administrador/acesso', 'refresh');
                    } else {
                        $this->session->set_flashdata("aviso", "<script>swal('Não foi possivel fazer a alteração!');</script>");

                        redirect('/Administrador/acesso', 'refresh');
                    }
                }


            }

        }

    }

    public function deletaInterlavoData()
    {
        $this->load->model("DAO/AdminDAO", "admin");


                $filial = $this->input->post("filial");

                if (!$this->global->isFilial($filial)) {
                    $this->session->set_flashdata("aviso", "<script>swal('Filial inválida!');</script>");
                    redirect('/Administrador/acesso', 'refresh');
                }


                $data = (object)array(
                    'filial' => $filial
                );

                if (count($this->admin->getExisteFilialInterval(trim($data->filial)))) {

                    if ($this->admin->deletaDataInterval(trim($data->filial))){

                        $this->session->set_flashdata("aviso", "<script>swal('Concluido!');</script>");
                        redirect('/Administrador/acesso', 'refresh');

                    }else{

                        $this->session->set_flashdata("aviso", "<script>swal('Não foi possivel deletar!');</script>");
                        redirect('/Administrador/acesso', 'refresh');

                    }


                } else {

                    $this->session->set_flashdata("aviso", "<script>swal('Filial inválida!');</script>");
                    redirect('/Administrador/acesso', 'refresh');

                }


    }


}
