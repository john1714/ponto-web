<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PontoWeb extends CI_Controller {

    private $data = array();

    public function __construct()
    {	date_default_timezone_set('America/Fortaleza');
        parent::__construct();
        $this->load->model('Csrf');
        //------------------------- SCRIPT ANTI RUFINO --------------------------------
	
        if(isset($_SESSION['session'])){
            $sess = $_SESSION['session'];

            if(!isset($sess['nome']) || !isset($sess['filial']) || !isset($sess['usuario']) || !isset($sess['em'])){
                $this->session->unset_userdata('session');
                echo 'No direct script access allowed';
                redirect('/login','refresh');
            }

            if($sess['token'] == $this->Csrf->get_token()){

                $this->data['usuario'] = $_SESSION['session'];
                $this->data['token_id']    = $this->Csrf->get_token_id();
                $this->data['token_value'] = $this->Csrf->get_token( $this->Csrf->get_token_id() );
                $this->data['body'] = "";
                $this->data['title'] = $this->data['usuario']['nome']." | Bem-Vindo";

            }else{

                $this->session->unset_userdata('session');
                echo 'No direct script access allowed';
                redirect('/login','refresh');


            }
        }else{
            $this->session->unset_userdata('session');
            echo 'No direct script access allowed';
            redirect('/login','refresh');

        }

        //$this->load->model("DAO/UsuarioDAO","usuario");

    }

    public function index()
    {
        $this->load->model("Functions","function");
        $this->load->model("DAO/GlobalLocalDAO","globall");

        $this->data['permissao_bool'] = $this->function->verifica_permissao_geral();
        $this->data['ip'] 			  = $this->function->getIp();
        $this->data['filialAuth'] 	  = $this->function->getFilialAuth();

        if($this->session->flashdata('erro')){
            $this->data['erro']       = $this->session->flashdata('erro');
        }

        $data = $_SESSION['session'];
        $this->load->view('ponto/index.php',$this->data);
    }

    public function listabatidas(){

        if($this->input->method() != 'get'){
            echo "Operação não permitida";
            exit();
        }

        $this->load->model('DAO/PontoDAO');

        $this->PontoDAO->setUsuario($this->data['usuario']['usuario']);

        $diames = $this->input->get('val');

        switch ($diames) {
            case 'dia':
                $dados = $this->PontoDAO->listaBatidasDia();
                break;

            case 'mes':
                $dados = $this->PontoDAO->listaBatidasMes();
                break;

            default:
                die("Operação inválida!");
                break;
        }

        $data['list'] = $dados;

        $this->load->view('ponto/table-batidas.php', $data);

    }


    public function ponto(){

        $this->load->model('Csrf');
        try {
            if ($this->Csrf->check_valid('post')) {

                $this->load->model('DAO/PontoDAO');
                $this->load->model('Service');
                $this->load->model('Functions');
                $this->load->model('DAO/UsuarioDAO', 'usuario');


                if (!$this->Functions->verifica_permissao_geral()) {
                    $this->session->set_flashdata("erro", "<script>swal('Operação não permitida','Bata o ponto da maneira convêncinal.');</script>");
                    redirect('/PontoWeb', 'refresh');
                    exit();
                }

                if (!isset($this->data['usuario']['usuario']) || empty($this->data['usuario']['usuario'])) {
                    $this->session->set_flashdata("erro", "<script>swal('Operação não permitida','Erro ao recuperar seu usuário.');</script>");
                    redirect('/PontoWeb', 'refresh');
                    exit();
                }

                if (!isset($this->data['usuario']['filial']) || empty($this->data['usuario']['filial'])) {
                    $this->session->set_flashdata("erro", "<script>swal('Operação não permitida','Erro ao recuperar sua filial.');</script>");
                    redirect('/PontoWeb', 'refresh');
                    exit();
                }

                if (!isset($this->data['usuario']['em']) || empty($this->data['usuario']['em'])) {
                    $this->session->set_flashdata("erro", "<script>swal('Operação não permitida','Bata o ponto da maneira convêncinal.');</script>");
                    redirect('/PontoWeb', 'refresh');
                    exit();
                }

                $em = $this->data['usuario']['em'];
                $usuario = $this->data['usuario']['usuario'];

                // Verifica interlado de hora
                $this->PontoDAO->setUsuario($usuario);
                $interval = $this->PontoDAO->batidaIntervaloHora();

                if ($interval['success'] == false) {
                    $this->session->set_flashdata("erro", "<script>swal('" . $interval['message'] . "');</script>");
                    redirect('/PontoWeb', 'refresh');
                    exit();
                }

                // RECUPERA PIS
                $pis = $this->usuario->getPis($em);
                if (empty(trim($pis))) {
                    $this->session->set_flashdata("erro", "<script>swal('Não foi possivel recuperar seu PIS');</script>");
                    redirect('/PontoWeb', 'refresh');
                    exit();
                }

                $dados = $this->Service->ldapGetUserEm($em);

                $obj = array();
                $obj = (object)$obj;

                $obj->nome = $dados["displayName"];
                $obj->usuario = $dados["sAMAccountName"];
                $obj->filial_origem = $dados["filial"];
                $obj->filial_autenticado = $this->Functions->getFilialAuth();
                $obj->ip = $this->Functions->getIp();
                $obj->data_ = date("Y-m-d");
                $obj->hora = date("H:i:s");
                $obj->status = "PD";
                $obj->pis = $pis;//$pis;
                $obj->em = $this->Functions->removeEm($dados["matricula"]);

                if ($this->PontoDAO->inserePonto($obj)) {
                    $this->session->set_flashdata("erro", "<script>swal('Batida Efetuada!');</script>");
                    redirect('/PontoWeb', 'refresh');
                } else {
                    $this->session->set_flashdata("erro", "<script>swal('Erro','Não foi possivel efetuar a batida de ponto!');</script>");
                    redirect('/PontoWeb', 'refresh');
                }

            } else {
                $this->session->set_flashdata("erro", "<script>swal('Erro','Erro interno.');</script>");
                redirect('/PontoWeb', 'refresh');
            }
        }catch (Exception $exception){
            $this->session->set_flashdata("erro", "<script>swal('". $exception->getMessage() ."');</script>");
            redirect('/PontoWeb', 'refresh');
            exit();
        }

    }


}

?>
