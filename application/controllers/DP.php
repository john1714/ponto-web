<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DP extends CI_Controller {

	private $data = array();
	//private $form_names = null;

	public function __construct()
	{

		parent::__construct();

		$this->load->model('Csrf');

		if(isset($_SESSION['session'])){
			$sess = $_SESSION['session'];

			$this->sess = $_SESSION['session'];

			if($sess['token'] == $this->Csrf->get_token() && $sess['permissao'] == 'l2'){

				$this->load->model('Functions');
				$this->load->model("Alert");
				$this->load->model("InputNames","names");

				$this->data['em']         = $sess['em'];
				$this->data['nome']       = $sess['nome'];
				$this->data['usuario']    = $sess['usuario'];
				$this->data['filial']     = "F03";
				$this->data['body'] 	 	= "";
				$this->data['title']	    = "Bem-Vindo";

			}else{

				$this->session->unset_userdata('session');
				echo 'No direct script access allowed';
				redirect('/login','refresh');


			}
		}else{
			$this->session->unset_userdata('session');
			echo 'No direct script access allowed';
			redirect('/login','refresh');

		}


	}

	public function index()
	{
		$this->load->model('DAO/GlobalDAO','global');
		$this->load->model("DAO/DpDAO","dp");
		$this->load->model("Modal","modal");

		$data['nome_'] = $this->data['nome'];
		$data['em_'] = $this->data['em'];
		$data['filial_'] = $this->data['filial'];

		$data['filiais'] = $this->global->getFiliais();

		$data['nome']   = $this->input->post("nome");
		$data['data1']  = $this->input->post("data1");
		$data['data2']  = $this->input->post("data2");
		$data['filial'] = $this->input->post("filial");
		$data['tabela'] = $this->listaBatidas();

		$this->load->view("dp/view-header.php");

		if($this->session->flashdata('exporta') == "true"){
			$data['exporta'] = 'true';
		}


		if(isset($_GET['d']) && $_GET['d'] == 'true'){
            $inputs = $this->input->post();
            $dataini = str_replace("/","-",$inputs['data1']);
            $datafim = str_replace("/","-",$inputs['data2']);
            $prefix = $dataini.'_ate_'.$datafim.'_';
			$this->liberaDownload($prefix);
		}

		$this->load->view("dp/view-top-bar.php",$data);

		if(isset($_POST['exp'])){

			if(empty($_POST['data1']) || empty($_POST['data2'])){
				$data['erro'] = $this->modal->modal(array('body' => 'Selecione o intervalo de datas.'));
				$this->load->view("dp/view-listabatidas.php",$data);
			}else{

				$ln = $this->verificaResultadoSelect();

				if(!$ln){

					$data['erro'] = $this->modal->modal(array('body' => 'Não há dados para exportar'));

				}else{
					$this->exporta($ln, $this->input->post());
					$this->compacta($this->input->post());
					//$this->liberaDownload();
					$this->session->set_flashdata('exporta', 'true');
					redirect("dp","refresh");
				}

					$this->load->view("dp/view-listabatidas.php",$data);
			}

		}else{
			$this->load->view("dp/view-listabatidas.php",$data);
		}


		$this->load->view("dp/view-footer.php",$data);
	}


	public function liberaDownload($prefix){
		$local = APPPATH."arquivos/{$prefix}relatorio.zip";
		$data = file_get_contents($local); // Read the file's contents
		$name = "{$prefix}relatorio.zip";
		force_download($name, $data);
	}

	public function compacta($inputs){
		$this->load->model("Ponto","ponto");

		$dataini = str_replace("/","_",$inputs['data1']);
        $datafim = str_replace("/","_",$inputs['data2']);
		$prefix = $dataini.'_ate_'.$datafim.'_';

		$this->ponto->newZip($prefix);
		$this->ponto->zipar($prefix);
		$this->liberaDownload($prefix);
	}

	public function listaBatidas(){
		$sql = $this->dp->prepare_query($this->input->post());

		$ln  = $this->dp->getBatidasQuery($sql);

		return $this->dp->view_table_ponto($ln);
	}

	public function verificaResultadoSelect(){
		$sql = $this->dp->prepare_query($this->input->post());

		$ln = $this->dp->getBatidasQuery($sql);

		if(count($ln)){
			return $ln;
		}else{
			return false;
		}
	}

	public function exporta($ln, $post){

		$this->load->model("Ponto","ponto");

		$this->ponto->dell();

		$this->ponto->gerar_txt($ln, $post);

	}


	public function deletaponto(){
		$this->load->model("DAO/PontoDAO","ponto");

		$id = $this->input->post("id");

		if(empty($id) || !is_numeric($id)){
			echo json_encode(array('success' => 'false', 'message' => 'Id inválido!'));
			exit();
		}

		if($this->ponto->atualizaStatus($id, 'EX')){

			echo json_encode(array('success' => 'true', 'message' => 'Efetuado com sucesso'));

		}else{

			echo json_encode(array('success' => 'false', 'message' => 'Erro ao excluir registro!'));

		}

	}
}
?>
