
 <!DOCTYPE html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $title ?></title>

    <!-- Bootstrap -->
    <link href="<?= base_url('bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('bootstrap/css/jquery-ui.theme.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('bootstrap/css/jquery-ui.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('bootstrap/css/mtimepicker.css')?>" rel="stylesheet">
    <script src="<?= base_url('bootstrap/js/jquery.min.js')?>" ></script>
    <script src="<?= base_url('bootstrap/js/bootstrap.min.js')?>" ></script>
    <script src="<?= base_url('bootstrap/js/jquery-ui.min.js')?>" ></script>
    <script src="<?= base_url('bootstrap/js/mtimepicker.js')?>" ></script>

    <style type="text/css">
      .hora_completa{
        border: 1px solid #666;
        background-color: transparent;
      }

      #hora{
        background-color: transparent;
      }
      #hora_1{
        background-color: transparent;
      }
      #hora_2{
        background-color: transparent;
      }
      #hora_3{
        background-color: transparent;
      }
      #hora_4{
        background-color: transparent;
      }

      #link-right > li > a{
        color: #FFF;
        font-weight: 800;
        margin-top: 3px;
      }

      #link-right > li > a:hover{
        color: #F2F2F2;
        font-weight: 800;
        margin-top: 3px;
      }

      .btn-custom{
        background: linear-gradient(to bottom, #888, #555);
        color: #FFFFFF;
        border: solid 1px #888;
        padding: 10px 30px;
      }

      .btn-custom:hover{
        background: linear-gradient(to bottom, #777, #555);
        border: solid 1px #888;
        color: #FFFFFF;
      }

      .btn-custom:active{
       background: linear-gradient(to bottom, #888, #555);
       color: #FFFFFF;
       border: solid 1px #888;
      }

      .btn-custom:focus{
       background: linear-gradient(to bottom, #888, #333);
       color: #FFFFFF;
       border: solid 1px #666666;
      }
    </style>



    <script>
        function eventFire(el, etype){
            if (el.fireEvent) {
                el.fireEvent('on' + etype);
            } else {
                var evObj = document.createEvent('Events');
                evObj.initEvent(etype, true, false);
                el.dispatchEvent(evObj);
            }
        }


            $(document).ready(function(){
                $("select option").click();
                var enableDays = <?= $dataintervalo ?>;

      function enableAllTheseDays(date) {
          var sdate = $.datepicker.formatDate( 'dd-mm-yy', date)
          if($.inArray(sdate, enableDays) != -1) {
              return [true];
          }
          return [false];
      }

      $('.calendar').datepicker({dateFormat: 'dd-mm-yy', beforeShowDay: enableAllTheseDays});


              $('#hora')  .mTimePicker().mTimePicker( 'setTime', '08:00' );
              $('#hora_1').mTimePicker().mTimePicker( 'setTime', '08:00' );
              $('#hora_2').mTimePicker().mTimePicker( 'setTime', '12:00' );
              $('#hora_3').mTimePicker().mTimePicker( 'setTime', '13:12' );
              $('#hora_4').mTimePicker().mTimePicker( 'setTime', '18:00' );

              $('#camp-busca-colaborador').keyup(function () {
                    var valthis = $(this).val().toLowerCase();
                    var num = 0;
                    $('select#sel-colaborador>option').each(function () {
                        var text = $(this).text().toLowerCase();
                        if(text.indexOf(valthis) !== -1)
                            {$(this).show(); $(this).prop('selected');}
                        else{$(this).hide();}
                    });
              });

              $("#checkjus").click(function(){
                  if( $(this).is(":checked")){
                     $("#row-horarios").slideDown();
                     $("#hora").animate({'backgroundColor':'#ddd'});
                     $("#hora").prop('disabled',true);
                  }else{
                    $("#row-horarios").slideUp();
                    $("#hora").animate({'backgroundColor':'transparent'});
                     $("#hora").prop('disabled',false);
                  }
              });

              $("select#sel-colaborador").change(function(){
                 $("#camp-nome").val($("select#sel-colaborador option:selected").data("nome"));
                 $("#camp-em").val($("select#sel-colaborador option:selected").data("em"));
                 $("#camp-filial").val($("select#sel-colaborador option:selected").data("filial"));
              });

              $("select#sel-motivo").change(function(){
                 $("#camp-motivo").val($("select#sel-motivo option:selected").data("motivo"));
              });

            });


    </script>


  </head>
  <body style="margin-top: 70px;">

 <nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
   <div class="navbar-header">

    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
     <span class="sr-only">Toggle navigation</span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?= base_url("/Gestor") ?>">
        <img alt="Brand" style="margin-top: -16px;" width="100" height=55" src="<?= base_url('public/img/logobranco.png') ?>" />
    </a>
    <p class="navbar-text" style="color:#FFF; margin-top: 17px;"><strong><?= $em ?> / <?= $nome ?> / <?= $filial ?></strong></p>
   </div>
   <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right" id="link-right">
         <?php if($catg == 'gs'): ?>
             <li><a href="<?= base_url("Gestor/justificativa-colaborador") ?>" color>Justificativa do Colaborador</a></li>
             <li><a href="<?= base_url("Gestor/colaborador") ?>" color>Batida Colaborador</a></li>
             <li><a href="<?= base_url("Gestor/justificativas") ?>" color>Minhas Justificativa</a></li>
         <?php endif; ?>
        <?php if($catg == 'cl'): ?>
            <li><a id="a" href="<?= base_url("Gestor/justificativa-colaborador") ?>" color>Minhas Justificativa</a></li>
        <?php endif; ?>
        <?php if($catg == 'dp'): ?>
            <li><a href="<?= base_url("Gestor/colaborador") ?>" color>Batida Colaborador</a></li>
            <li><a href="<?= base_url("Gestor/justificativas") ?>" color>Minhas Justificativa</a></li>
        <?php endif; ?>
         <li><a href="<?= site_url('login/sair') ?>">Sair</a></li>
    </ul>
   </div>
  </div>
 </nav>

 <div class="container-fluid">

  <form action="<?= base_url('Gestor/justifica') ?>" method="post">
    <?php  if (isset($aviso)): ?>
        <div class="row">
          <?php foreach ($aviso as $msg): ?>
              <div class="<?= $msg['class'] ?>">
                <a href="" class="close" data-dismiss="alert" arial-label="close">&times;</a>
                <?= $msg['message'] ?>
              </div>
          <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <div class="row">
      <div class="col-md-4">

        <div class="form-group">
          <label for="exampleInputEmail1">Colaboradores</label>
          <input type="text" class="form-control" id="camp-busca-colaborador" placeholder="Busque aqui..">
          <select class="form-control" multiple name="<?= $form_names['em'] ?>" id="sel-colaborador" style="height:160px;" required="require">
            <?php foreach ($colaboradores as $col): ?>
            	<?php if( isset($col['matricula']) && isset($col['filial']) && isset($col['displayName'])  ){ ?>
                     <option data-em="<?= $col['matricula'] ?>" value="<?= $col['matricula'] ?>" data-nome="<?= $col['displayName'] ?>" data-filial="<?= $col['filial'] ?>"><?= $col['displayName'] ?></option>
            	<?php } ?>
            <?php endforeach; ?>
          </select>
        </div>

      </div>
      <div class="col-md-8">
        <div class="row">
            <div class="form-group col-md-3">
                <label for="">Em</label>
                <input type="text" class="form-control" id="camp-em" disabled="disable">
            </div>

            <div class="form-group col-md-7">
                <label for="">Nome</label>
                <input type="text" class="form-control" id="camp-nome" disabled="disable">
            </div>

            <div class="form-group col-md-2">
                <label for="">Filial</label>
                <input type="text" class="form-control" id="camp-filial" disabled="disable">
            </div>
        </div>

        <div class="row">

            <div class="form-group col-md-3">
                <label for="exampleInputEmail1">Data</label>
                <input type="text" class="form-control calendar" name="<?= $form_names['data'] ?>" id="" placeholder="00/00/0000" required="require" readonly="readonly">
            </div>

            <div class="form-group col-md-2">
                <label for="exampleInputEmail1">Hora</label>
                <input type="text" class="form-control" name="<?= $form_names['hora'] ?>" id="hora" placeholder="00:00" required="require">
            </div>
            <div class="form-group col-md-4" style="margin-top: 18px;">
              <div class="checkbox">
               <label><input type="checkbox" name="<?= $form_names['check'] ?>" id="checkjus"><strong>Justifica Batida Dia Todo</strong></label>
              </div>
            </div>

        </div>

        <div class="row" id="row-horarios" style="display: none;">
          <div class="form-group col-md-3">
            <label for="hora_1">1º batida</label>
            <input type="text" class="form-control hora_completa hora"  name="<?= $form_names['hora1'] ?>" id="hora_1" placeholder="" required="require" >
          </div>
          <div class="form-group col-md-3">
              <label for="hora_2">2º batida</label>
              <input type="text" class="form-control hora_completa hora" name="<?= $form_names['hora2'] ?>" id="hora_2" placeholder="" required="require">
          </div>
          <div class="form-group col-md-3">
            <label for="hora_3">3º batida</label>
            <input type="text" class="form-control hora_completa hora" name="<?= $form_names['hora3'] ?>" id="hora_3" placeholder="" required="require">
          </div>
          <div class="form-group col-md-3">
            <label for="hora_4">4º batida</label>
            <input type="text" class="form-control hora_completa hora" name="<?= $form_names['hora4'] ?>" id="hora_4" placeholder="" required="require">
          </div>
      </div>

      </div>
      <hr />


  </div>

  <div class="row">
      <div class="form-group col-md-4">
        <label for="">Motivo</label>
         <select class="form-control" name="<?= $form_names['motivo'] ?>" id="sel-motivo" required="require">
         <option value="" >Seleciona um motivo</option>
         <?php foreach ($motivos as $val): ?>

            <option value="<?= $val['descricao'] ?>" data-motivo="<?= $val['descricao'] ?>"><?= $val['tipo'] ."-". $val['descricao'] ?></option>

          <?php endforeach; ?>
          </select>
      </div>
    <div class="form-group col-md-8">
        <label for="">-</label>
        <input type="text" class="form-control" id="camp-motivo" disabled="disable" required="require">
    </div>
    <br /><br /><br /><br />
    <div class="form-group col-md-12">
        <label for="">Observação</label>
        <input type="text" class="form-control" name="<?= $form_names['obs'] ?>" id="camp-observacao" required="require" >
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <button type="submit" class="btn btn-custom">Salvar</button>
    </div>
  </div>
  <input type="hidden" name="<?= $token_id; ?>" value="<?= $token_value; ?>" />
  </form>
 </div>



</body>

</html>
