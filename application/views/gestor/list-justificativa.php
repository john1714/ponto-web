<?php
#var_dump($colaboradores);
#exit();
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $title ?></title>

    <!-- Bootstrap -->
    <script src="<?= base_url('bootstrap/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('bootstrap/js/datatables.min.js') ?>"></script>

    <link href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('bootstrap/css/jquery.dataTables.min.css') ?>" rel="stylesheet">


    <link href="<?= base_url('bootstrap/css/jquery-ui.min.css') ?>" rel="stylesheet">
    <script src="<?= base_url('bootstrap/js/jquery-ui.min.js') ?>"></script>
    <style type="text/css">
        .hora_completa {
            border: 1px solid #666;
            background-color: transparent;
        }

        #hora {
            background-color: transparent;
        }

        #hora_1 {
            background-color: transparent;
        }

        #hora_2 {
            background-color: transparent;
        }

        #hora_3 {
            background-color: transparent;
        }

        #hora_4 {
            background-color: transparent;
        }

        #link-right > li > a {
            color: #FFF;
            font-weight: 800;
            margin-top: 3px;
        }

        #link-right > li > a:hover {
            color: #F2F2F2;
            font-weight: 800;
            margin-top: 3px;
        }
    </style>


    <script>


    </script>


</head>
<body style="margin-top: 80px;">

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url("/Gestor") ?>">
                <img alt="Brand" style="margin-top: -16px;" width="100" height=55"
                     src="<?= base_url('public/img/logobranco.png') ?>"/>
            </a>
            <p class="navbar-text" style="color:#FFF; margin-top: 17px;"><strong><?= $em ?> / <?= $nome ?>
                    / <?= $filial ?></strong></p>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right" id="link-right">
                <li><a href="<?= base_url("Gestor") ?>">Painel Justificativa</a></li>
                <li><a href="<?= site_url('login/sair') ?>">Sair</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div style="position: fixed; width: 100%; top:50px; padding: 30px; background-color: #FFF; z-index: 999;">
        <div class="row">
            <form class="form-inline col-md-12" action="<?= base_url('Gestor/justificativas') ?>" method="post">

                <div class="form-group">
                    <label for="filial-sel">Filiais:</label>
                    <select class="form-control" id="filial-sel" name="filial">
                        <option value="">Nenhuma</option>
                        <?php foreach ($filiais as $val): ?>
                            <?php if ($form['filial'] == $val['iuorg']) { ?>
                                <option value="<?= $val['iuorg'] ?>" selected='selected'><?= $val['iuorg'] ?></option>
                            <?php } else { ?>
                                <option value="<?= $val['iuorg'] ?>"><?= $val['iuorg'] ?></option>
                            <?php } ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="sel1">Status:</label>
                    <select class="form-control" id="filial-sel" name="status">
                        <?php foreach ($status_ as $val): ?>
                            <?php if ($form['status'] == $val['status']) { ?>
                                <option value="<?= $val['status'] ?>" selected='selected'><?= $val['nome'] ?></option>
                            <?php } else { ?>
                                <option value="<?= $val['status'] ?>"><?= $val['nome'] ?></option>
                            <?php } ?>

                        <?php endforeach; ?>
                    </select>
                </div>

                <?php if($catg == 'dp'){ ?>

                    <div class="form-group">
                        <label for="text">Nome</label>
                        <input type="text" class="form-control" name="nome"
                               value="<?= isset($form['nome']) ? $form['nome'] : "" ?>">
                    </div>

                <?php } else { ?>

                <div class="form-group">
                    <select class="form-control" multiple name="nome" id="sel-colaborador" style="height:80px;">
                        <?php foreach ($colaboradores as $col): ?>
                            <?php if( isset($col['matricula']) && isset($col['filial']) && isset($col['displayName'])  ){ ?>
                                <option value="<?= $col['matricula'] ?>" data-nome="<?= $col['displayName'] ?>" data-filial="<?= $col['filial'] ?>"><?= $col['displayName'] ?></option>
                            <?php } ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <?php } ?>

                <div class="form-group">
                    <label for="text">Data Inicio</label>
                    <input type="text" class="form-control data" id="data-inicio" name="data1" autocomplete = "off"
                           value="<?= isset($form['data1']) ? $form['data1'] : "" ?>">
                </div>
                <div class="form-group">
                    <label for="text">Data Fim</label>
                    <input type="text" class="form-control data" id="data-fim" name="data2" autocomplete = "off"
                           value="<?= isset($form['data2']) ? $form['data2'] : "" ?>">
                </div>

                <button type="submit" class="btn btn-default">Buscar</button>

            </form>
        </div>
    </div>
    <br/>
    <div id="list" class="row" style="margin-top: 45px;">

        <div class="table-responsive col-md-12">
            <table class="table table-striped" id="table" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th>EM</th>
                    <th>Nome</th>
                    <th>Filial</th>
                    <th>Data</th>
                    <th>Data Registro</th>
                    <th>Hora</th>
                    <th>Motivo</th>
                    <th>Observação</th>
                    <th>Ações</th>

                </tr>
                </thead>
                <tbody>
                    <?php
                        echo (isset($justificativas)) ? $justificativas : '';
                    ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /#list -->


</div>

<!-- Modal -->
<div id="modal-feed" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-info">Feedback</h4>
            </div>
            <div class="modal-body modal-feed-body">

            </div>
        </div>

    </div>
</div>


</body>

</html>
<script>
    function openModalFeed(id = ''){
        if(id == ''){
            return;
        }
        $.post('vieweditafeed/', {id: id}, function (retorno) {
            $(".modal-feed-body").html(retorno);
        });
        $("#modal-feed").modal({backdrop: 'static'});
    }

    function recusar(id) {

        $.ajax({
            type: "POST",
            url: "<?= base_url('Gestor/recusar') ?>",
            data: {id: id},
            dataType: "json",
            beforeSend: function () {
                var tr = $("#tr_" + id + " .img-load").show();
                console.log('asdasd');
            },
            success: function (data) {
                var tr = $("#tr_" + id + " .img-load").hide();
                if (data.success == "true") {

                    var tr = $("#tr_" + id);

                    var trAcao = tr.find('td:eq(8)');

                    trAcao.html(data.input.acao);

                    openModalFeed(id);

                }
            }, error : function(data) {
                console.log(data);
            }

        });
    }


    function aprovar(id) {

        $.ajax({
            type: "POST",
            url: "<?= base_url('Gestor/aprovar') ?>",
            data: {id: id},
            dataType: "json",
            beforeSend: function () {
                console.log('carregando...');
                var tr = $("#tr_" + id + " .img-load").show();
            },
            success: function (data) {
                console.log('ok');

                var tr = $("#tr_" + id + " .img-load").hide();

                if (data.success == "true") {

                    var tr = $("#tr_" + id);

                    var trAcao = tr.find('td:eq(8)');

                    trAcao.html(data.input.acao);

                }

            }

        });
    }


    function excluir(id) {


        $.ajax({
            type: "POST",
            url: "<?= base_url('Gestor/excluir') ?>",
            data: {id: id},
            dataType: "json",
            beforeSend: function () {
                var tr = $("#tr_" + id + " .img-load").show();
            },
            success: function (data) {
                var tr = $("#tr_" + id + " .img-load").hide();
                var identf = data.identf;

                if (data.success == "true") {
                    $("#tr_" + identf).fadeOut();
                } else {
                    alert(data.message);
                }

            }

        });
    }

    $(document).ready(function () {

        $("body").on('click', ".bt-feed", function () {
            var id = $(this).attr('data-id');  // pega o id do botão

            $.post('vieweditafeed/', {id: id}, function (retorno) {
                $(".modal-feed-body").html(retorno);
            });

            $("#modal-feed").modal({backdrop: 'static'});

        });

        $(".data").datepicker({dateFormat: 'dd/mm/yy'});

        $('body').on('click', '.jus-info', function () {
            $(".modal-feed-body").html("<h4>" + $(this).data('info') + "</h4>");
            $("#modal-feed").modal({backdrop: 'static'});
        })

        $('#table').DataTable({
            "scrollY": "370px",
            "scrollCollapse": true,
            "paging": true,
            "order": [[0, "desc"]],
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            }

        });


    });

</script>
