	<form action="" id="form-feed">
		<div class="form-group">
		  <label for="comment">Descreva motivo sobre a recusação da justificativa</label>
		  	<textarea class="form-control" rows="5" name="feedback" id="comment"><?= isset($feedbd) ? $feedbd : "" ?></textarea>
		  </div>
		  <input type="hidden" value="<?= isset($id) ? $id : "" ?>" name="id">
		<button type="button" class="btn btn-info" id="btn-salva-feed">Salvar</button>
		<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
		
		<img style="display:none;" class="img-load-feed" width="70" height="15" src="<?= base_url('public/img/30.gif') ?>" />
		<span id="feedretorno"></span>
	</form>
	
	<script>
		var url = "<?= base_url("Gestor/editarFeedback") ?>";
		
		$("#btn-salva-feed").click(function(){
				$.ajax({
					type : "POST",
					 url : url,
					data : $("#form-feed").serialize(),
				dataType : "json",
				beforeSend : function(){
					$(".img-load-feed").show();
				},
				success : function(data){
					$(".img-load-feed").hide();

					if(data.success == 'true'){
						$("#feedretorno").html("<strong>Salvo!</strong>");
						$("#modal-feed").modal("toggle");
					}else{
						$("#feedretorno").html("<strong>"+data.message+"</strong>");
					}
					
				}
			});
		});
	</script>