<header>
	<div class="container-fluid">
		<div class="row" id="content-1">
			<div class="col-md-6" id="text-content-1">
				<img src="<?= base_url("public/img/logobranco.png") ?>" width="100" style="margin-top:-40px;" />
				<label><p><?= $em_ ?></p></label>
				<label><p> - <?= $nome_ ?></p></label>
				<label><p> - <?= $filial_ ?></p></label>
			</div>
			<div class="col-md-6" id="text-content-2">
				<a href="<?= base_url("login/sair") ?>" class="btn btn-default" style="color: #000;">Sair</a>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12 col-md-offset-1">
			 <form  action="<?= base_url('dp') ?>" method="post">
			  	
			  	<div class="col-md-1 col-sm-1 col-xs-1" >
				  <label for="sel1">Filial:</label>
				  <select class="form-control" id="sel1" name="filial">
				      	        <option value="">Nenhuma</option>
			                <?php foreach ($filiais as $val): ?>
			

						<?php if($filial == $val['iuorg']){ ?>
	 					     	<option value="<?= $val['iuorg'] ?>" selected='selected'><?= $val['iuorg'] ?></option>
						   <?php }else{ ?>
							<option value="<?= $val['iuorg'] ?>"><?= $val['iuorg'] ?></option>
                                                	<?php } ?>

	        		        	
			                <?php endforeach;?>
				  </select>
				</div>	

				<div class="col-md-1 col-sm-1 col-xs-1" >
				  <label for="sel1">Status:</label>
				  <select class="form-control" id="sel1" name="status">
					<option value=''>Todos</option>
					<option value='PD'>Pendentes</option>
					<option value='CF'>Confirmados</option>
				  </select>
				</div>	

	         	 <div class="col-md-2 col-sm-2 col-xs-2">
				  <label for="ex1">Nome</label>
				  <input class="form-control" id="ex1" value="<?= isset($nome) ? $nome : "" ?>" type="text" name="nome">
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2">
				  <label for="ex2">Data inicial</label>
				  <input class="form-control data" id="ex2" value="<?= isset($data1) ? $data1 : "" ?>" type="text" name="data1" autocomplete = "off">
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2">
				  <label for="ex3">Data Final</label>
				  <input class="form-control data" id="ex3" value="<?= isset($data2) ? $data2 : "" ?>" type="text" name="data2" autocomplete = "off">
				</div>
				<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
				<?php if(isset($exporta)){?>
						<a href="<?= base_url('dp/?d=true') ?>" class="btn btn-default">Baixar</a>
				<?php } ?>
				<button type="submit" name="exp" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span>Exportar</button>
				<a href="javascript:void(0);" id="bar-toogle" class="btn btn-default"><span class="glyphicon glyphicon-resize-vertical" aria-hidden="true"></span></a>

	        </form>
				  
			</div>
		</div>
	</div>
	
</header>

