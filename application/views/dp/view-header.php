 <!DOCTYPE html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tratamento de batidas | DP</title>

    <!-- Bootstrap -->
    <link href="<?= base_url('bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('datatable/css/dataTables.bootstrap.min.css')?>" rel="stylesheet">



    <link href="<?= base_url('bootstrap/css/sweetalert.css')?>" rel="stylesheet">
    <link href="<?= base_url('bootstrap/css/style-dp.css')?>" rel="stylesheet">
    <script src="<?= base_url('bootstrap/js/jquery.min.js')?>" ></script>
    <script src="<?= base_url('bootstrap/js/bootstrap.min.js')?>" ></script>



    <script src="<?= base_url('bootstrap/datatable/jquery.dataTables.min.js')?>" ></script>
 	<script src="<?= base_url('bootstrap/datatable/dataTables.bootstrap.min.js')?>" ></script>
 	<script src="<?= base_url('bootstrap/js/bootstrap-confirmation.min.js')?>" ></script>
 	<script src="<?= base_url('bootstrap/js/sweetalert.min.js')?>" ></script>

 	 <link href="<?= base_url('bootstrap/css/jquery-ui.theme.min.css')?>" rel="stylesheet">
 	 <link href="<?= base_url('bootstrap/css/jquery-ui.min.css')?>" rel="stylesheet">
    <script src="<?= base_url('bootstrap/js/jquery-ui.min.js')?>" ></script>


    <script>
    function toggle(){
        var content1 = $("#content-1");
		content1.slideToggle("fast");
    }

    function deletaponto(ident){
        var url = "<?= base_url("dp/deletaponto") ?>";
		$.ajax({
			type : 'post',
			 url : url,
			data : {id:ident},
		dataType : "json",
		beforeSend : function(){

		},
		success : function(data){

			if(data.success == 'false'){
				console.log(data.message);
			}

			if(data.success == 'true'){
				$("#tr_"+ident).slideUp('slow');
				success_delete();
			}
		}

		});
    }

	function success_delete(){
		swal({
  		  title: "Deletado!",
  		  text: "Clique ou aguarde...",
  		  type: "success",
  		  timer: 2000,
  		  closeOnConfirm: true,
  		  showConfirmButton: true
  		});
	}

    $(document).ready(function(){

    	$(".data").datepicker({ dateFormat: 'dd/mm/yy' });

    	$('[data-toggle=confirmation]').confirmation({
    		  rootSelector: '[data-toggle=confirmation]',
    		  onConfirm   : function(test,teste) {
    			  deletaponto($(this).data('id'));
    		  }


    	});
    	//$('[data-toggle="confirmation"]').confirmation();

		var count = 0;
		$("#bar-toogle").click(function(){



			if(count == 0){

				$("#list").animate({marginTop: "100px"},function(){
					toggle();
				});
				count = 1;
				tableActions(535);
				return false;
			}

			if(count == 1){
				$("#list").animate({marginTop: "140px"},function(){
					toggle();
				});
				count = 0;
				tableActions(570);
				return false;
			}


		});

    	initTable();
    });

    function initTable () {
    	var header = $("header");

    	var headeraltura = header.height();
    	var browserHeight = $(window).height();
	//alert(headeraltura);
    	//alturaContent = 800;
    	alturaContent = browserHeight - headeraltura;

    	return $('#table').DataTable( {

    		 "destroy": true,
    		 "lengthMenu": [25, 50, 75, 100 ],
             "scrollY":  alturaContent+"px",
             "scrollCollapse": true,
             "paging":         true,
             "order": [[ 0, "desc" ]],
             "language": {
               "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
              }


         } );
    }

       function tableActions (altura) {
    	  var table = initTable();
    	  var browserHeight = $(window).height() + 300;

    	  alturaContent = browserHeight - altura;

    	  table = $('#table').DataTable( {
     		 "destroy": true,
     		"lengthMenu": [25, 50, 75, 100 ],
     		 "scrollY":  alturaContent+"px",
              "scrollCollapse": true,
              "paging":         true,
              "order": [[ 0, "desc" ]]

          } );

    	}

    </script>

 </head>

 <body>
