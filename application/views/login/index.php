 <!DOCTYPE html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $title ?></title>
    
    <!-- Bootstrap -->
    <link href="<?= base_url('bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('bootstrap/css/style-custom.css')?>" rel="stylesheet">
    <script src="<?= base_url('bootstrap/js/jquery.min.js')?>" ></script>
    <script src="<?= base_url('bootstrap/js/bootstrap.min.js')?>" ></script>
	<style>
		body {
    		
		}
	</style>
    <script>
            
            $(document).ready(function(){

            });

    </script>

    
  </head>
  <body class="">
 
<div class="line-blue"></div>
<div class="container">
	

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form class="form-s1" action="<?= base_url('login/logar') ?>" method="post"  >
              <div class="row">
              <center><img class="" width="240" src="<?= base_url('public/img/New-Logo-Ibyte-e-iOne-300-75d.png') ?>"></center>
              <br />
              </div>
              <div class="form-group">
                <label for="formGroupExampleInput">Usuário</label>
                <input type="text" class="form-control" name="<?= isset($form_names['user']) ? $form_names['user'] : '' ?>" id="formGroupExampleInput" placeholder="" required />
              </div>
              <div class="form-group">
                <label for="">Senha</label>
                <input type="password" class="form-control" name="<?= isset($form_names['password']) ? $form_names['password'] : '' ?>" id="" placeholder="" required />
              </div>
               <div class="radio">
				  <label><input type="radio" value="l1" name="<?= isset($form_names['opcao']) ? $form_names['opcao'] : '' ?>" checked="checked">Bater Ponto</label>
				</div>
				<div class="radio">
				  <label><input type="radio" value="l2" name="<?= isset($form_names['opcao']) ? $form_names['opcao'] : '' ?>">Departamento Pessoal</label>
				</div>
				<div class="radio">
				  <label><input type="radio" value="l3" name="<?= isset($form_names['opcao']) ? $form_names['opcao'] : '' ?>">Justificativa de Ponto</label>
				</div>
				<div class="radio">
				  <label><input type="radio" value="l4" name="<?= isset($form_names['opcao']) ? $form_names['opcao'] : '' ?>">Administrador</label>
				</div>
              <div class="form-group">
                <button type="submit" class="btn btn-danger btn-lg btn-block">Entrar</button>
              </div>
              <input type="hidden" name="<?= $token_id; ?>" value="<?= $token_value; ?>" />
	<center><i style="color:#888; font-size:12px;"><span style="font-weight:600; color:#999;">Ver:</span>4.0.1</i></center>
            </form>
            <div class="msg">
              <?php
                if(isset($aviso)){
	                  echo $aviso;
                }
              ?>
            </div>
        </div>
    </div>
</div>



</body>
</html>
