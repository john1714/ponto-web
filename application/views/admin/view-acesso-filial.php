<div class="container">
	<div class="col-md-4">
		<div class="col-md-12 content">
				<form action="<?= base_url("Administrador/permissao-filial/salva") ?>" method="post" >
					 <div class="form-group">
					  <label for="sel1">Filiais:</label>
					  <select class="form-control" name="<?= $form_names["filial"] ?>" id="" required>
					    <option value="">Seleciona a filial</option>
				                <?php foreach ($filial as $val): ?>
				                	<option value="<?= $val['iuorg'] ?>"><?= $val['iuorg'] ?></option>
				                <?php endforeach;?>
					  </select>
					</div>
					<div class="form-group col-md-12">
						 <div class="radio">
						  <label><input type="radio" name="<?= $form_names['acao'] ?>" value="add" required>Liberar batida na mesma filial</label>
						</div>
						<div class="radio">
						  <label><input type="radio" name="<?= $form_names['acao'] ?>" value="rmv" required>Regeitar batida na mesma filial</label>
						</div>
					</div>
					<input type="hidden" name="<?= $token_id; ?>" value="<?= $token_value; ?>" />
					
					<button type="submit" class="btn btn-default btn-block">Salvar</button>
				
						<?php if (isset($aviso)): ?>
							<hr />
							<?php echo $aviso; ?>
						<?php endif; ?>
				</form>
		</div>	
	</div>
	
	<!-- ############# -->
	
	<div class="col-md-4">
		<div class="col-md-12 content">
			<div class="cabecalho">
				<h3>Filial</h3>
			</div>
			<ul class="list-group list-filial">
			
			</ul>
		</div>
	</div>
	
</div>

<script>

	function listafilial(){
		var url = "<?= base_url("Administrador/listaFilial") ?>";
		$.get( url, function( data ) {
			  $( ".list-filial" ).html( data );
		});
	}

	$(document).ready(function(){
		listafilial();
	});
</script>