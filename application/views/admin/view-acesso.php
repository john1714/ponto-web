<div class="container-fluid">

	<div class="col-md-3">
		<div class="col-md-12 content">
			<form action="<?= base_url("Administrador/acesso/salva") ?>" method="post" class="col-md-12" style="">
				<div class="form-group">
					<label for="usuario">Usuario</label>
					<input type="text" class="form-control" name="<?= $form_names['usuario'] ?>" id="usuario" required>
				</div>
				<div class="form-group col-md-6">
					 <div class="radio">
					  <label><input type="radio" name="<?= $form_names['acao'] ?>" value="ad" required>Acesso DP</label>
					</div>
					<div class="radio">
					  <label><input type="radio" name="<?= $form_names['acao'] ?>" value="ag" required>Acesso Gestor</label>
					</div>
				</div>
				<div class="form-group col-md-6">
					 <div class="radio">
					  <label><input type="radio" name="<?= $form_names['acao'] ?>" value="nad" required>Negar acesso DP</label>
					</div>
					<div class="radio">
					  <label><input type="radio" name="<?= $form_names['acao'] ?>" value="nag" required>Negar acesso Gestor</label>
					</div>
				</div>
				<input type="hidden" name="<?= $token_id; ?>" value="<?= $token_value; ?>" />
				<button type="submit" class="btn btn-default btn-block">Salvar</button>
				
				<?php if (isset($erro)): ?>
					<hr />
					<p><?php echo $erro; ?></p>
				<?php endif; ?>
				
				<?php if (isset($aviso)): ?>
					<hr />
					<p><?php echo $aviso; ?></p>
				<?php endif; ?>
			</form>
		</div>
	</div>
	
	<!-- ############# -->
	
	<div class="col-md-3">
		<div class="col-md-12 content">
			<div class="cabecalho">
				<h4>Acesso DP</h4>
			</div>
			<ul class="list-group list-usuarios-dp">
			
			</ul>
		</div>
	</div>
	
	<!-- ############# -->
	
	<div class="col-md-3">
		<div class="col-md-12 content">
			<div class="cabecalho">
				<h4>Acesso Gestor</h4>
			</div>
			<ul class="list-group list-usuarios-gestor">
			
			</ul>
		</div>
	</div>

	<div class="col-md-3">
		<div class="col-md-12 content">
			<div class="cabecalho">
				<h4>Intervalo de dias p/ justificativas</h4>
			</div>
			
			<form action="<?= base_url("Administrador/acesso/salvaintervalo") ?>" method="post" >
                <div class="form-group col-md-4">
                    <label for="filial"><h6>Filial</h6></label>
                    <input type="text" class="form-control" value="<?= isset($filial) ? $filial : '' ?>" name="<?= $form_names['filial'] ?>" id="filial" required>
                </div>

				<div class="form-group col-md-4">
					<label for="diaanterior"><h6>(-) Dia </h6></label>
					<input type="text" class="form-control" value="<?= isset($diaanterior) ? $diaanterior : '' ?>" name="<?= $form_names['diaanterior'] ?>" id="diaanterior" required>
				</div>

				<div class="form-group col-md-4">
					<label for="disposterior"><h6>(+) Dia</h6></label>
					<input type="text" class="form-control" value="<?= isset($diaposterior) ? $diaposterior : ''  ?>" name="<?= $form_names['diaposterior'] ?>" id="disposterior" required>
				</div>
				
				<input type="hidden" name="<?= $token_id; ?>" value="<?= $token_value; ?>" />
				<button type="submit" class="btn btn-default btn-block">Salvar</button>
				
				<?php if (isset($erro)): ?>
					<hr />
					<p><?php echo $erro; ?></p>
				<?php endif; ?>
				
				<?php if (isset($aviso)): ?>
					<hr />
					<p><?php echo $aviso; ?></p>
				<?php endif; ?>
			</form>
            <style>
                #DataTables_Table_0_filter > label > input{
                    width: 60px;
                }

                #DataTables_Table_0_info{
                    display: none;
                }
            </style>

            <div class="col-md-12" style="margin-top: 15px;">
                <div class="table-responsive">
                    <table class="datetable table-hover">
                        <thead>
                        <tr>
                            <th>Filial</th>
                            <th>Dia anterior</th>
                            <th>Dia posterior</th>
                            <th>-</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($intervalos as $intervalo) {?>
                            <?php if(isset($intervalo['filial']) && isset($intervalo['qnt_inicio'])  && isset($intervalo['qnt_fim'])){ ?>
                                <tr>
                                    <td><?= $intervalo['filial'] ?></td>
                                    <td><?= $intervalo['qnt_inicio'] ?></td>
                                    <td><?= $intervalo['qnt_fim'] ?></td>
                                    <td>
                                        <form id="formdeletaintervalo" style="box-shadow: 0 0 0 0; border: none; background-color: transparent; padding: 0; margin-top: 0;" action="<?= base_url("Administrador/acesso/deletaintervalo") ?>" method="post">
                                            <input type="hidden" value="<?= $intervalo['filial'] ?>" name="filial">
                                            <a href="#" onclick="document.getElementById('formdeletaintervalo').submit();"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                        </form>
                                    </td>
                                </tr>
                            <?php  } ?>
                        <?php  } ?>
                        </tbody>
                    </table>
                </div>
            </div>

	</div>


	
</div>


<script>
function listaUsuariosDp(){
		var url = "<?= base_url("Administrador/listaAcessoDp") ?>";
		$.get( url, function( data ) {
			  $( ".list-usuarios-dp" ).html( data );
		});
	}

function listaUsuariosGestor(){
	var url = "<?= base_url("Administrador/listaAcessoGestor") ?>";
	$.get( url, function( data ) {
		  $( ".list-usuarios-gestor" ).html( data );
	});
}

	$(document).ready(function(){

		listaUsuariosDp();
		listaUsuariosGestor();
				
	});
</script>