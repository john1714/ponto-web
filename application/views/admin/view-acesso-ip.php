<div class="container">
	<div class="col-md-4">
		<div class="col-md-12 content">
				<form action="<?= base_url("Administrador/permissao-filial/salva-ip") ?>" method="post" class="" style="">
					<div class="form-group">
					  <label for="ipf">IP Fixo:</label>
  					  <input type="text" class="form-control ip_address" id="ipf" name="<?= $form_names['ipf'] ?>" placeholder="Ex: 192.168.0.0, 10.10.0.2"  maxlength="5">
					</div>
					 <div class="form-group">
					  <label for="sel1">Selecione a filial:</label>
					  <select class="form-control" id="sel1" name="<?= $form_names['filial'] ?>">
					    <option value="">Seleciona a filial</option>
				                <?php foreach ($filial as $val): ?>
				                	<option value="<?= $val['iuorg'] ?>"><?= $val['iuorg'] ?></option>
				                <?php endforeach;?>
					  </select>
					</div>
					<hr />
					<div class="form-group">
					  <label for="ipp">IP Padrão:</label>
  					  <input type="text" class="form-control ip_address" name="<?= $form_names['ipp'] ?>" id="ipp" placeholder="Ex: 192.168, 10.10" >
					</div>
					<div class="form-group col-md-6">
						 <div class="radio">
						  <label><input type="radio" name="<?= $form_names['acao'] ?>" value="adipf" required>Aplicar IP Fixo</label>
						</div>
					</div>
					
					<div class="form-group col-md-6">
						<div class="radio">
						  <label><input type="radio" name="<?= $form_names['acao'] ?>" value="rmipf" required>Desativar IP Fixo</label>
						</div>
					</div>
					
					<div class="form-group col-md-6">
						 <div class="radio">
						  <label><input type="radio" name="<?= $form_names['acao'] ?>" value="adipp" required>Aplicar IP Padrão</label>
						</div>
					</div>
					
					<div class="form-group col-md-6">
						<div class="radio">
						  <label><input type="radio" name="<?= $form_names['acao'] ?>" value="rmipp" required>Desativar IP Padrão</label>
						</div>
					</div>
					<input type="hidden" name="<?= $token_id; ?>" value="<?= $token_value; ?>" />
					
					<button type="submit" class="btn btn-default btn-block">Salvar</button>
				
						<?php if (isset($aviso)): ?>
							<hr />
							<?php echo $aviso; ?>
						<?php endif; ?>
				</form>
		</div>	
	</div>
	
	<!-- ############# -->
	
	<div class="col-md-4">
		<div class="col-md-12 content">
			<div class="cabecalho">
				<h4>IP'S Fixo</h4>
			</div>
			<ul class="list-group ip-fixo">
			
			</ul>
		</div>
	</div>
	
	<!-- ############# -->
	
	<div class="col-md-4">
		<div class="col-md-12 content">
			<div class="cabecalho">
				<h4>Padrão de IP suportado</h4>
			</div>
			<ul class="list-group ip-suport">
				
			</ul>
		</div>
	</div>
</div>

<script src="<?= base_url('bootstrap/js/jquery.mask.min.js')?>" ></script>
<script>
function listaippadrao(){
		var url = "<?= base_url("Administrador/listaPadraoIp") ?>";
		$.get( url, function( data ) {
			  $( ".ip-suport" ).html( data );
		});
	}

	function listaipfixo(){
		var url = "<?= base_url("Administrador/listaIpFixo") ?>";
		$.get( url, function( data ) {
			  $( ".ip-fixo" ).html( data );
		});
	}

	$(document).ready(function(){

		$('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
		    translation: {
		      'Z': {
		        pattern: /[0-9]/, optional: true
		      }
		    }
		  });

			   
		
		listaippadrao();
		listaipfixo();
	});
</script>