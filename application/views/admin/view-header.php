 <!DOCTYPE html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Administrador</title>

     <!-- Bootstrap -->
     <link href="<?= base_url('bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
     <link href="<?= base_url('datatable/css/dataTables.bootstrap.min.css')?>" rel="stylesheet">



     <link href="<?= base_url('bootstrap/css/sweetalert.css')?>" rel="stylesheet">
     <link href="<?= base_url('bootstrap/css/style-dp.css')?>" rel="stylesheet">
     <script src="<?= base_url('bootstrap/js/jquery.min.js')?>" ></script>
     <script src="<?= base_url('bootstrap/js/bootstrap.min.js')?>" ></script>



     <script src="<?= base_url('bootstrap/datatable/jquery.dataTables.min.js')?>" ></script>
     <script src="<?= base_url('bootstrap/datatable/dataTables.bootstrap.min.js')?>" ></script>
     <script src="<?= base_url('bootstrap/js/bootstrap-confirmation.min.js')?>" ></script>
     <script src="<?= base_url('bootstrap/js/sweetalert.min.js')?>" ></script>

     <link href="<?= base_url('bootstrap/css/jquery-ui.theme.min.css')?>" rel="stylesheet">
     <link href="<?= base_url('bootstrap/css/jquery-ui.min.css')?>" rel="stylesheet">
     <script src="<?= base_url('bootstrap/js/jquery-ui.min.js')?>" ></script>


     <script>
        $(document).ready(function(){
            $('.datetable').DataTable({
                "paging":false,
                "scrollY":  "180px",
            });
        });
    </script>
    <style>

    	body{
    		background-color: #f8f8f8;
    	}
    	.navbar-custom{
    		background-color: #FFF;
    		border-bottom: solid 1px #ddd;
    	}
    	
    	.content{
    		background: #fff;
    		width:100%;
    		box-shadow: 0px 0px 5px 2px #999;
    		padding:0;
    	}
    	
    	.list-group{
    		background-color: #fff;
    		box-shadow: 0px 0px 5px 1px #ddd; 
    		margin-top:70px;    		
    		max-height:400px;
    		overflow: auto;
    	}
    	
    	.content form{
    		background-color: #fff;
    		box-shadow: 0px 0px 5px 1px #ddd; 
    		padding:35px; 
    		margin-top:115px;
    	}
    	
    	.cabecalho{
    		padding:0px 10px;
    	}
    	
    	.cabecalho h4{
    		position:relative;
    		top:60px;
    		font-weight:600;
    	}
    </style>
    
 </head>
 
 <body>
 
 <nav class="navbar navbar-custom navbar-fixed-top">
  <div class="container-fluid">
   <div class="navbar-header">
    
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
     <span class="sr-only">Toggle navigation</span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">
        <img alt="Brand" style="margin-top: -12px;" width="100" height=45" src="<?= base_url('public/img/LogoIbyte.png') ?>" />
    </a>
    <p class="navbar-text" style="color:#FFF; margin-top: 17px;"></strong></p>
   </div>
   
   <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Funções <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?= base_url("Administrador/acesso") ?>">Permissão de acesso DP / Gestor</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?= base_url("Administrador/permissao-filial") ?>">Permissão de batida na mesma filial</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?= base_url("Administrador/permissao-ip") ?>">Configuração de batida por IP</a></li>
          </ul>
        </li>
      </ul>
   
   <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right" id="link-right">     
     <li><a href="<?= site_url('login/sair') ?>">Sair</a></li>
    </ul>
   </div>
  </div>
 </nav>