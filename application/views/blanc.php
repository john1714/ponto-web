<!DOCTYPE html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $title ?></title>
    
    <!-- Bootstrap -->
    <link href="<?= base_url('bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('bootstrap/css/jquery-ui.theme.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('bootstrap/css/jquery-ui.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('bootstrap/css/mtimepicker.css')?>" rel="stylesheet">
    <script src="<?= base_url('bootstrap/js/jquery.min.js')?>" ></script>
    <script src="<?= base_url('bootstrap/js/bootstrap.min.js')?>" ></script>
    <script src="<?= base_url('bootstrap/js/jquery-ui.min.js')?>" ></script>
    <script src="<?= base_url('bootstrap/js/mtimepicker.js')?>" ></script>

</head>
<body>
        
        

</body>

</html>