<?php date_default_timezone_set('America/Fortaleza');  ?>
 <!DOCTYPE html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $title ?></title>
    
    <!-- Bootstrap -->
    <link href="<?= base_url('bootstrap/css/bootstrapn.css')?>" rel="stylesheet">
    <link href="<?= base_url('bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('bootstrap/css/sweetalert.css')?>" rel="stylesheet">
    
    <script src="<?= base_url('bootstrap/js/jquery.min.js')?>" ></script>
    <script src="<?= base_url('bootstrap/js/bootstrap.min.js')?>" ></script>
    <script src="<?= base_url('bootstrap/js/sweetalert.min.js')?>" ></script>

	
    <script>
            
           var digital = new Date(); // crio um objeto date do javascript
	//digital.setHours(15,59,56); // caso não queira testar com o php, comente a linha abaixo e descomente essa
	digital.setHours(<?php echo date("H,i,s"); ?>); // seto a hora usando a hora do servidor

	function clock() {
	var hours = digital.getHours();
	var minutes = digital.getMinutes();
	var seconds = digital.getSeconds();
	digital.setSeconds( seconds+1 ); // aquin que faz a mágica

	// acrescento zero
	if (minutes <= 9) minutes = "0" + minutes;
	if (seconds <= 9) seconds = "0" + seconds;

	dispTime = hours + ":" + minutes + ":" + seconds;
	document.getElementById("hora").innerHTML = dispTime; // coloquei este div apenas para exemplo
	setTimeout("clock()", 1000); // chamo a função a cada 1 segundo

	}

	window.onload = clock;

	function alerta(title,text){
		swal(title, text);
	}

	$(document).ready(function(){
		
		$( ".opt-resgistros" ).click(function() {
		  $( "#content-ponto" ).slideUp( "fast", function(){
		  		$("#content-registro").slideDown('fast');
		  });
		});

		$( ".opt-ponto" ).click(function() {
		  $( "#content-registro" ).slideUp( "fast", function(){
		  		$("#content-ponto").fadeIn('fast');
		  });
		});

	});

    </script>

    <style type="text/css">

    	@media (min-width: 768px){
		    p{
		        font-size:24px;
		    }

		    #btn-option{
		    	position: relative;
		    	margin: 0 auto;
		    }
		}
			
		.view{
			display: none;
		}
    
    	.info{
    		color:#02595b;
    	}
    	
    	.corpo p{
    		color: #013d3f;
    	}
    
    	#hora{
    		font-size: 410%;
    		color: #013d3f;
    		text-shadow: #888 2px 1px 6px, #FFF -1px -1px 1px;
    	}

    	.content{
    		position: relative;
    		background-color: #FFFFFF;
    		border-radius: 10px;
    		top: 0px;
    		padding: 20px 20px 30px 20px;
    		box-shadow: 0px 0px 2px 1px #999;
    	}

    	.clear{
    		clear: both;
    	}

    	#btn-bt{
    		position: relative;	
    		weight:70px;
    		height:150px;
    		border-radius: 40px;
    		margin: 0 auto;
    		color: #FFF;
    		text-shadow: 1px 1px 2px #000;
    		font-size: 18px;
    		font-weight:800;
    		background-color: #FFF;
    		/*box-shadow: 0px 0px 2px 1px #999;*/
    		background-image: url("<?= base_url('/public/img/dedo.png') ?>");
    		background-size: 130px 160px;
    		background-position: 0px -6px;
    		background-repeat: no-repeat;
    		transition: 0.2s;
    		opacity: 0.8;
    	}
    	#btn-bt:hover{
    		background-color: #FFF;
    		transition: 0.2s;
    		opacity: 1;
    	}

    	.color-custom{
    		background-color: #888;
    	}

    	.btn-custom{
    		border: solid 1px #ddd; 
    		color: #999;
    		transition: 0.5s;
    		font-weight: 600;
    	}
    	.btn-custom:hover{
    		transition: 0.2s;
    		color: #FFF;	
    		text-shadow: #333 1px 1px 1px;
    		background: #999;
    	}

    	#btn-option{
    		float: right;
    	}

        thead {
            background-color: #555;
            color: #FFF;
        }
    </style>
    
  </head>
  
  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img alt="Brand" style="margin-top: -15px; width: 100px; height: 50px;"  src="<?= base_url('public/img/LogoIbyte.png') ?>" />
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      
      <ul class="nav navbar-nav navbar-right">
         <li><a href="javascript:view('ponto-form');">Bater Ponto</a></li>
	     <li><a href="javascript:view('batidas-form');" >Meus Pontos</a></li>
	     <li><a href="<?= site_url('login/sair') ?>">Sair</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

 <!-- <nav class="navbar navbar-default">
  <div class="container-fluid">
  	
  	 
  
    <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img alt="Brand" style="margin-top: -15px; width: 100px; height: 50px;"  src="<?= base_url('public/img/LogoIbyte.png') ?>" />
      </a>
    </div>
    
    <div id="navbar" class="navbar-collapse collapse">
	    <ul class="nav navbar-nav navbar-right" id="link-right">
	     <li><a href="javascript:view('ponto-form');">Bater Ponto</a></li>
	     <li><a href="javascript:view('batidas-form');" >Meus Pontos</a></li>
	     <li><a href="<?= site_url('login/sair') ?>">Sair</a></li>
	    </ul>
    </div>

  </div>
</nav> -->



<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<!--<div class="row">
					<div class="cabecalho" >
						<img class="" width="120" height="50" src="">
						<div class="btn-group" role="group" id="btn-option">
	  						<a href="" class="btn btn-custom" style="float: right;">Sair</a>
							<a href="javascript:view('batidas-form');" class="btn btn-custom view-lk" style="float: right;">Batidas</a>
							<a href="javascript:view('ponto-form');" class="btn btn-custom view-lk" style="float: right;">Ponto</a>
						</div>
	
						<div class="clear"></div>
						<hr />
					</div>
				</div> -->
				<div id="ponto-form" class="view" >
				<div class="row">
					<center><p id="hora"></p></center>
					
					<?php 
						
					?>
					<div class="corpo"><span class="info">
						<div class="col-md-8" style="padding-right: 0;">
							<p><strong>Nome</strong>: <span class="info"><?= $usuario['nome'] ?></span></p>
							<p><strong>Filial:</strong> <span class="info"><?= (empty($usuario['filial'])) ? "" : $usuario['filial'] ?></span></p>
                                <?= ($permissao_bool)
                                    ? "<h5 class='text-success'>Permissão Concedida</h5>"
                                    : "<h5 class='text-danger'>Permissão Negada</h5>"
                                ?>
                            <!-- teste -->
						</div>
						<div class="col-md-1"></div>
						
						<div class="col-md-4">
							<p style=""><strong>Data:</strong> <span class="info"><?= date("d/m/Y") ?></span></p>
							<p style=""><strong>IP:</strong> <span class="info"><?= isset($ip) ? $ip : " "  ?></span></p>
							<p style=""><strong>Filial Autenticado:</strong> <span class="info"><?= (empty($filialAuth)) ? "" : $filialAuth ?></span></p>
						</div>

					</div>

				</div>
				
						
						<form action="<?= site_url('PontoWeb/ponto') ?>" method="post">
								<input type="hidden" name="<?= $token_id; ?>" value="<?= $token_value; ?>" />
							    <center><button class="btn" id="btn-bt" type="submit" style="">Bater Ponto</button></center>
						</form>
					
					
				</div>

				<!-- #################### -->

				<div id="batidas-form" class="view">
					
					<div class="col-md-3">
						
						 <div class="form-group">
						  <label for="batidas"></label>
						  <select class="form-control" id="batidas">
						    <option value="dia">Hoje</option>
						    <option value="mes">Hoje / Mẽs anterior</option>
						  </select>
						</div>
				                        <h4 id="load-table" style="display: none;">Carregando...</h4>
     					         <h5>Excluido ou Cancelado <span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></h5>
     					         <h5>Confirmado pelo DP <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></h5>
     					         <h5>Pendente <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></h5>
					</div>


					<table class="table table-hover" id="tb-ponto">

                    </table>
					

				</div>


			</div>
		</div>
	</div>
</div>

<?php
   if(isset($aviso)){
       echo $aviso;
    }
    
    if(isset($erro)){
    	echo $erro;
    }
?>


<script>

	function listabatidas(valordia){
			if(valordia == null){
				var valor = 'dia';		
			}else{
				var valor = valordia;
			}
		  	console.log(valor);
			$.ajax({
				type : 'GET',
				url  : "<?= base_url('PontoWeb/listabatidas') ?>/?val="+valor,
		contentType  : "text",
				beforeSend : function(){
                    $("#load-table").show();
				}, 
				success : function(data){
                    $("#load-table").fadeOut();
					$("#tb-ponto").html(data);
				}
			});

		
	  
	}
	
	function view(view){

		if($("#"+view).is( ":hidden" )){
			$(".view").slideUp('fast');
			setTimeout(function(){ $("#"+view).fadeIn(); }, 500);
		}
		
	}

	$(document).ready(function(){
		view('ponto-form');
		listabatidas();

		$( "#batidas" ).change(function() {
			listabatidas(this.value);
		});


		
	});

</script>

</body>
</html>