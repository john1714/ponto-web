<thead>
<tr>
    <th>Nome</th>
    <th>Em</th>
    <th>Filial</th>
    <th>Filial Autenticado</th>
    <th>Data</th>
    <th>Hora</th>
    <th>Status</th>
</tr>
</thead>
<tbody>
<?php

foreach ($list as $row) {
	
    if(trim($row['status']) == 'EX') {
	$status = "<span style=\" margin-left:15px; \" class=\"glyphicon glyphicon-ban-circle\"></span>";
    }

    if(trim($row['status']) == 'CA') {
	$status = "<span style=\" margin-left:15px; \" class=\"glyphicon glyphicon-ban-circle\"></span>";
    }

    if(trim($row['status']) == 'PD') {
             $status = "<span style=\" margin-left:15px; \" class=\"glyphicon glyphicon-question-sign\"></span>";
    }

    if(trim($row['status']) == 'CF') {
             $status = "<span style=\" margin-left:15px; \" class=\"glyphicon glyphicon-ok\"></span>";
    }

 
?>

    <!-- HTML -->

    <tr>
        <td><?= $row['nome'] ?></td>
        <td><?= $row['em'] ?></td>
        <td><?= $row['filial_origem'] ?></td>
        <td><?= $row['filial_autenticado'] ?></td>
        <td><?= $row['data_bt'] ?></td>
        <td><?= $row['hora'] ?></td>
        <td><?= $status ?></td>
    </tr>

    <!-- HTML -->
<?php
    } //End Foreach
?>

</tbody>