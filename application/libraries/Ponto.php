<?php

class Ponto {
        public $nome;
        public $usuario;
        public $filial_origem;
        public $filial_autenticado;
        public $ip;
        public $data_;
        public $hora;
        public $status;
        public $pis;
        public $em;
    
        function getNome() {
            return $this->nome;
        }

        function getUsuario() {
            return $this->usuario;
        }

        function getFilial_origem() {
            return $this->filial_origem;
        }

        function getFilial_autenticado() {
            return $this->filial_autenticado;
        }

        function getIp() {
            return $this->ip;
        }

        function getData_() {
            return $this->data_;
        }

        function getHora() {
            return $this->hora;
        }

        function getStatus() {
            return $this->status;
        }

        function getPis() {
            return $this->pis;
        }

        function getEm() {
            return $this->em;
        }

        function setNome($nome) {
            if(empty($nome) || $nome === NULL){
                 throw new Exception("Nome vazio");
            }
            
            $this->nome = $nome;
        }

        function setUsuario($usuario) {
            if(empty($usuario) || $usuario === NULL){
                 throw new Exception("Usuário vazio");
            }
            $this->usuario = $usuario;
        }

        function setFilial_origem($filial_origem) {
            if(empty($filial_origem) || $filial_origem === NULL){
                 throw new Exception("Filial colaborador vazio");
            }
            $this->filial_origem = $filial_origem;
        }

        function setFilial_autenticado($filial_autenticado) {
            if(empty($filial_autenticado) || $filial_autenticado === NULL){
                 throw new Exception("Filial autenticado vazio");
            }
            $this->filial_autenticado = $filial_autenticado;
        }

        function setIp($ip) {
             if(empty($ip) || $ip === NULL){
                 throw new Exception("Ip vazio");
            }
            $this->ip = $ip;
        }

        function setData_($data_) {
             if(empty($data_) || $data_ === NULL){
                 throw new Exception("Data vazio");
            }
            $this->data_ = $data_;
        }

        function setHora($hora) {
             if(empty($hora) || $hora === NULL){
                 throw new Exception("Hora vazia");
            }
            $this->hora = $hora;
        }

        function setStatus($status) {
            if(empty($status) || $status === NULL){
                 throw new Exception("Status vazio");
            }
            $this->status = $status;
        }

        function setPis($pis) {
             if(empty($pis) || $pis === NULL){
                 throw new Exception("Pis vazio");
            }
            $this->pis = $pis;
        }

        function setEm($em) {
             if(empty($em) || $em === NULL){
                 throw new Exception("Em vazio");
            }
            $this->em = $em;
        }


}
