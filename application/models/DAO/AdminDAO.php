<?php
	 class AdminDAO extends CI_Model{


	 	public function __construct(){

	 		parent::__construct();

	 		$this->load->model("BD_log");

	 	}


	 	public function autentica($user,$pass){
		 		$this->db->select("*");
		 		$this->db->from('tab_admin_acesso');
		 		$this->db->where('login_admin', $user);
		 		$this->db->where('senha_admin', $pass);
	  			$query = $this->db->get();

	  			return ($query->num_rows()) ? true : false;
	 	}

	 	public function getAcessoGestor($data = array()){
	 		$query = array();

	 		isset($data["usuario"]) ? $this->db->where('usuario', $data['usuario']): null;
	 		isset($data["nome"])    ? $this->db->where('nome'   , $data['nome'])   : null;
	 		isset($data["status"])  ? $this->db->where('status' , $data['status']) : null;

	 		$this->db->select('*');
	 		$query = $this->db->get('tab_gestor_acesso');

	 		$test = array();

	 		if($query !== FALSE && $query->num_rows() > 0){

	 			$test = $query->result_array();

	 		}

	 		return $test;

	 		if($error = $this->db->error()){
	 			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
	 		}
	 	}

	 	public function updateGestor($data = array()){

	 		$query = array();

	 		isset($data['usuario']) ? $query['usuario']          = $data['usuario'] : null;

	 		isset($data['nome'])    ? $query['nome_colaborador'] = $data['nome']    : null;

	 		isset($data['status'])  ? $query['status']           = $data['status']  : null;

	 		$this->db->where('idgestor', $data['id']);

	 		return $this->db->update('tab_gestor_acesso', $query);

	 		if($error = $this->db->error()){
	 			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
	 		}

	 	}

	 	public function getAcessoDp($data = array()){

	 		$query = array();

	 		isset($data["usuario"]) ? $this->db->where('usuario', $data['usuario']): null;
	 		isset($data["nome"])    ? $this->db->where('nome'   , $data['nome'])   : null;
	 		isset($data["status"])  ? $this->db->where('status' , $data['status']) : null;

	 		$this->db->select('*');
	 		$query = $this->db->get('tab_dp_acesso');

	 		$test = array();

	 		if($query !== FALSE && $query->num_rows() > 0){

	 			$test = $query->result_array();

	 		}

	 		return $test;

	 		if($error = $this->db->error()){
	 			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
	 		}
	 	}

	 	public function insereGestor($dados){

	 		return $this->db->insert('tab_gestor_acesso', $dados);

	 		if($error = $this->db->error()){
	 			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
	 		}

	 	}

	 	public function insereDp($dados){

	 		return $this->db->insert('tab_dp_acesso', $dados);

	 		if($error = $this->db->error()){
	 			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
	 		}

	 	}

	 	public function updateDp($data){

	 		$query = array();

	 		isset($data['usuario']) ? $query['usuario']          = $data['usuario'] : null;

	 		isset($data['nome'])    ? $query['nome_colaborador'] = $data['nome']    : null;

	 		isset($data['status'])  ? $query['status']           = $data['status']  : null;

	 		$this->db->where('idusuario_dp', $data['id']);

	 		return $this->db->update('tab_dp_acesso', $query);

	 		if($error = $this->db->error()){
	 			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
	 		}

	 	}

	 	public function removeAcessoGestor($dados){

	 		return $this->db->delete('gestor',$dados);

	 		if($error = $this->db->error()){
	 			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
	 		}

	 	}

	 	public function removeAcessoDp($dados){

	 		if($this->db->delete('dp',$dados)){
	 			return true;
	 		}else{
	 			return false;
	 		}

	 		if($error = $this->db->error()){
	 			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
	 		}else{
	 			echo "sem erroe";
	 		}

	 	}


	 	public function liberaFilial($filial = NULL){

	 		if($filial == NULL) return false;

	 		$this->db->where('filial', $filial['filial']);

	 		$this->db->select('*');

	 		$query = $this->db->get('tab_filial_mirror');

	 		if($query !== FALSE && $query->num_rows() > 0){

	 			foreach ($query->result_array() as $field)
	 			{
	 				return $this->updateFilial( array('id' => $field['idfilial'], 'status' => 'cf') );
	 			}

	 		}else{
	 			$data = (object) array(
	 					'filial' => $filial['filial'],
	 					'status' => 'cf'
	 			);

	 			return $this->db->insert('tab_filial_mirror', $data);
	 		}

	 	}


	 	public function insereFilial($filial = NULL){

	 		if($filial == NULL) return false;

	 		$data = (object) array(
	 				'filial' => $filial,
	 				'status' => 'cf'
	 		);

	 		return $this->db->insert('tab_filial_mirror', $data);

	 		if($error = $this->db->error()){
	 			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
	 		}

	 	}


	 	public function cancelaFilial($filial = NULL){

	 		if($filial == NULL) return false;

	 		$this->db->where('filial', $filial['filial']);

	 		$this->db->select('*');

	 		$query = $this->db->get('tab_filial_mirror');

	 		if($query !== FALSE && $query->num_rows() > 0){

	 			foreach ($query->result_array() as $field)
	 			{
	 				return $this->updateFilial( array('id' => $field['idfilial'], 'status' => 'ca') );
	 			}

	 		}else{
	 			$data = (object) array(
	 					'filial' => $filial['filial'],
	 					'status' => 'ca'
	 			);

	 			return $this->db->insert('tab_filial_mirror', $data);
	 		}


	 	}


	 	public function getAllAcessoFilial($filial = array()){

	 		isset($filial['filial']) ? $this->db->where('filial', $filial['filial']) : null;

	 		isset($filial['status']) ? $this->db->where('status', $filial['status']) : null;

	 		$this->db->select("*");

	 		$query = $this->db->get('tab_filial_mirror');

	 		$test = array();

	 		if($query !== FALSE && $query->num_rows() > 0){

	 			$test = $query->result_array();

	 		}

	 		return $test;

	 	}

	 	public function updateFilial($data = array()){

	 		$query = array();

	 		isset($data['filial']) ? $query['filial']   = $data['filial']: null;

	 		isset($data['status']) ? $query['status']  = $data['status']: null;

	 		$this->db->where('idfilial', $data['id']);

	 		return $this->db->update('tab_filial_mirror', $query);

	 		if($error = $this->db->error()){
	 			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
	 		}

	 	}


	 	public function getAcessoFilial($filial){
	 		$this->db->select("*");
	 		$this->db->from('tab_filial_mirror');
	 		$this->db->where('filial', $filial);
	 		$query = $this->db->get('tab_filial_mirror');

	 		$test = false;

	 		if($query !== FALSE && $query->num_rows() > 0){
	 			$test = ($query->num_rows()) ? true : false;
	 		}

	 		return $test;


	 	}


	 	public function existFilialMirror($filial){

	 		$filial = array(
	 				'filial' => $filial,
	 				'status' => 'cf'
	 		);

	 		$this->db->select("*");
	 		$this->db->from('tab_filial_mirror');
	 		$this->db->where($filial);

	 		$query = $this->db->get();

	 		return ($query->num_rows()) ? true : false;
	 	}


	 	public function getDataInterval(){

	 		$this->db->select("*");
	 		$this->db->order_by('id', 'DESC');
	 		$this->db->limit(1);

	 		$query = $this->db->get('tab_data_intervalo');

	 		$test = array();

	 		if($query !== FALSE && $query->num_rows() > 0){

	 			$test = $query->result_array();

	 		}else {
	 			$test[] = array('qnt_inicio' => '-6', 'qnt_fim' => '1');
	 		}

	 		return $test;
	 	}


	 	public function getFilialInterval($filial = null){

            $this->db->select("*");
            if($filial != null) {$this->db->where('filial', $filial);}
            $this->db->order_by('id', 'DESC');
            $this->db->limit(1);

            $query = $this->db->get('tab_data_intervalo');

            if($query !== FALSE && $query->num_rows() > 0){

                $result = $query->result_array();

            }else{
                $result[] = array('qnt_inicio' => '-6', 'qnt_fim' => '1');
            }

            return $result;

        }

         public function getExisteFilialInterval($filial = null){

             $this->db->select("*");
             if($filial != null) {$this->db->where('filial', $filial);}
             $this->db->order_by('id', 'DESC');
             $this->db->limit(1);

             $query = $this->db->get('tab_data_intervalo');

             if($query !== FALSE && $query->num_rows() > 0){

                 $result = $query->result_array();

             }else{
                 $result = [];
             }

             return $result;

         }


         public function getFilialIntervalAll(){

             $this->db->select("*");

             $this->db->order_by('id', 'DESC');

             $query = $this->db->get('tab_data_intervalo');

             if($query !== FALSE && $query->num_rows() > 0){

                 $result = $query->result_array();

             }else{
                 $result[] = array('qnt_inicio' => '-6', 'qnt_fim' => '1');
             }

             return $result;

         }


	 	public function insereDataInterval($dia){

	 		return $this->db->insert('tab_data_intervalo', $dia);

	 	}

         public function deletaDataInterval($filial = null){

	 	    if($filial == null)
	 	        return false;

             return $this->db->delete('tab_data_intervalo', array('filial' => $filial));

         }

	 	public function updateDataInterva($params){
            $this->db->where('filial', $params->filial);
            return $this->db->update('tab_data_intervalo', $params);

        }

	 }
