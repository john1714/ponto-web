<?php

	class GestorDAO extends CI_MODEL{

		private $usuario;

		public function __construct(){

                parent::__construct();

                $this->load->model("BD_log");

                date_default_timezone_set('America/Fortaleza');
		 }

		public function getJustificativaColaborador($em = ""){
            $anomes = date("Y-m");
            $query = $this->db->query("SELECT *,
											DATE_FORMAT(data_,'%d/%m/%Y')    as data_bt,
											DATE_FORMAT(data_add,'%d/%m/%Y') as data_add
			FROM tab_ponto_justificativa where EXTRACT(YEAR_MONTH FROM data_add) >= EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 3 MONTH) and gestor = '{$em}' and status <> 'EX' ");

            $test = array();

            if($query !== FALSE && $query->num_rows() > 0){
                $test = $query->result_array();
            }
            return $test;
		}

		public function atualizaStatus($id = NULL, $status = NULL, $dateNow = false){

			if(!$dateNow){
                $data = array(
                    'status' => $status,
                );
			}else{
                $data = array(
                    'status' => $status,
					'data_add' => date('Y-m-d')
                );
			}

			$this->db->where('idponto_jus', $id);
			return $this->db->update('tab_ponto_justificativa', $data);

			if($error = $this->db->error()){
				$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
			}
		}

		public function atualizaFeed($id = NULL, $feed = NULL){
			$data = array(
					'feedback' => $feed
			);

			$this->db->where('idponto_jus', $id);
			return $this->db->update('tab_ponto_justificativa', $data);

			if($error = $this->db->error()){
				$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
			}
		}

		public function getJusId($id = NULL){
			$this->db->select("*,
								DATE_FORMAT(data_,'%d/%m/%Y')    as data_bt,
								DATE_FORMAT(data_add,'%d/%m/%Y') as data_add");

			$this->db->from('tab_ponto_justificativa');
			$this->db->where('idponto_jus', $id);
			$query = $this->db->get();

			$test = array();

			if($query !== FALSE && $query->num_rows() > 0){

				$test = $query->result_array();

			}

			return $test;

		}

		public function listajustificativasgestor($user, $all = NULL){
				$anomes = date("Y-m");
				$query = $this->db->query("SELECT *,
							   DATE_FORMAT(data_,'%d/%m/%Y')    as data_bt,
							   DATE_FORMAT(data_add,'%d/%m/%Y') as data_add
							   FROM ponto_jus
							   WHERE gestor = '{$user}'
							   AND status <> 'EX'
							   ORDER BY data_add DESC");

				$test = array();

				if($query !== FALSE && $query->num_rows() > 0){

					$test = $query->result_array();

				}

				return $test;
		}

		public function listaMotivos(){

			$query = $this->db->get('tab_motivos');

			$test = array();

			if($query !== FALSE && $query->num_rows() > 0){

				$test = $query->result_array();

			}

			return $test;

		}

		public function listaFiltroGestor($sql){

			$query = $this->db->query($sql);

			$test = array();

			if($query !== FALSE && $query->num_rows() > 0){

				$test = $query->result_array();

			}

			return $test;

		}

		public function listajustificativas(){

				$anomes = date("Y-m");

				$query = $this->db->query("SELECT *,
											DATE_FORMAT(data_,'%d/%m/%Y')    as data_bt,
											DATE_FORMAT(data_add,'%d/%m/%Y') as data_add
											FROM tab_ponto_justificativa
									        WHERE 1 ORDER BY idponto_jus DESC LIMIT 30");

				$test = array();

				if($query !== FALSE && $query->num_rows() > 0){

					$test = $query->result_array();

				}

				return $test;
		}

		public function insereJustificativa($obj){

					if(!$this->db->insert('tab_ponto_justificativa', $obj)){
						/*codigo para inserir log*/
							$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
						/*codigo para inserir log*/

						throw new Exception("Seus dados dados não foram inseridos pois ouve algum erro interno.");
					}

		}

		public function prepare_filter($array, $catg = ''){

			$filial = isset($array['filial']) ? $array['filial'] : "";
			$nome   = isset($array['nome'])   ? trim($array['nome'])   : "";
            $em     = isset($array['em'])   ? trim($array['em'])   : "";
			$data1  = isset($array['data1'])  ? $array['data1']  : "";
			$data2  = isset($array['data2'])  ? $array['data2']  : "";
			$gestor = isset($array['gestor']) ? $array['gestor'] : "";
			$status = isset($array['status']) ? $array['status'] : "";

			if($status == '0'){
				$newstatus = 'zero';
			}else{
				$newstatus = $status;
			}

			$anomes = date("Y-m");

			if(!empty($data1)){
				$dataArray1 = explode("/", $data1);
				$newData1 = $dataArray1[2]."-".$dataArray1[1]."-".$dataArray1[0];
			}

			if(!empty($data2)){
				$dataArray2 = explode("/", $data2);
				$newData2 = $dataArray2[2]."-".$dataArray2[1]."-".$dataArray2[0];
			}

			$where = Array();

			$statusDP = "";
			if($catg == 'dp'){
				$statusDP = " and status <> CAGS";
			}

			if($nome){$where[] =" LOWER(nome) LIKE LOWER('%$nome%' ) ";}
			if($filial){$where[] = " filial = '{$filial}'";}
            if($em){$where[] = " em = '{$em}'";}
			if($gestor){$where[] = " gestor = '{$gestor}' ";}
			if($newstatus){$where[] = " $status ";}else{ $where[] = " status <> 'EX' {$statusDP}";  }
			//if(empty($newData1) && empty($newData2)){$where[] = " MONTH(data_add)=MONTH(CURDATE()) AND YEAR(data_add)=YEAR(CURDATE()) ";}
			if(!empty($newData1) && empty($newData2)){$where[] = " data_ = '{$newData1}'";}
			if(!empty($newData1) && !empty($newData2)){$where[] =" data_ BETWEEN '{$newData1}' and '{$newData2}'";}
            if(empty($newData1) && empty($newData2)){$where[] =" LEFT(data_add,7) = '{$anomes}' ";}
            #DATE_FORMAT(data_add,'%d/%m/%Y') as data_add
			$sql = "SELECT *, DATE_FORMAT(data_,'%d/%m/%Y')    as data_bt
							  
							  FROM tab_ponto_justificativa";

			if(sizeof($where)){
				 $sql .= " WHERE ".implode(" AND ",$where );
				 $sql .= " ORDER BY data_add DESC limit 1000";

				 return $sql;
			}else{
				return $sql .= " WHERE LEFT(data_add,7) = '{$anomes}' ORDER BY data_add DESC";
			}
		}

        public function view_table_colaborador_jus($array, $id = null){
            $html = "";

            //echo "<pre>";
				//var_dump($array);
            //echo "</pre>";
            foreach ($array as $row) {
                $status = "";
                $acoes = $row['status'];

                if($row['status'] == 'PDCL'){
                    $sess = $this->session->userdata('session');

                    if($row['gestor'] === $sess['usuario']){
                        $acoes = "
						<div class=\"btn-group\">		
                            <a class=\"btn btn-danger btn-xs\"  href=\"javascript:excluir($row[idponto_jus])\">
                                    <span class=\"glyphicon glyphicon-trash\"></span>
                            </a>			
						</div>

                        <img alt=\"Brand\" style=\"display:none;\" class=\"img-load\" width=\"30\" height=\55\" src=".base_url('public/img/30.gif')." />";

                    }
                }

                if($row['status'] == 'PD'){
                    $sess = $this->session->userdata('session');
                    if($row['gestor'] === $sess['usuario']){
                        $acoes = "
						<small><b>Aprovado pelo gestor <br /> Esperando aprovação do DP</b></small>
                        ";
                    }
                }

                if($row['status'] == 'CA'){
                    $sess = $this->session->userdata('session');
                    if($row['gestor'] === $sess['usuario']){
                        $acoes = "
						<p>Cancelado pelo DP</p>
                        ";
                    }
                }

                if($row['status'] == 'CAGS'){
                    $sess = $this->session->userdata('session');
                    if($row['gestor'] === $sess['usuario']){
                        $acoes = "
						<p>Reprovado pelo gestor</p>
                        ";
                    }
                }

                if($row['status'] == 'CF'){
                    $sess = $this->session->userdata('session');
                    if($row['gestor'] === $sess['usuario']){
                        $acoes = "
						<p>Confirmado pelo DP</p>
                        ";
                    }
                }

                $html .= "<tr id=\"tr_{$row['idponto_jus']}\">";
                $html .= "<td>".$row['idponto_jus']."</td>";
                $html .= "<td>".$row['nome']."</td>";
                $html .= "<td>".$row['filial']."</td>";
                $html .= "<td>".$row['data_bt']."</td>";
                $html .= "<td>".$row['data_add']."</td>";
                $html .= "<td>".$row['hora']."</td>";
                $html .= "<td>".$row['motivo']."</td>";
                $html .= "<td>".$row['obs']."</td>";
                $html .= "<td>".$acoes."</td>";
                $html .= "</tr>";
            }

            return $html;

        }

		public function view_table_gestor_jus($array, $id = null){
			$html = "";
			$acoes = "";

			foreach ($array as $row) {

				if($row['status'] == 'PD'){

					$acoes = "
					<div class=\"btn-group \">
						 <a class=\"btn btn-danger btn-xs\"  href=\"javascript:excluir($row[idponto_jus])\">
                        	<span class=\"glyphicon glyphicon-trash\"></span>
                         </a>
					</div>
						<span class=\"glyphicon glyphicon-question-sign\" data-toggle=\"tooltip\" title=\"Aguardando resposta do DP.\"></span>
                       ";

				}elseif($row['status'] == 'CF'){

					$acoes = "<span style=\" margin-left:15px; \" class=\"glyphicon glyphicon-ok text-success\"></span>";

				}elseif($row['status'] == 'CA'){
					$acoes = "
							<div class=\"btn-group \">
								<a class=\"btn btn-default btn-xs jus-info\" style=\" margin-left:8px; \"  href=\"#\" data-toggle=\"modal\" data-target=\"#modal-info\" data-info=\"$row[feedback]\">
									<span class=\"glyphicon glyphicon-ban-circle text-danger\" ></span>
								</a>
							</div>
							";
				}elseif($row['status'] == 'PDCL'){
                    $acoes = "
						<div class=\"btn-group\">
							<a class=\"btn btn-success btn-xs\" href=\"javascript:aprovar($row[idponto_jus])\">
                                                            <span class=\"glyphicon glyphicon-floppy-saved\"></span>
                                                        </a>                                                        
							<a class=\"btn btn-danger btn-xs\"  href=\"javascript:recusar($row[idponto_jus])\">
								<span class=\"glyphicon glyphicon-floppy-remove\"></span>
							</a>
						</div>

                        <img alt=\"Brand\" style=\"display:none;\" class=\"img-load\" width=\"30\" height=\55\" src=".base_url('public/img/30.gif')." />";
				}elseif($row['status'] == 'CAGS'){
                    $acoes = "<a class=\"btn btn-info btn-xs jus-info bt-feed\"  href=\"#\" data-toggle=\"modal\" data-id=\"$row[idponto_jus]\" data-target=\"#modal-info\" data-info=\"$row[feedback]\">
								<span style=\" \" class=\"glyphicon glyphicon-pencil\" ></span>	
								<b><small>Motivo</small></b>
							 </a>
							  ";
                }

				$html .= "<tr id=\"tr_{$row['idponto_jus']}\">";
					$html .= "<td>".$row['em']."</td>";
				    $html .= "<td>".$row['nome']."</td>";
					$html .= "<td>".$row['filial']."</td>";
					$html .= "<td>".$row['data_bt']."</td>";
					$html .= "<td>".$row['data_add']."</td>";
					$html .= "<td>".$row['hora']."</td>";
                    $html .= "<td>".$row['motivo']."</td>";
					$html .= "<td>".$row['obs']."</td>";
					$html .= "<td>".$acoes."</td>";
				$html .= "</tr>";
			}

			return $html;
		}

		public function view_table_dp_jus($array, $id = null){
			$html = "";
			foreach ($array as $row) {

				$status = "";
				$acoes = "";


				if($row['status'] == 'PD'){
                                       $sess = $this->session->userdata('session');
                                       
                                    if($row['gestor'] === $sess['usuario']){
                                        $acoes = "
						<div class=\"btn-group\">
							<a class=\"btn btn-success btn-xs\" href=\"javascript:aprovar($row[idponto_jus])\">
                                                            <span class=\"glyphicon glyphicon-floppy-saved\"></span>
                                                        </a>
                                                        <a class=\"btn btn-danger btn-xs\"  href=\"javascript:excluir($row[idponto_jus])\">
                                                            <span class=\"glyphicon glyphicon-trash\"></span>
                                                        </a>
							<a class=\"btn btn-danger btn-xs\"  href=\"javascript:recusar($row[idponto_jus])\">
								<span class=\"glyphicon glyphicon-floppy-remove\"></span>
							</a>
						</div>

                        <img alt=\"Brand\" style=\"display:none;\" class=\"img-load\" width=\"30\" height=\55\" src=".base_url('public/img/30.gif')." />";
                                    
                                        
                                    } else {
                                        $acoes = "
						<div class=\"btn-group\">
							<a class=\"btn btn-success btn-xs\" href=\"javascript:aprovar($row[idponto_jus])\">
	                        	<span class=\"glyphicon glyphicon-floppy-saved\"></span>
	                        </a>
							<a class=\"btn btn-danger btn-xs\"  href=\"javascript:recusar($row[idponto_jus])\">
								<span class=\"glyphicon glyphicon-floppy-remove\"></span>
							</a>
						</div>

                        <img alt=\"Brand\" style=\"display:none;\" class=\"img-load\" width=\"30\" height=\55\" src=".base_url('public/img/30.gif')." />";
                                    }
					

				}elseif($row['status'] == 'CF'){

					$acoes = "<span style=\" margin-left:15px; \" class=\"glyphicon glyphicon-ok text-success\"></span>";

				}elseif($row['status'] == 'CA'){
					$acoes = "<a class=\"btn btn-info btn-xs jus-info bt-feed\"  href=\"#\" data-toggle=\"modal\" data-id=\"$row[idponto_jus]\" data-target=\"#modal-info\" data-info=\"$row[feedback]\">
								<span style=\" \" class=\"glyphicon glyphicon-pencil\" ></span>
								<b><small>Motivo</small></b>
							 </a>
							 ";
				}

				if($row['status'] == 'CAGS'){
					$acoes = '<small><b>Aguardando aprovação do gestor</b></small>';
				}


				$html .= "<tr id=\"tr_{$row['idponto_jus']}\">";
					$html .= "<td>".$row['em']."</td>";
					$html .= "<td>".$row['nome']."</td>";
					$html .= "<td>".$row['filial']."</td>";
					$html .= "<td>".$row['data_bt']."</td>";
					$html .= "<td>".$row['data_add']."</td>";
					$html .= "<td>".$row['hora']."</td>";
                                        $html .= "<td>".$row['motivo']."</td>";
					$html .= "<td>".$row['obs']."</td>";
					$html .= "<td>".$acoes."</td>";
				$html .= "</tr>";
			}

			return $html;

		}

		public function view_table_gestor_jus_obj($arrayT, $id = null){

			$array = $arrayT[0];
			if($array['status'] == 'PD'){
				if($_SESSION['session']['catg'] == 'gs'){
                    	$acoes = "<b><small>Aguardando aprovação do DP</small></b>";
				}

				if($_SESSION['session']['catg'] == 'dp'){
                    $acoes = "
						 <div class=\"btn-group btn-group-justified\">
						  	<a class=\"btn btn-danger btn-xs\"  href=\"javascript:recusar('$array[idponto_jus]')\">Recusar</a>
						</div>

				<img alt=\"Brand\" style=\"margin-top: -16px;\" width=\"100\" height=\55\" src=".base_url('public/img/logobranco.png')." />";
				}
			}elseif($array['status'] == 'CF'){

				$acoes = "<span style=\" margin-left:15px; \" class=\"glyphicon glyphicon-ok text-success\"></span>";

			}elseif($array['status'] == 'CA'){
				$acoes = "<a class=\"btn btn-info btn-xs jus-info bt-feed\"  href=\"#\" data-toggle=\"modal\" data-id=\"$array[idponto_jus]\" data-target=\"#modal-info\" data-info=\"$array[feedback]\">
								<span style=\" \" class=\"glyphicon glyphicon-pencil\" ></span>
								<b><small>Motivo</small></b>
							 </a>";
			}elseif($array['status'] == 'CAGS'){
                $acoes = "<a class=\"btn btn-info btn-xs jus-info bt-feed\"  href=\"#\" data-toggle=\"modal\" data-id=\"$array[idponto_jus]\" data-target=\"#modal-info\" data-info=\"$array[feedback]\">
								<span style=\" \" class=\"glyphicon glyphicon-pencil\" ></span>
								<b><small>Motivo</small></b>
							 </a>";
            }

			return array(
					  'id' => $array['idponto_jus'],
					  'em' => $array['em'],
					'nome' => $array['nome'],
				  'filial' => $array['filial'],
					'data' => $array['data_bt'],
                                        'data_add' => $array['data_add'],
					'hora' => $array['hora'],
                                        'motivo' => $array['motivo'],
					 'obs' => $array['obs'],
					'acao' => $acoes
			);

		}

	}

?>
