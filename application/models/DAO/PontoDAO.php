<?php 

	class PontoDAO extends CI_MODEL{

		private $usuario;

		 public function __construct(){

                parent::__construct();

                date_default_timezone_set('America/Fortaleza');
		 }

		 public function setUsuario($usuario){
		 	$this->usuario = $usuario;
		 }

		 public function getUsuario(){
		 	return $this->usuario;
		 }
		
		 public function query(){
		 	$query = $this->db->query("SELECT WEEKDAY('2016-11-28') not in(5,6)");
		 	
		 	return $query->result_array();
		 }
		
		public function atualizaStatus($id = NULL, $status = NULL){
		 	$data = array(
		 			'status' => $status
		 	);
		 	
		 			   $this->db->where('id', $id);
		 		return $this->db->update('tab_ponto_batidas', $data);
		 	
		 		
		 	if($error = $this->db->error()){
		 		echo "erro";
		 		
		 		$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
		 	}
		 }
		 
		public function listaBatidasDia(){
				
				$query = $this->db->query("SELECT *, 
											DATE_FORMAT(data_,'%d/%m/%Y') 
												as data_bt
												FROM tab_ponto_batidas 
												WHERE data_ 
												BETWEEN DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND CURDATE()
												AND usuario = '{$this->getUsuario()}' ORDER BY data_ DESC, hora DESC
											");
				
				$data = array();
				if($query !== FALSE && $query->num_rows() > 0){
					$data = $query->result_array();
				}
				
				return $data;
		}

		public function listaBatidasMes(){
			$sql = "SELECT *, DATE_FORMAT(data_,'%d/%m/%Y') 
					AS data_bt 
					FROM tab_ponto_batidas 
					WHERE usuario = '{$this->getUsuario()}'
					AND EXTRACT(YEAR_MONTH FROM data_) BETWEEN EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 2 MONTH) 
					AND EXTRACT(YEAR_MONTH FROM CURDATE()) ORDER BY data_ DESC, hora DESC";

				if ( $query = $this->db->query($sql) )
				{
        				$query = $this->db->query($sql);
        				return $query->result_array();
				}else{
					return $this->db->error(); // Has keys 'code' and 'message'
				}
		}

		public function batidaIntervaloHora(){
				$horaUltimaBatida;
				$horaAtual = date("H:i:s");

				$query = $this->db->query("
					SELECT hora FROM tab_ponto_batidas 
					WHERE usuario = '{$this->getUsuario()}' 
					and status <> 'ca'
					and data_ = CURDATE() 
					ORDER BY hora DESC LIMIT 1"
				);

				if(count($query->result()) == 0){
					return array('success' => true);	
					exit();
				}

	  		 	foreach ($query->result() as $row)
				{
   				     $horaUltimaBatida = $row->hora;
				}	
	     		
				$query = $this->db->query("SELECT TIME_TO_SEC(TimeDiff('{$horaAtual}','{$horaUltimaBatida}')) as diferenca");

				foreach ($query->result() as $row)
				{
   				     $diferenca = $row->diferenca;
				}
				
				$diferencaFalta =  3600 - $diferenca;
				$qminutos = gmdate("H:i:s", $diferencaFalta);
				
				if($diferenca >= 3600){
					return array('success' => true);	
				}else{
					return array('success' => false, 'message' => "Aguarde: $qminutos minutos");	
				}
				
		}

		public function inserePonto($obj){

			if($a = $this->db->insert('tab_ponto_batidas', $obj)){
				return $a;
			}else{
				return false;
			}
			
		}


		public function select_batida_export($post){

			if(isset($post['filial']) && $post['filial'] != ""){
				$this->db->where('filial', $post['filial']);	
			}

			if(isset($post['nome']) && $post['nome'] != ""){
				$this->db->where('nome', $post['nome']);	
			}

			if(isset($post['data1']) && isset($post['data2'])){
				
				
				if(!empty($post['data1']) && !empty($post['data2'])){

					$dataArray1 = explode("/", $post['data1']);
				 	$newData1 = $dataArray1[2]."-".$dataArray1[1]."-".$dataArray1[0];

				 	$dataArray2 = explode("/", $post['data2']);
				 	$newData2 = $dataArray2[2]."-".$dataArray2[1]."-".$dataArray2[0];

					$this->db->where("data_ BETWEEN $newData1 AND $newData2");
				}else{

					if($post['data1'] != "" ){
						$dataArray1 = explode("/", $post['data1']);
				 		$newData1 = $dataArray1[2]."-".$dataArray1[1]."-".$dataArray1[0];
						
						$this->db->where('data1', $newData1);	
					}

					if($post['data2'] != ""){
						$dataArray2 = explode("/", $post['data2']);
				 		$newData2 = $dataArray2[2]."-".$dataArray2[1]."-".$dataArray2[0];

						$this->db->where('data2', $newData2);	
					}

				}

			}


			if(isset($post['status']) && $post['status'] != ""){
				$this->db->where("status", $post['status']);	
			}

			
			$ver = false;
			foreach ($post as $key => $value) {
				if(!empty($value)){
					$ver = true;
				}
			}

			
			if($ver){
				//$this->db->where("status <>", 'ca');	
				$sql = $this->db->get_compiled_select('tab_ponto_batidas');	
				var_dump($sql);

				$query = $this->db->get('tab_ponto_batidas');	
			}else{
				$this->db->where('LEFT(data_,7)',date("Y-m"));	
				//$this->db->where('status <>', 'ca');	
				$sql = $this->db->get_compiled_select('tab_ponto_batidas');	
				$query = $this->db->get('tab_ponto_batidas');	
			}

			
			//$query = $this->db->get('tab_ponto_batidas');

			$test = array();
				
			if($query !== FALSE && $query->num_rows() > 0){
			
				$test = $query->result_array();
			
			}
				
			return $test;

		}


		public function view_table_ponto_user($array){
			$html = "";
			
			foreach ($array as $row) {
				


				$status = "";
				
				if($row['status'] != 'CA'|| $row['status'] != 'EX'){



				if($row['status'] == 'PD'){
					$status = "<span style=\" margin-left:15px; \" class=\"glyphicon glyphicon-question-sign\"></span>";
				}elseif($row['status'] == 'CF'){
					$status = "<span style=\" margin-left:15px; \" class=\"glyphicon glyphicon-ok\"></span>";
				}

				$html .= "<tr>";
					$html .= "<td>".$row['em']."</td>";
					$html .= "<td>".$row['nome']."</td>";
					$html .= "<td>".$row['filial_origem']."</td>";
					$html .= "<td>".$row['filial_autenticado']."</td>";
					$html .= "<td>".$row['data_bt']."</td>";
					$html .= "<td>".$row['hora']."</td>";
					$html .= "<td>".$status."</td>";
				$html .= "</tr>";


				}

			}
			
			return $html;
		}
		

		
	}

?>