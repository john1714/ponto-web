<?php
 class GlobalDAO extends CI_Model{

 	private $conection;

 	public function __construct(){

 		parent::__construct();

 		try{
 			$this->load->model("DAO/ConnFactory","conn");

 			$this->conection = $this->conn->conn();
 		}catch(Exception $e){
 			echo $e->getMessage();
 		}



 	}


 	public function getFiliais(){
 		$sql = "SELECT iuorg
 				FROM bentt0
 				WHERE ient = 'TEC'
 				AND ient0 = 'TEC'
 				AND iuorgt = 'UO'
 				AND ientt0 = '*'
 				AND ientt = '*'
 				AND cst = 'E0'
 				ORDER BY ientt ,ient ,iuorgt0 ,iuorg0 ,xord ,iuorg";

 		$stmt = $this->conection->prepare($sql);
 		$stmt->execute();

 		$ln = $stmt->fetchAll();
 		return $ln;
 	}

 	public function isFilial($filial){
 		$sql = "select count(*) as cont
 				from bentt0
 				with(nolock)
 				where ient = 'TEC'
 				and iuorgt = 'UO'
 				and ientt = '*'
 				and iuorg = '$filial'";

 		$stmt = $this->conection->prepare($sql);
 		$stmt->execute();

 		$cont  = $stmt->fetch(PDO::FETCH_ASSOC);

 		return ($cont['cont']) ? true : false;
 	}

  public function getCnpjFilial($filial = null, $check = true){
        $sql = "SELECT tidfisc FROM bentt0
               WHERE ient = 'TEC'
               AND ient0 = 'TEC'
               AND iuorgt = 'UO'
               AND ientt0 = '*'
               AND ientt = '*'
               AND cst = 'E0'
               AND iuorg = '{$filial}'
               ORDER BY ientt ,ient ,iuorgt0 ,iuorg0 ,xord";

        $stmt = $this->conection->prepare($sql);
     		$stmt->execute();

     		$ln  = $stmt->fetch(PDO::FETCH_ASSOC);

        if($check === NULL){
            $rp = array('.','/','-');
            $cnpj = str_replace($rp, '', $ln['tidfisc']);
        }elseif ($check == true) {
            $rp = array('.','/','-');
            $cnpj = str_replace($rp, '', $ln['tidfisc']);
        }elseif ($check === false) {
            $cnpj = $ln['tidfisc'];
        }

				return $cnpj;
  }



 }
