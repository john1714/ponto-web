<?php

class UsuarioDAO extends CI_MODEL
{

    private $conn;

    public function __construct()
    {
        parent::__construct();
        try {
            $this->load->model('DAO/ConnFactory', 'sqlv');
            $this->conn = $this->sqlv->conn();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    public function getPis($em)
    {
        try {
            $sql = "select tidfisc3 from bentt0 with (NOLOCK) where ientt = 'EM' and ient = '{$em}'";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            while ($ln = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $pis = $ln['tidfisc3'];
            }
            return $pis;
        } catch (Exception $e) {
            throw new Exception("Não foi possivel recuperar o pis");
        }
    }


    public function isGestor($em)
    {
        $cat = $this->getCategoria($em);
        $catgs = array("000004", "000005", "000006");
        if (!in_array($cat, $catgs)) {
            return false;
        } else {
            return true;
        }
    }


    public function getCategoria($em)
    {
        $sql_1 = "select c.tid as filial, b.tid as categ, a.ient, a.nent, a.temail
						from bentt0 a with(nolock)
						 inner join benxt0 b with(nolock)
						 on  b.ccx   = 'RH'
						 and b.ientt0 = a.ientt
						 and b.ient0  = a.ient
						 and b.xord  = 'CATG'
						 --and b.tid   = filial
						 and REPLACE(CONVERT(CHAR,GETDATE(),102),'.','-') between b.dini and b.dfim
						 inner join benxt0 c with(nolock)
						 on  c.ccx   = 'RH'
						 and c.ientt0 = a.ientt
						 and c.ient0  = a.ient
						 and c.xord  = 'UORG'
						 --and c.tid   = canal
						 and REPLACE(CONVERT(CHAR,GETDATE(),102),'.','-') between c.dini and c.dfim
						where a.ientt = 'EM'
						and a.cst  = 'E0'
						and a.ient = '" . $em . "' ";
        try {
            $stmt = $this->conn->prepare($sql_1);
            $stmt->execute();
            $ln = $stmt->fetch(PDO::FETCH_ASSOC);
            (string)$categ = trim($ln['categ']);
            return $categ;
        } catch (Exception $e) {
            throw new Exception("Não foi possivel recuperar a categoria");
        }

    }

    public function getCanal($em)
    {
        $sql_1 = "select c.tid as filial, b.tid as canal, a.ient, a.nent, a.temail
      						from bentt0 a with(nolock)
      						 inner join benxt0 b with(nolock)
      						 on  b.ccx   = 'RH'
      						 and b.ientt0 = a.ientt
      						 and b.ient0  = a.ient
      						 and b.xord  = 'ACTV'
      						 --and b.tid   = filial
      						 and REPLACE(CONVERT(CHAR,GETDATE(),102),'.','-') between b.dini and b.dfim
      						 inner join benxt0 c with(nolock)
      						 on  c.ccx   = 'RH'
      						 and c.ientt0 = a.ientt
      						 and c.ient0  = a.ient
      						 and c.xord  = 'UORG'
      						 --and c.tid   = canal
      						 and REPLACE(CONVERT(CHAR,GETDATE(),102),'.','-') between c.dini and c.dfim
      						where a.ientt = 'EM'
      						and a.cst  = 'E0'
      						and a.ient = '" . $em . "' ";
        try {
            $stmt = $this->conn->prepare($sql_1);
            $stmt->execute();
            $ln = $stmt->fetch(PDO::FETCH_ASSOC);
            return $ln ['canal'];
        } catch (Exception $exception) {
            throw new Exception("Não foi possivel recuperar o canal.");
        }
    }

    public function getInfoCol($em)
    {
        $sql_1 = "select c.tid as filial, b.tid as canal, a.ient, a.nent, a.temail 
						from bentt0 a with(nolock) 
						 inner join benxt0 b with(nolock) 
						 on  b.ccx   = 'RH' 
						 and b.ientt0 = a.ientt  
						 and b.ient0  = a.ient  
						 and b.xord  = 'ACTV' 
						 --and b.tid   = filial
						 and REPLACE(CONVERT(CHAR,GETDATE(),102),'.','-') between b.dini and b.dfim 
						 inner join benxt0 c with(nolock) 
						 on  c.ccx   = 'RH' 
						 and c.ientt0 = a.ientt 
						 and c.ient0  = a.ient  
						 and c.xord  = 'UORG' 
						 --and c.tid   = canal 
						 and REPLACE(CONVERT(CHAR,GETDATE(),102),'.','-') between c.dini and c.dfim 
						where a.ientt = 'EM' 
						and a.cst  = 'E0' 
						and a.ient = '" . $em . "' ";

        try {
            $stmt = $this->conn->prepare($sql_1);
            $stmt->execute();
            $ln = $stmt->fetch(PDO::FETCH_ASSOC);
            return $ln;
        } catch (Exception $exception) {
            throw new Exception("Não foi possivel recuperar suas informações.");
        }
    }

    public function getColaboradores_cf($ln)
    {
        $sql_1 = "select c.tid as filial, b.tid as canal, a.ient, a.nnomel, a.temail 
								from bentt0 a with(nolock) 
								 inner join benxt0 b with(nolock) 
								 on  b.ccx   = 'RH' 
								 and b.ientt0 = a.ientt  
								 and b.ient0  = a.ient  
								 and b.xord  = 'ACTV' 
								 and b.tid   = '$ln[canal]'
								 and REPLACE(CONVERT(CHAR,GETDATE(),102),'.','-') between b.dini and b.dfim 
								 inner join benxt0 c with(nolock) 
								 on  c.ccx    = 'RH' 
								 and c.ientt0 = a.ientt 
								 and c.ient0  = a.ient  
								 and c.xord   = 'UORG' 
								 and c.tid    = '$ln[filial]'
								 and REPLACE(CONVERT(CHAR,GETDATE(),102),'.','-') between c.dini and c.dfim 
								where a.ientt = 'EM' 
								and a.cst  = 'E0' 
								--and a.ient <> '$ln[ient]' ";
        $stmt = $this->conn->prepare($sql_1);
        $stmt->execute();
        $dados = array();
        while ($ln = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $dados[] = $ln;
        }
        return $dados;
    }

    public function listaColaboradorGestor($em)
    {

        $sql_1 = "select b.ivalk as filial, b.nctx as canal,nnomel ,a.ient as em, a.nent as nome, a.temail
				from bentt0 a with(nolock)
				inner join bctxt0 b with(nolock) 
				on b.ictx = 'PONTOWEB*GESTOR'
				and b.tclass = '{$em}'
				and REPLACE(CONVERT(CHAR,GETDATE(),102),'.','-') between b.dini and b.dfim
				and b.cst = 'PD'
				where a.ientt = 'EM'
				--and a.ient = '002910'
				and a.cst = 'E0'
				and dbo.benxposxn('TEC',a.ientt,a.ient,REPLACE(CONVERT(CHAR,GETDATE(),102),'.','-'),'ACTV') = b.nctx
				and dbo.benxposxn('TEC',a.ientt,a.ient,REPLACE(CONVERT(CHAR,GETDATE(),102),'.','-'),'UORG') = b.ivalk 
				--and (a.ient <> b.tclass or a.ient = b.tclass)
				order by filial, canal";

        try {

            $stmt = $this->conn->prepare($sql_1);
            $stmt->execute();
            $dados = array();
            while ($ln = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $dados[] = $ln;
            }
            return $dados;
        } catch (Exception $exception) {
            throw new Exception("Não foi possivel recuperar seus colaboradores.");
        }
    }

    public function isDp($em)
    {
        try {
            $canal = (int)$this->getCanal($em);
            if ($canal === 1012) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

}

?>
