<?php
 class DpDAO extends CI_Model{


 	public function getBatidasQuery($sql){


 		$query = $this->db->query($sql);

 		$test = array();

 		if($query !== FALSE && $query->num_rows() > 0){

 			$test = $query->result_array();

 		}

 		return $test;

 	}

 	public function exeQuery($sql){

		 	$query = $this->db->query($sql);

		 	$test = array();

	 		if($query !== FALSE && $query->num_rows() > 0){

	 			$test = $query->result_array();

	 		}

	 		return $test;

 	}

 	public function prepare_query($array){

 		$filial = isset($array['filial']) ? $array['filial'] : "";
 		$nome   = isset($array['nome'])   ? $array['nome']   : "";
 		$data1  = isset($array['data1'])  ? $array['data1']  : "";
 		$data2  = isset($array['data2'])  ? $array['data2']  : "";
 		$status = isset($array['status']) ? $array['status'] : "";
 		$anomes = date("Y-m");

 		if(!empty($data1)){
 			$dataArray1 = explode("/", $data1);
 			$newData1 = $dataArray1[2]."-".$dataArray1[1]."-".$dataArray1[0];
 		}

 		if(!empty($data2)){
 			$dataArray2 = explode("/", $data2);
 			$newData2 = $dataArray2[2]."-".$dataArray2[1]."-".$dataArray2[0];
 		}

 		$where = Array();


 		if(!empty($nome)){$where[] ="nome LIKE '{$nome}%' ";}
 		if(!empty($filial)){$where[] = " filial_origem = '{$filial}'";}
 		if(!empty($status)){$where[] = " status = '{$status}' "; }
 		if(!empty($newData1) && empty($newData2)){$where[] = " data_ = '{$newData1}'";}
 		if(!empty($newData1) && !empty($newData2)){$where[] =" status <> 'CA' and status <> 'EX' and data_ BETWEEN '{$newData1}' and '{$newData2}' ORDER BY data_ DESC ";}
 		//if(empty($newData1) && empty($newData2)){$where[] =" WHERE LEFT(data_,7) = '{$anomes}' AND status <> 'CA' and status <> 'EX'  ";}

 		$sql = "SELECT *, DATE_FORMAT(data_,'%d/%m/%Y') as data_bt FROM tab_ponto_batidas";

 		if(sizeof($where)){

 			return $sql .= " WHERE ".implode(" AND ",$where );

 		}else{

 			return $sql .= " WHERE LEFT(data_,7) = '{$anomes}' AND status <> 'CA' and status <> 'EX' ORDER BY id DESC LIMIT 30";

 		}

 	}



 	public function view_table_ponto($array, $id = null){
 		$html = "";
 		$acoes = "";
 		foreach ($array as $row) {
		

 			if($row['status'] !== 'CA'){

	 			if($row['status'] === 'PD'){

	 				$acoes = " <a href=\"javascript:void(0);\"
	 					          data-toggle=\"confirmation\"
	 					          data-id=\"$row[id]\"
	 					          data-title=\"Deseja excluir?\"
	 					          data-placement=\"left\"

	 						      class=\" btn btn-default toggle-confirmation\">
	 				              <span class=\"glyphicon glyphicon-trash\"></span>
	 						   </a> ";
	 				//javascript:deletabatida($row[idponto]);
	 			}elseif($row['status'] === 'CF'){

          $acoes = "<span class=\"glyphicon glyphicon-ok text-success \"></span>";

	 			}

	 			$html .= "<tr id=\"tr_{$row['id']}\">";
	 			$html .= "<td>".$row['em']."</td>";
	 			$html .= "<td>".$row['nome']."</td>";
	 			$html .= "<td>".$row['filial_origem']."</td>";
	 			$html .= "<td>".$row['filial_autenticado']."</td>";
	 			$html .= "<td>".$row['ip']."</td>";
	 			$html .= "<td>".$row['data_bt']."</td>";
	 			$html .= "<td>".$row['hora']."</td>";
	 			$html .= "<td>".$acoes."</td>";
	 			$html .= "</tr>";
	 		}
		      
 		}

 		return $html;
 	}

 }
 ?>
