<?php class GlobalLocalDAO extends CI_Model{


	public function __construct(){

		parent::__construct();

		$this->load->model("BD_log");

	}

	// TAB IP

	public function insertIpPadrao($data = array()){

		$query = array();

		isset($data['ip'])             ? $query['ip'] = $data['ip']     : null;

		isset($data['status'])         ? $query['status'] = $data['status'] : null;

		return $this->db->insert('tab_ip_padrao', $query);

		if($error = $this->db->error()){
			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
		}
	}

	public function updateIpPadrao($data = array()){

		$query = array();

		isset($data['status']) ? $query['status'] = $data['status'] : null;

		$this->db->where('codip', $data['id']);

		return $this->db->update('tab_ip_padrao', $query);

		if($error = $this->db->error()){
			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
		}


	}

	public function getIpPadrao($data = array()){

		isset($data["ip"])     ? $this->db->where('ip'    ,$data['ip'])     : null;

		isset($data['status']) ? $this->db->where('status',$data['status']) : null;

		$this->db->select('*');

		$query = $this->db->get('tab_ip_padrao');

		$ln = array();

		if($query !== FALSE && $query->num_rows() > 0){
			$ln = $query->result_array();
		}

		return $ln;

		if($error = $this->db->error()){
			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
		}
	}

	public function getIpFixo($data = array()){



		isset($data['status']) ? $this->db->where('status'        ,$data['status']) : null;

		isset($data["ip"])     ? $this->db->where('ip'            ,$data['ip'])     : null;

		isset($data["filial"]) ? $this->db->where('filial_usuario',$data['filial']) : null;

		$query = $this->db->get('tab_ip_fixo');

		$ln = array();

		if($query !== FALSE && $query->num_rows() > 0){
			$ln = $query->result_array();
		}

		return $ln;

				 if($error = $this->db->error()){
				    $this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
				 }
	}

	public function getFilial($val = array()){

		isset($val["filial"]) ? $this->db->where('filial', $val['filial']) : null;

		isset($val["status"]) ? $this->db->where('status', $val['status']) : null;

		$this->db->select('*');

		$query = $this->db->get('tab_filial_mirror');

		$ln = array();

		if($query !== FALSE && $query->num_rows() > 0){
			$ln = $query->result_array();
		}

		return $ln;

		if($error = $this->db->error()){
			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
		}
	}


	public function insertIpFixo($data = array()){

				$query = array();

				isset($data['ip'])             ? $query['ip'] = $data['ip']     : null;

				isset($data['filial']) ? $query['filial_usuario'] = $data['filial'] : null;

				isset($data['status'])         ? $query['status'] = $data['status'] : null;


				return $this->db->insert('tab_ip_fixo', $query);

				if($error = $this->db->error()){
					$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
				}
	}

	public function updateIpFixo($data = array()){

		$query = array();

		isset($data['ip'])             ? $query['ip'] = $data['ip']     : null;

		isset($data['filial_usuario']) ? $query['filial_usuario'] = $data['filial'] : null;

		isset($data['status'])         ? $query['status'] = $data['status'] : null;

		$this->db->where('codip', $data['id']);

		return $this->db->update('tab_ip_fixo', $query);

		if($error = $this->db->error()){
			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
		}


	}

	public function updateStatus($id = NULL, $status){
			$data = array(
					'status' => $status
			);

				$this->db->where('codip', $id);
				return $this->db->update( $data);

				if($error = $this->db->error()){
					$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
				}
	}


	// TAB FILIAL

	public function getEspFilial($data = NULL){

		$query = array();

		isset($data['filial']) ? $this->db->where('filial', $data['filial']) : null;

		isset($data['status']) ? $this->db->where('status', $data['status']) : null;

		$this->db->select("*");

		$this->db->from('tab_filial_mirror');

		$query = $this->db->get();


		$data = array();

		if($query !== FALSE && $query->num_rows() > 0){
			$data = $query->result_array();
		}

		return $data;


		if($error = $this->db->error()){
			$this->BD_log->insert_log_query($this->db->error(),$this->db->last_query());
		}
	}


}?>
