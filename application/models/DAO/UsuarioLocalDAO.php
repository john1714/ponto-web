<?php

class UsuarioLocalDAO extends CI_MODEL{

	private $conn;

	public function __construct(){

		parent::__construct();


		}


		public function isLocalGestor($user){
			$this->db->select("count(*) as count");
			$this->db->from('tab_gestor_acesso');
			$this->db->where('usuario', $user);
			$this->db->where('status', 'cf');
			$query = $this->db->get();

			$ln = $query->result_array();
			$count = $ln[0]['count'];

			return ($count) ? true : false;
		}

		public function isLocalDp($user){
			$this->db->select("count(*) as count");
			$this->db->from('tab_dp_acesso');
			$this->db->where('usuario', $user);
			$this->db->where('status' , 'cf');
			$query = $this->db->get();

			$ln = $query->result_array();

			$count = $ln[0]['count'];

			return ($count) ? true : false;
		}

}
?>
