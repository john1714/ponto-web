<?php

	class Filial extends CI_Model{

	// CNPJ DAS FILIAIS

		public function getF00(){return "07272825000104";}

		public function getF01(){return "07272825000295";}

		public function getF02(){return "07272825000376";}

		public function getF03(){return "07272825000457";}

		public function getF04(){return "07272825000538";}

		public function getF05(){return "07272825000708";}

		public function getF06(){return "07272825000880";}

		public function getF07(){return "07272825001003";}

		public function getF08(){return "07272825000961";}

		public function getF09(){return "07272825001186";}

		public function getF10(){return "07272825001267";}

		public function getF11(){return "07272825001348";}

		public function getF12(){return "07272825001429";}

		public function getF13(){return "07272825001690";}

		public function getF14(){return "07272825001500";}

		public function getF15(){return "07272825001771";}

		public function getF16(){return "07272825001852";}

		public function getF17(){return "07272825001933";}

		public function getF18(){return "07272825002077";}

		public function getF19(){return "07272825002158";}

		public function getF20(){return "07272825002239";}

		public function getF21(){return "07272825002310";}

		public function getF22(){return "07272825002409";}

		public function getF23(){return "07272825002581";}

		public function getF24(){return "07272825002662";}

		public function getF25(){return "07272825002743";}

		public function getF26(){return "07272825002824";}

		public function getF27(){return "07272825002905";}

		public function getF28(){return "07272825003049";}

		public function getF29(){return "07272825003120";}

		public function getF30(){return "07272825003200";}

		public function getF31(){return "07272825003391";}

		public function getF32(){return "07272825003472";}

	// NÚMERO DE SÉRIE DOS RELÓGIOS
	public function getNF00() {return "00001000680004027";}

	public function getNF01() {return "00001000680004032";}

	public function getNF02() {return "00001000680004040";}

	public function getNF03() {return "00030001580002227";}

	public function getNF04() {return "00043002180000805";}

	public function getNF05() {return "00001000680003424";}

	public function getNF06() {return "00001000680003469";}

	public function getNF08() {return "00001000680004035";}

	public function getNF09() {return "00001000680004176";}

	public function getNF10() {return "00001000680003426";}

	public function getNF11() {return "00001000680003412";}

	public function getNF12() {return "00001000680005296";}

	public function getNF13() {return "00001000680005627";}

	public function getNF14() {return "00001000680005628";}

	public function getNF15() {return "00001000680005667";}

	public function getNF16() {return "00001000680006873";}

	public function getNF17() {return "00001000680006812";}

	public function getNF18() {return "00001000680008098";}

	public function getNF19() {return "00001000680007206";}

	public function getNF20() {return "00001000680008489";}

	public function getNF22() {return "00001000680008489";}

	public function getNF23() {return "00043002180000015";}

	public function getNF24() {return "00043002180000001";}

	public function getNF25() {return "00043002180000577";}

	public function getNF26() {return "00043002180000589";}

	public function getNF27() {return "00043002180000871";}

	public function getNF28() {return "00030001580001777";}

	public function getNF29() {return "0041407122024219";}

	public function getNF30() {return "00003001580001778";}

	public function getNF32() {return "0041409042030981";}

	public function getNF34() {return "00030002360000517";}

		public function setFilial($_filial){
			switch ($_filial){

				case "F00":
					return $this->getF00();
				break;

				case "F01":
					return $this->getF01();
				break;

				case "F02":
					return $this->getF02();
				break;

				case "F03":
					return $this->getF03();
				break;

				case "F04":
					return $this->getF04();
				break;

				case "F05":
					return $this->getF05();
				break;

				case "F06":
					return $this->getF06();
				break;

				case "F07":
					return $this->getF07();
				break;

				case "F08":
					return $this->getF08();
				break;

				case "F09":
					return $this->getF09();
				break;

				case "F10":
					return $this->getF10();
				break;

				case "F11":
					return $this->getF11();
				break;

				case "F12":
					return $this->getF12();
				break;

				case "F13":
					return $this->getF13();
				break;

				case "F14":
					return $this->getF14();
				break;

				case "F15":
					return $this->getF15();
				break;

				case "F16":
					return $this->getF16();
				break;

				case "F17":
					return $this->getF17();
				break;

				case "F18":
					return $this->getF18();
				break;

				case "F19":
					return $this->getF19();
				break;

				case "F20":
					return $this->getF20();
				break;

				case "F21":
					return $this->getF21();
				break;

				case "F22":
					return $this->getF22();
				break;

				case "F23":
					return $this->getF23();
				break;

				case "F24":
					return $this->getF24();
				break;

				case "F25":
					return $this->getF25();
				break;

				case "F26":
					return $this->getF26();
				break;

				case "F27":
					return $this->getF27();
				break;

				case "F28":
					return $this->getF28();
				break;

				case "F29":
					return $this->getF29();
				break;

				case "F30":
					return $this->getF30();
				break;

				case "F31":
					return $this->getF31();
				break;

				case "F32":
					return $this->getF32();
				break;

			}
		}

			//####################


			public function setNFilial($_nfilial){
			switch ($_nfilial){

				case "F00":
					return $this->getNF00();
				break;

				case "F01":
					return $this->getNF01();
				break;

				case "F02":
					return $this->getNF02();
				break;

				case "F03":
					return $this->getNF03();
				break;

				case "F04":
					return $this->getNF04();
				break;

				case "F05":
					return $this->getNF05();
				break;

				case "F06":
					return $this->getNF06();
				break;

				case "F08":
					return $this->getNF08();
				break;

				case "F09":
					return $this->getNF09();
				break;

				case "F10":
					return $this->getNF10();
				break;

				case "F11":
					return $this->getNF11();
				break;

				case "F12":
					return $this->getNF12();
				break;

				case "F13":
					return $this->getNF13();
				break;

				case "F14":
					return $this->getNF14();
				break;

				case "F15":
					return $this->getNF15();
				break;

				case "F16":
					return $this->getNF16();
				break;

				case "F17":
					return $this->getNF17();
				break;

				case "F18":
					return $this->getNF18();
				break;

				case "F19":
					return $this->getNF19();
				break;

				case "F20":
					return $this->getNF20();
				break;

				case "F22":
					return $this->getNF22();
				break;

				case "F23":
					return $this->getNF23();
				break;

				case "F24":
					return $this->getNF24();
				break;

				case "F25":
					return $this->getNF25();
				break;

				case "F26":
					return $this->getNF26();
				break;

				case "F27":
					return $this->getNF27();
				break;

				case "F28":
					return $this->getNF28();
				break;

				case "F29":
					return $this->getNF29();
				break;

				case "F30":
					return $this->getNF30();
				break;

				case "F32":
					return $this->getNF32();
				break;

				case "F34":
					return $this->getNF34();
				break;

			}



		}


	}

?>
