<?php 
class Justificativa extends CI_MODEL{

	private $gestor;
    private $nome;
    private $user;
    private $em;
    private $filial;
    private $pis;
    private $data_;
    private $data_add;
    private $hora;
    private $motivo;
    private $obsr;
    private $status;
    
    public function setNome($nome = NULL){
    	
    	if(empty($nome) || $nome === NULL){ throw new Exception('Nome'); }  $this->nome = $nome;
    	
    }

	public function setUsuario($usuario = NULL){
    	
    	if(empty($usuario)){ throw new Exception( 'Usuário' ); }  $this->user = $usuario;
    	
    }

    public function setEm($em = NULL){
    	
    	if(empty($em) || $em === NULL){ throw new Exception( 'Em' ); }  $this->em = $em;
    	
    }

    public function setFilial($filial = NULL){
    	
    	if(empty($filial) || $filial === NULL){ throw new Exception( 'Filial' ); }  $this->filial = $filial;
    	
    }

    public function setPis($pis = NULL){
    	
    	if(empty($pis) || $pis === NULL){ throw new Exception( 'Pis' ); }  $this->pis = $pis;
    	
    }

	public function setData($data){
    	
    	if(empty($data) || $data == NULL){ throw new Exception( 'Data da justificativa colaborador' ); }  $this->data_ = $data;
    	
    }

    public function setDataRegistro($dataR = NULL){
    	
    	if(empty($dataR) || $dataR === NULL){ throw new Exception( 'Data da insersão do registro' ); }  $this->data_add = $dataR;
    	
    }

    public function setHora($hora = NULL){
    	
    	if(empty($hora) || $hora === NULL){ throw new Exception( 'Hora' ); }  $this->hora = $hora;
    	
    }

    public function setMotivo($motivo = NULL){
    	
    	if(empty($motivo) || $motivo === NULL){ throw new Exception( 'Motivo' ); }  $this->motivo = $motivo;
    	
    }      	

    public function setObservacao($observacao = NULL){
    			
    	if(empty($observacao) || $observacao === NULL){ throw new Exception( 'Observação' ); }  $this->obsr = $observacao;
    	
    } 

    public function setStatus($status = NULL){
    	
    	if(empty($status) || $status === NULL){ throw new Exception( 'Status' ); }  $this->status = $status;
    	
    } 

    public function setGestor($gestor = NULL){
    	$gestor = trim($gestor);
    	if(empty($gestor) || $gestor === NULL){ throw new Exception( 'Usuário de Gestor' ); }  $this->gestor = $gestor;
    	
    } 

    public function getObject(){
                    
            $data = array(
                       
            			'nome'     => $this->nome,
 
						'user'     => $this->user,
						 
						'em'       => $this->em,
						   
						'filial'   => $this->filial,
						
						'pis'      => $this->pis,
						  
						'data_'    => $this->data_,

						'data_add' => $this->data_add,
						
						'hora'     => $this->hora,
						 
						'motivo'   => $this->motivo,
						
						'obs'      => $this->obsr,
						  
						'status'   => $this->status,

						'gestor'   => $this->gestor
                    
            );
            
            return (object) $data;
            
    }

}
?>