<?php

	class Ponto extends CI_MODEL{

		 public function __construct()
        {
                parent::__construct();
        }

        private $direct = APPPATH."arquivos/";

        public function gerar_txt($linha, $post){

        	$this->load->model("Obj/Filial","filial");
        	$this->load->model("DAO/GlobalDAO","glob");
        	$this->load->helper('file');

        	$direct = APPPATH."arquivos/";
        	$a = 1;

            $datIni = $post['data1'];
            $datFim = $post['data2'];


        	//CRIA ARQUIVO E ADICIONA O CABEÇALHO E ACORDO COM O PADRÃO
        	foreach($linha as $ln => $n){

        		$dat = explode("/",$n['data_bt']);
        		$ano = $dat[2];
        		$mes = $dat[1];
        		$dia = $dat[0];
        		// Por Filial e dia
        		//$abertow=fopen($direct.$n['filial_origem'].$ano.$mes.$dia.".txt","w",777);//abrindo e zerando o arquivo;
                //Por Filial apenas
				$abertow=fopen($direct.$n['filial_origem'].".txt","w",777);//abrindo e zerando o arquivo;
        		$escrever = "00000000011".trim($this->glob->getCnpjFilial($n['filial_origem']))."000000000000".$n['filial_origem']." TECNO INDUSTRIA E COMERCIO DE COMPUTADORES LTDA                                                                                                    ".$this->filial->setNFilial($n['filial_origem'])."".$this->tiraBarra($post['data1'])."".$this->tiraBarra($post['data2'])."".date('dmY')."".date("H")."".date("i")."\r\n";
        		fwrite($abertow,$escrever);//escrevendo o arquivo
        		fclose($abertow);
        	}

        	$count = 1;
        	$i = 0;

        	//ADICIONA REGISTROS DE BATIDA NO ARQUIVO, ABAIXO DO CABEÇALHO
        	foreach($linha as $ln => $n){
        		$dat = explode("/",$n['data_bt']);
        		$ano = $dat[2];
        		$mes = $dat[1];
        		$dia = $dat[0];

        		// Por Filial e dia
        		//$abertoa=fopen($direct.$n['filial_origem'].$ano.$mes.$dia.".txt","a",777);//abrindo e zerando o arquivo;
                // Por Filial Apenas
                $abertoa=fopen($direct.$n['filial_origem'].".txt","a",777);//abrindo e zerando o arquivo;
        		fwrite($abertoa,substr(str_pad($n['id'], 9, "0", STR_PAD_LEFT),0,9)."3".$this->tiraBarra($n['data_bt']).$this->organizaHora($n['hora']).$this->limitarTexto($n['pis'],12)."\r\n");//escrevendo o arquivo

				//ALTERA VALOR DO STATUS PARA '1'
        		$this->db->set('status', 'CF');
        		$this->db->where('id', $n['id']);
        		$this->db->update('tab_ponto_batidas');

        	}


        	/* Diretorio que deve ser lido */

        	$dir = $direct;

        	/* Abre o diretório */

        	$pasta= opendir($dir);

        	/* Loop para ler os arquivos do diretorio */

        	while ($arquivo = readdir($pasta)){

        		/* Verificacao para exibir apenas os arquivos e nao os caminhos para diretorios superiores */

        		if ($arquivo != "." && $arquivo != ".."){

        			/* Escreve o nome do arquivo na tela */

        			$linhas = count( file( $dir.$arquivo ) ) - 1;

        			$fp=fopen($dir.$arquivo,"a",777);//abrindo e zerando o arquivo;
        			fwrite($fp,"999999999000000000".substr(str_pad($linhas, 9, "0", STR_PAD_LEFT),0,9)."0000000000000000009"."\r\n");//escrevendo o arquivo

        		}


        	}

        }



        public function zipar($prefix){
        	//if($this->verificaExisteArquivoReg()){

        	$zip = new ZipArchive();
        	if ( $zip->open( $this->direct."{$prefix}relatorio.zip", ZipArchive::OVERWRITE ) ) {
        		foreach( glob( $this->direct.'*.txt*' ) as $current )
        			$zip->addFile($current, basename( $current ) );
        			$zip->close();
        			return true;
        	}else{
        		echo "não foi possivel zipar!";
        	}
        	// }else{
        	//	 echo "Não foi gerado o arquivo ZIP";
        	//}
        }

        public function verificaExisteArquivoDownload($prefix){
        	$arquivo = $this->direct."{$prefix}relatorio.zip";
        	if (file_exists($arquivo)) {
        		return true;
        	} else {
        		return false;
        	}
        }


        public function verificaExisteArquivoReg($prefix){
        	$arquivo = $this->direct."{$prefix}relatorio.zip";
        	if (file_exists($arquivo)) {
        		return true;
        	} else {
        		return false;
        	}
        }


        public function liberaDownload(){
        	echo "<script>$('#modal-download').modal();</script>";
        }

        public function dell(){
        	foreach (glob($this->direct.'*.*') as $filename) {
        		unlink($filename);
        	}
        }



        public function newZip($prefix){

        		$zip = new ZipArchive();
        		$zip->open($this->direct."{$prefix}relatorio.zip", ZipArchive::CREATE);
        		$zip->addEmptyDir(date('d-m-Y'));
        		// $zip->extractTo('./');
        		$zip->close();

        }

        public function organizaData($data){
        	$data = explode('-',$data);
        	$dia = $data[0];
        	$mes = $data[1];
        	$ano = $data[2];

        	$dataF = $ano."".$mes."".$dia;
        	return $dataF;
        }

        public function organizaDataInsert($data){
        	$data = explode('/',$data);
        	$dia = $data[0];
        	$mes = $data[1];
        	$ano = $data[2];

        	$dataF = $ano."-".$mes."-".$dia;
        	return $dataF;
        }

        public function organizaHora($hora){
        	$hora  = explode(':',$hora);
        	$horaF = $hora[0];
        	$min   = $hora[1];

        	$horaF = $horaF."".$min;
        	return $horaF;
        }

        function tiraPonto($valor){
        	$pontos = array(",", ".",":");
        	$result = str_replace($pontos, "", $valor);
        	return $result;
        }

        function tiraEm($valor){
        	$pontos = array("EM");
        	$result = str_replace($pontos, "", $valor);
        	return $result;
        }



        function tiraBarra($valor){
        	$pontos = array("/");
        	$result = str_replace($pontos, "", $valor);
        	return $result;


        }
        public function limitarTexto($texto, $limite){
        	$texto = substr($texto,0,$limite);
        	return $texto;
        }


	}

?>
