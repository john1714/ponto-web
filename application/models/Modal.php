<?php

  class Modal extends CI_MODEL{

  		 public function __construct()
        {
                parent::__construct();
        }

  		public function modal($array = array())
  		{
  			if( isset($array['header']) && !empty($array['header']) ){}

  			$time = time();
  			$modal = "<div class='modal fade' id='".$time."'>";
  				$modal .= "<div class='modal-dialog'>";
  					$modal .= "<div class='modal-content'>";
  						
  						if(isset($array['header']) && !empty($array['header'])){
  						
  							$modal .= "<div class='modal-header'>";
	        					$modal .= "<button type='button' class='close' data-dismiss='modal' aria-label='Close'>";
	          					$modal .= "<span aria-hidden='true'>&times;</span>";
	        					$modal .= "</button>";
	        					$modal .= "<h4 class='modal-title'>".$array['header']."</h4>";
      						$modal .= "</div>";

  						}

	      			$modal .= "<div class='modal-body'>";
	        		
	      				if(isset($array['body']) && !empty($array['body'])){
	      					$modal .= $array['body'];
	      				}

	      			$modal .= "</div>";
	      		if(isset($array['footer']) && !empty($array['footer'])){
			        $modal .= "<div class='modal-footer'>";
			  	      $modal .= "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>";
				      $modal .= "<button type='button' class='btn btn-primary'>Save changes</button>";
				    $modal .= "</div>";
				}
  					$modal .= "</div>";
				$modal .= "</div>";
  			$modal .= "</div>";
 
 			if(true){
 				$modal .= "<script>$('#".$time."').modal();</script>";
 			}

  			return $modal;

  			//echo "<script>$('#".$time."').modal();</script>";

  		}

  }

?>
