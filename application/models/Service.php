<?php 

	class Service extends CI_MODEL{

		 public function __construct()
        {
                parent::__construct();
                
        }

		public function ldapGetAllUser(){
			
			//$url  = "http://186.215.108.18:3520/wsldap/webapi/ldap/users";
			$url  = "http://10.200.250.80:8080/wsldap/webapi/ldap/users";
			$user = "";
			$pass = "";
			$headers = array(
		    'Content-Type:application/json'
		    //'Authorization: Basic '. base64_encode($user.":".$pass) // <---
			);
			$ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_HEADER, 0);
		    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    // Timeout in seconds
		    curl_setopt($ch, CURLOPT_TIMEOUT, 30);

		    return curl_exec($ch);
			
		}



		public function ldapGetUserEm($em){
			
			//$url  = "http://186.215.108.18:3520/wsldap/webapi/ldap/".$em;
			$url  = "http://10.200.250.80:8080/wsldap/webapi/ldap/".$em;
			$user = "";
			$pass = "";
			$headers = array(
		    'Content-Type:application/json'
		    //'Authorization: Basic '. base64_encode($user.":".$pass) // <---
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_HEADER, 0);
		    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    // Timeout in seconds
		    curl_setopt($ch, CURLOPT_TIMEOUT, 30);

			$retorno = curl_exec($ch);

			return json_decode($retorno,true);

		}
		
		
		public function ldapGetUser($user){
				
			//$url  = "http://186.215.108.18:3520/wsldap/webapi/ldap/user/".$user;
			$url  = "http://10.200.250.80:8080/wsldap/webapi/ldap/user/".$user;
			$user = "";
			$pass = "";
			$headers = array(
					'Content-Type:application/json'
					//'Authorization: Basic '. base64_encode($user.":".$pass) // <---
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// Timeout in seconds
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		
			$retorno = curl_exec($ch);
		
			return json_decode($retorno,true);
		
		}



		public function ldapAutenticar($user,$pass){
			$URL = "http://10.200.250.80:8080/wsldap/webapi/ldap/autenticar";
			//$URL = "http://186.215.108.18:3520/wsldap/webapi/ldap/autenticar";
					
			$headers = array(
		    'Content-Type:application/json'
		    //'Authorization: Basic '. base64_encode($user.":".$pass) // <---
			);

			$aut = array('user' => $user, 'password' => $pass);
			
			$users = json_encode($aut);
	
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$URL);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $users);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			$retorno = curl_exec($ch);
			curl_close($ch);
			return json_decode($retorno,true);

		}



	}

?>
