<?php

	class Functions extends CI_MODEL{

		private $ip;

		public function __construct()
        {
                parent::__construct();

                $this->ip = $_SERVER['HTTP_X_FORWARDED_FOR'];//$this->input->ip_address();

        }


        public function getIp(){
        	return $this->ip;
        }


		public function validIp(){
			if (!filter_var($this->ip, FILTER_VALIDATE_IP) === false) {
				return true;
			} else {
				return false;
			}
		}

		public function criaFilial(){
			$ip = $this->getArrayIp();

			$newip = (int)$ip[2];

			if($newip == '3' || $newip == '0'){
				$newip = 3;
			}

			if($newip > 9){

				return "F".$newip;

			}elseif($newip < 10){

				return "F0".$newip;

			}
		}

		public function getFilialAuth(){

			if($this->validIp()){

				if($this->permiteIpFixo()){
						return $this->criaFilial();
				}else{
						if($this->validIpPadrao()){
							return $this->criaFilial();
						}else{
							return "Não identificado";
						}
				}

			}else{
				return "Não identificado";
			}


		}

		public function getArrayIp(){

			if($this->validIp()){
				$ip = $this->ip;
				$ip2 = explode(".", $ip);
				return $ip2;
			}else{
				return false;
			}

		}


		public function getPadraoIpLocal(){

			$ip = $this->getArrayIp();

			return $ip[0].".".$ip[1];

		}

		public function verifica_permissao_geral(){


			if($this->validIp()){
				if(!$this->permiteIpFixo()){
					if($this->validIpPadrao()){
						if($this->bloqueiaBatidaMesmaFilial()){
								if($this->permiteMesmaFilial()){
									return true;
								}else{
									return false;
								}
							}else{
								return true;
							}
					}else{
						return false;
					}
				}else{

					$session = $_SESSION['session'];

					$ln = $this->glob->getIpFixo(array('ip' => $this->getIp(), "filial" => $session['filial'], "status" => "cf") );
					
					
					if($session['usuario'] == 'ffernand'){
						return true;
					}

					if( $session['filial'] == $ln[0]['filial_usuario'] ){
						return true;
					}else{
						return false;
					}

				}
			}else{
				return false;
			}

		}


		public function bloqueiaBatidaMesmaFilial(){

			$this->load->model("DAO/AdminDAO","admin");

			$name = $this->session->userdata('session');

			$filialAuth = $this->getFilialAuth();

			$filialUser = $this->filialUser($name['filial']);

				if($filialAuth === $filialUser){
					return true;
				}else{
					return false;
				}


		}

		public function filialUser($filial){ 			if($filial === "F00"){
				return "F03";	
			}else{
				return $filial;
			}
		}

		public function permiteIpFixo(){

			$this->load->model("DAO/GlobalLocalDAO","glob");

			if(isset($_SESSION['session'])){
				$session = $_SESSION['session'];
				if(isset($session['filial'])){
					$filial_usuario = $session['filial'];
				}else{
					$filial_usuario = false;
				}
			}else{$filial_usuario = false;}

			$ln = $this->glob->getIpFixo(array('ip' => $this->getIp(), "filial" => $filial_usuario, 'status' => 'cf') );

			return (count($ln)) ? true : false;

		}

		public function permiteMesmaFilial(){

			$this->load->model("DAO/GlobalLocalDAO","glob1");

			$ln = $this->glob1->getEspFilial( array('filial' => $this->getFilialAuth(), 'status' => 'cf') );

			return (count($ln) > 0) ? true : false;

		}

		public function validIpPadrao(){

			$this->load->model("DAO/GlobalLocalDAO","glob");

			$ln = $this->glob->getIpPadrao( array('ip' => $this->getPadraoIpLocal(), 'status' => 'cf') );

			return (count($ln)) ? true : false;

		}

		public function verificaFormatoIP(){
			$arrayIp = explode('.',$this->getIp());

			if($arrayIp[0] != 10  || $arrayIp[1] != 10 ){
				return false;
			}else{
				return true;
			}
		}


		public function removeEm($em){
			$em = $em;
			$retira = array("em", "EM","Em","eM");
			$result = str_replace($retira, "", $em);
			return $result;
		}

		public function convertData($data){
			$newdata = explode("/", $data);
			$newdata = $newdata[2]."-".$newdata[1]."-".$newdata[0];
			return $newdata;
		}


		public function load_view(){
			$load = "<!DOCTYPE html>
			 <head>
			    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
			    <!-- Meta, title, CSS, favicons, etc. -->
			    <meta charset='utf-8'>
			    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
			    <meta name=\"viewport\" content='width=device-width, initial-scale=1'>
			    <title></title>
			    <!-- Bootstrap -->
			    <link href=".base_url('bootstrap/css/bootstrap.min.css')." rel='stylesheet'>
			    <script src=".base_url('bootstrap/js/jquery.min.js')." ></script>
			    <script src=".base_url('bootstrap/js/bootstrap.min.js')." ></script>
			    <style>

			    </style>
			</head>
			<body>

			        <div class='container'>
			            <div class='row' style='margin-top: 15%;'>
			                <div class='col-md-12 col-md-offset-5'>
			                    <img alt='Ibyte' src=".base_url('public/img/LogoIbyte.png')." />
			                </div>
			                <div class='col-md-12 col-md-offset-5'>
			                    <img alt='Load' src=".base_url('public/img/30.gif')." />
			                </div>
			            </div>
			        </div>

			</body>

			</html>";
			return $load;
		}


		public function getDataSemanal($filial = null){


			$this->load->model("DAO/AdminDAO","admin");

			$ln = $this->admin->getFilialInterval($filial);

			foreach ($ln as $key => $val) {
				$qnt1 = (int)$val['qnt_inicio'];
				$qnt2 = (int)$val['qnt_fim'];
			}
			$qnt1--;

    		// Transforma em datetime
			$data['inicio'] = new Datetime(date("Y-m-d"));
			$data['fim']    = new Datetime(date("Y-m-d"));
			$data['hoje']   = new Datetime(date("Y-m-d"));

			// Adiciona um dia, porque o array do foreach começa de 0
			//$data['inicio']->modify('3+ weekdays');
			$inicio = $data['inicio']->modify("$qnt1 day");

			$fim    = $data['fim']->modify("$qnt2 day");

			// Intervalo de 1 dia
			$interval = new DateInterval("P1D");

			// Crio um array com as datas do periodo definido
			$periodoVigente = new DatePeriod($inicio, $interval ,$fim);

			// Agora, percorro o array, pegando apenas datas
			$dias = array();
			foreach ($periodoVigente as $key => $dataPeriodo)
			{

				// Se não for final de semana (php >= 5.1)
				//if (!(date('N', strtotime($dataPeriodo->format('Y-m-d'))) >= 6))
				//{
					if ($dataPeriodo >= $data['hoje'] || $dataPeriodo <= $data['hoje'] )
					{
						$dias[] = $dataPeriodo->format('d/m/Y');
					}
				//}
			}

			return $dias;

		}



		public function validaDataJustificativa($data = null){

			if($data === null) return false;

			$this->load->model("DAO/AdminDAO","admin");

			$dateRange = $this->getDataSemanal();


		    if(in_array($this->convertDataPadrao($data), $dateRange)){
		    	return true;
		    }else{
		    	return false;
		    }

		}

		public function checkdata($data){

		    $data_do_formulario = $data;
		    $parts = explode('-', $data_do_formulario);
		    $ano = $parts[0];
		    $mes = $parts[1];
		    $dia = $parts[2];
		    if (checkdate($mes, $dia, $ano))
		    {
		        return true;
		    }
		    else
		    {
		        return false;
		    }

		}


		public function convertDataPadrao($data){

		    $data_do_formulario = $data;

		    $parts = explode('-', $data_do_formulario);

		    $ano = $parts[0];
		    $mes = $parts[1];
		    $dia = $parts[2];

		    return $dia."/".$mes."/".$ano;

		}


		public function ifArrayEmpty($arr){

			$campos = "";

			if(is_array($arr)){
				foreach ($arr as $key => $value) {

					if(empty($arr[$key])){
						$campos .= " Campo ".$key." vazio -";
					}
				}


				$campos = substr($campos,0,-1);

				if(empty($campos)){
					return true;
				}else{
					return array('resp' => $campos );
				}

			}else{
				return array('resp' => 'Array test erro!' );
			}

		}


	}

?>
