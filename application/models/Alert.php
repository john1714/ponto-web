<?php

  class Alert extends CI_MODEL{

    public function alert_message($msg = array(),$redirect = NULL)
    {
     	$this->session->set_flashdata('aviso', array('type' => 'text', 'message' => $msg));
		
		if(!empty($redirect))
		{
			redirect($redirect, 'refresh');				
		}

    }
    
    public function _alert($text = NULL, $class = NULL){
    	return "<div class=\"$class\"> $text </div>";
    }

  }

?>