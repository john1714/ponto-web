<?php
class InputNames{
	
	
	public function namesFormGestor(){
		return array(
				'em',
				'check',
				'motivo',
				'data',
				'hora',
				'hora1',
				'hora2',
				'hora3',
				'hora4',
				'obs'
		);
	}
	
	public function getNameAcesso(){
		return array(
				'usuario',
				'acao',
                'filial',
				'diaanterior',
				'diaposterior'
		);
	}
	
}