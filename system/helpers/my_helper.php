<?php

if ( ! function_exists('status_name'))
{
    function status_name($status = "")
    {
        $name = "";
        switch ($status){
            case 'PD':
                $name = "Aguardando aprovação do DP";
                break;
            case 'CF':
                $name = "Justificativa Aprovada";
                break;
            case 'PDCL':
                $name = "Aguardando aprovação do Gestor";
                break;
            case 'CAGS':
                $name = "Justificativa recusada pelo Gestor";
                break;
            case 'CA':
                $name = "Justificativa recusada pelo DP";
                break;
        }
        return $name;
    }
}