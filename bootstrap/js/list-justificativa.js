function recusar(id){

		    	
				$.ajax({
					type : "POST",
					 url : "<?= base_url('Gestor/recusar') ?>",
					data : {id:id},
			    dataType : "json",
					beforeSend : function(){
						var tr = $("#tr_"+id+" .img-load").show();
					},
					success : function(data){
						var tr = $("#tr_"+id+" .img-load").hide();
				
						if(data.success == "true"){
								
								var tr = $("#tr_"+id);
	
						    	var trNome     = tr.find('td:eq(1)');
						    	var trFilial   = tr.find('td:eq(2)');
						    	var trData     = tr.find('td:eq(3)');
						    	var trData_add = tr.find('td:eq(4)');
						    	var trHora     = tr.find('td:eq(5)');
						    	var trAcao     = tr.find('td:eq(6)');

								
							 	trNome.html(data.input.nome);
							    trFilial.html(data.input.filial);
							    trData.html(data.input.data);
							    trData_add.html(data.input.data_add);
							    trHora.html(data.input.hora);
							    trAcao.html(data.input.acao);
							    
							    															
						}
						console.log(data.input);
					}
					
				});
}
			


function aprovar(id){

	
	$.ajax({
		type : "POST",
		 url : "<?= base_url('Gestor/aprovar') ?>",
		data : {id:id},
    dataType : "json",
		beforeSend : function(){
			var tr = $("#tr_"+id+" .img-load").show();
		},
		success : function(data){
			var tr = $("#tr_"+id+" .img-load").hide();
	
			if(data.success == "true"){
					
					var tr = $("#tr_"+id);

			    	var trNome     = tr.find('td:eq(1)');
			    	var trFilial   = tr.find('td:eq(2)');
			    	var trData     = tr.find('td:eq(3)');
			    	var trData_add = tr.find('td:eq(4)');
			    	var trHora     = tr.find('td:eq(5)');
			    	var trAcao     = tr.find('td:eq(6)');

					
				 	trNome.html(data.input.nome);
				    trFilial.html(data.input.filial);
				    trData.html(data.input.data);
				    trData_add.html(data.input.data_add);
				    trHora.html(data.input.hora);
				    trAcao.html(data.input.acao);
				    
				    															
			}
			console.log(data.input);
		}
		
	});
}
